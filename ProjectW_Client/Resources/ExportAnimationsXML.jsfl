﻿//var file = fl.configURI + 'Javascript/MotionXML.jsfl';
//fl.runScript(file, 'exportMotionXML'); 
var sourceXML = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
var depth = 0;
var array = new Array();

var lastFramePosX = 0;
var lastFramePosY = 0;
var lastFrameRot = 0;

stringReplace = function(source, find, replace)
{
	if (!source || !source.length)
		return '';
	//return source.replace(find, replace);
	return source.split(find).join(replace);
}

saveMotionXML = function(contents, motionName)
{
	fl.trace("Save");
	if (!contents)
		return false;
	var fileURL = '';
	if (!motionName)
	{
		fileURL = fl.browseForFileURL("save", "Save Motion XML as...");
		if (!fileURL || !fileURL.length)
			return false;
	}
	else
	{
		var motionsFolder = fl.configURI+'JavaScript/';
		FLfile.createFolder(motionsFolder);
		fileURL = motionsFolder + motionName;
	}
	var ending = fileURL.slice(-4);
	if (ending != '.xml')
		fileURL += '.xml';

	var contentsLinebreaks = stringReplace(contents, "\n", "\r\n");
	
	fl.trace(fileURL);
	fl.trace(contentsLinebreaks);
	if (!FLfile.write(fileURL, contentsLinebreaks))
	{
		alert(CopyMotionErrorStrings.SAVE_ERROR);
		return false;
	}
	return true;
}

pushArray = function( pkey, data )
{	
	if( pkey == undefined)
	{
		fl.trace("undefined key!!");
		return	;		
	}
	//fl.trace("PushArray : " + array.length );
	for(var i = 0; i < array.length; ++i)
	{
		//fl.trace("keycomp : "+ i + "//" + array[i].key + "//" + pkey);
		if( array[i].key == pkey )
		{
			//fl.trace("Found!!!");
			var len = array[i].value.length;
			array[i].value[len] = data;
			//fl.trace(pkey + "- pushArray len: " + len + "/" + data.rot);
			return;
		}
	}
	
	var aniDataMap = { key:pkey, value:null 
	, display:function(){
			for(var i = 0; i < this.value.length; ++i)
			{
				fl.trace("x:" + this.value[i].x + " y:" + this.value[i].y + " rot:" + this.value[i].rot);
			}		
		}
	, toXml:function()
		{			
			var xmlStr = '\t<Motion symbolName="' + this.key + '">';
			for(var i = 0; i < this.value.length; ++i)
			{
				xmlStr += '\n\t\t<Keyframe index="' + (i+1) + '" x="' + this.value[i].x + '" y="' + this.value[i].y + '" rotation="' + this.value[i].rot +'"/>';
			}
			xmlStr += "\n\t</Motion>\n";
			return xmlStr;
		}	
	};
	
	aniDataMap.key = pkey;
	aniDataMap.value = new Array();
	array[array.length] = aniDataMap;
	//fl.trace(pkey + " - new elem added! : " + array.length);
}



convertRelatvieTransform = function()
{
	fl.trace("Conver to Relative transform");
	for(var i = 0; i < array.length; ++i)
	{
		for(var j = 1; j < array[i].value.length-1 ; ++j)
		{
			//fl.trace(i + "/" + j + "/" + array[i].key + "/" + array[i].value.length);
			//fl.trace("rot1:" + array[i].value[j+1].rot	+ "  rot2:" + array[i].value[j].rot);
			
					  
			var x = 	array[i].value[j+1].x	- 	array[i].value[j].x;
			var y = 	array[i].value[j+1].y	- 	array[i].value[j].y;
			var rot = 	array[i].value[j+1].rot	- 	array[i].value[j].rot;			
			//fl.trace( i + "//" + j + " Key:" + array[i].key + " /rota:" + rot + " x:" + x);
			
			array[i].value[j].x = x;
			array[i].value[j].y = y;
			array[i].value[j].rot = rot;
		}
	}
}


printArray = function()
{
	fl.trace("Print Array!");
	for(var i = 0; i < array.length; ++i)
	{
		array[i].display();
	}
}

printXml = function()
{
	for(var i = 0; i < array.length; ++i)
	{
		fl.trace(array[i].toXml());
	}
}

writeXml = function()
{
	sourceXML += "<Animation>\n";
	for(var i = 0; i < array.length; ++i)
	{
		sourceXML += array[i].toXml();
	}
	sourceXML += "\n</Animation>";
	saveMotionXML(sourceXML, "Animations.xml");
}

extractPartName = function( itemName )
{
	var partName = "";
	for(var i = itemName.length-1; i >= 0; --i)
	{
		if( "/" == itemName[i])
		{
			for(var j = i+1; j < itemName.length; ++j)
			{
				partName += itemName[j];
			}
			
			return partName;
		}
	}
}

parseTimeLine = function()
{	
	var timeline = fl.getDocumentDOM().getTimeline();
	//fl.trace("DEPTH:" + ++depth);
	for (var li=0; li<timeline.layers.length; li++)
	{
		var theLayer = timeline.layers[li];
		//fl.trace('======layer[ ' + li + ']:' + theLayer.name);	
		
		var frameArray = theLayer.frames;		
		for (var n=0; n<frameArray.length; n++)
		{ 	
			fl.trace("=========frame:" + n);
			var frameElements = frameArray[n].elements;
			if( frameElements == null || frameElements == undefined ) continue;
			for(var elemIdx = 0; elemIdx < frameElements.length; ++elemIdx)
			{
				var elem = frameElements[elemIdx];
				if( elem != undefined )
				{
					var partName = extractPartName( elem.libraryItem.name );
					if( partName == undefined)
						continue;
					//fl.trace("part:" + partName + " rot:" + elem.rotation);
					var aniData = { x:elem.x, y:elem.y, rot:elem.rotation, key:partName
					, display:function()
						{
							fl.trace(this.key);
							fl.trace(this.x);
							fl.trace(this.y);
							fl.trace(this.rot);							
						}
					};
					pushArray(partName, aniData);					
				}
			}
		}
	}
	--depth;
	convertRelatvieTransform();
	writeXml();
}

parseFrame = function(frameArray, beginFrame, endFrame)
{
	var ele = frameArray[beginFrame].elements[0];
	if(!ele) return;
	
	var px = ele.x;
	var py = ele.y;
	var rot = ele.rotation;
	
	
	ele = frameArray[endFrame].elements[0];
	if( !ele) return;
	var pxx = ele.x - px; //change to last x, y , rotation
	var pyy = ele.y - py;	
	var rott = rot;
	
	if( pxx == 0 && pyy == 0 && rott == 0)
		return;
	
	sourceXML += "\t<Keyframe";
	sourceXML += ' x="' + pxx + '" y="' + pyy + '" rotation="' +  rott + '" index="' + beginFrame + '"';
	sourceXML += '/>\n';
}

ff = function(item)
{
	if( !item || !item.timeline) return;
	for (var li=0; li<timeline.layers.length; li++)
	{
		var theLayer = timeline.layers[li];
		fl.trace('======layer[ ' + li + ']:' + theLayer.name);	
		
		var firstFrame = 0;
		var frameArray = theLayer.frames;
		var isNewMotion = false;
		var hasMotion = false;
		if( frameArray.length == 0 )
			continue;
			
		for (var n=0; n<frameArray.length; n++)
		{				
			//fl.trace( "type[" + n + "]:"+ frameArray[n].tweenType);
			if (n==frameArray[n].startFrame)//현재 프레임이 키프레임의 시작?
			{ 
				fl.trace("startFrame");
				if( isNewMotion )
				{
					if( false == hasMotion )
					{
						sourceXML += '<Motion symbolName="' + theLayer.name +'">\n';
						hasMotion = true;
					}
					parseFrame(frameArray, firstFrame, n);					
				}
				isNewMotion = true;
				firstFrame = n;				
			}
			if( n == frameArray.length -1)
			{//마지막 프레임
				parseFrame(frameArray, firstFrame, n);
			}
		}
		if( hasMotion )
			sourceXML += '</Motion>\n';
	}
}

parseLibItem = function()
{	
	var libItems = fl.getDocumentDOM().library.items;
	fl.trace("items:"+ libItems.length);

	for( var i = 0; i < libItems.length ; ++i)
	{		
		fl.trace("item[" + i + "] : " + libItems[i].name + " type of " + libItems[i].itemType);	
		
		var timeline = libItems[i].timeline;
		if( timeline == undefined ) continue;
		
		if( libItems[i].itemType == "graphic")
		{
			ff(libItems[i]);
			continue;
		}	
	}
}

fl.outputPanel.clear();
fl.trace("******START EXPORT! 0716******");

parseTimeLine();

//fl.trace(sourceXML);
saveMotionXML(sourceXML, "Animations.xml");
