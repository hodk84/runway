#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include <map>
#include "cocos2d.h"

#include "Box2D/Box2D.h"

#include "SimpleAudioEngine.h"
#include "CCGeometry.h"
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>
#include "DrawDebug.h"
#include "WSprite.h"
#include "Scenes\SceneBase.h"

class WCharacter;
class HelloWorld : public SceneBase
{
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::CCScene* scene();
    
    // a selector callback
    virtual void menuCloseCallback(CCObject* pSender);
	virtual void menuSkinCallback(CCObject* pSender);
	virtual void menuAnimCallback(CCObject* pSender);
	virtual void menuClothCallback(CCObject* pSender);
	virtual void menuCatWalkCallback(CCObject* pSender);

    // implement the "static node()" method manually
    LAYER_NODE_FUNC(HelloWorld);	

	virtual void onEnter();	
	WSprite* CreateSprite(const char* fileName, cocos2d::CCSprite* = NULL, int px=0, int py=0);
	
	void	InitSkeletons();
	//bool void XmlParseRecursive();

	void	ScheduleTest(ccTime time)
	{
		printf("hahaa");
	}

	cocos2d::CCSprite* m_pSkeletonRoot;

	CCSprite* ProcessNode(xmlNodePtr pNode, int &nDepth);
	typedef std::map<string, WSprite*>	SkeletonMap;
	SkeletonMap							m_SkeletonMap;

	cocos2d::CCPoint	m_charPos;
	cocos2d::CCPoint	m_charScale;

	WCharacter*			m_pCharacter;

};

#endif  // __HELLOWORLD_SCENE_H__