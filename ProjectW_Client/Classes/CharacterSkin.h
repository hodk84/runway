#pragma once;

#include "Cocos2d.h"
#include "WSkeleton.h"

using namespace cocos2d;

class CharacterSkin
{
public :
	CharacterSkin()
	{
		skinParts.resize(WSkeleton::Node_Count, 0);
	}

	CharacterSkin(const char* fileName)
	{
		skinParts.resize(WSkeleton::Node_Count, 0);
		loadSkin(fileName);
	}


	void addSkinPart(unsigned partId, unsigned resourceID);	
	bool loadSkin(const char* fileName);	

	std::vector<unsigned int> skinParts;
};
