#pragma once

#include "WSprite.h"
#include "Cocos2d.h"

class XmlNode;

class Item
{
public :
	struct Option
	{
		unsigned int	type;
		unsigned int	value;
	};
	typedef std::vector<Option> Options;

	unsigned int		m_type;
	unsigned int		m_thumnailID;
	string				m_name;
	string				m_desc;
	Options				m_options;
	
};
typedef std::map<unsigned int, Item*> ItemMap;

class WClothes : public Item
	// : CCObject autoRelease()는 vector에서 ref++안한다함. CCMutableArray를 사용해야함.
{
public :

	enum ClothesType
	{
		Tops = 1,
		Outer = 2,
		Bottoms = 3,
		Starking = 4,
		Shoes = 5,
		Bag = 6,
		Neckless = 7,
		HairAcc = 8,
		Cloth_Count,
	};
	
	enum Category
	{
		kCat_All = 0,
		kCat_Onepiece = 1,
		kCat_Top = 2,
		kCat_Bottom = 3,
		kCat_Shoes = 4,
		kCat_Accessory = 5,
	};

	static unsigned int Type2Category(unsigned int type);

	struct Part
	{
		unsigned int	partID;
		unsigned int	resourceID;
		int				depth;
	};
	typedef std::vector<Part> ClothParts;


	WClothes(){  }
	virtual ~WClothes(){};

	ClothParts			m_parts;

};
typedef std::map<unsigned int, WClothes*> ClothesMap;

struct Closet
{	
	struct ClosetClothes
	{
		unsigned int	category;
		unsigned int	clothID;
		int				state;
	};
	typedef std::vector<ClosetClothes> ClothesList;

	void addClothes(unsigned int category, unsigned int clothID, int state);
	void getClothes(unsigned int category, ClothesList& list);
	ClosetClothes* getClothes(unsigned int clothID);

	int getClothesCount() { return list.size(); }
	
	ClothesList			list;
};

