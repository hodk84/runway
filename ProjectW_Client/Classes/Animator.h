
#ifndef _ANIMATION_H_
#define _ANIMATION_H_

#include "cocos2d.h"
#include "CCLibxml2.h"
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>
#include <libxml/xmlreader.h>

using namespace cocos2d;

struct KeyAnimation : public CCObject
{
	
	bool ParseXmlKeyAnim(xmlNodePtr pNode);

	struct Rotate
	{
		Rotate()
			:value(0)
			, activated(false)
		{}
		void setValue(float v)
		{
			activated = true;
			value = v;
		}
		bool activated;
		float value;
	};

	struct MoveX
	{
		MoveX()
			:value(0.f)
			,activated(false)
		{}
		void setValue(float v)
		{
			activated = true;
			value = v;
		}
		bool activated;
		float value;
	};
	struct MoveY
	{
		MoveY()
			:value(0.f)
			,activated(false)
		{}
		void setValue(float v)
		{
			activated = true;
			value = v;
		}
		bool activated;
		float value;
	};		
	int _beginFrame;
	int _endFrame;
	unsigned int _frameRate;

	string _nodeName;

	Rotate		_rotate;
	MoveX		_moveX;
	MoveY		_moveY;

};
typedef std::vector<const KeyAnimation*> KeyAnimationVec;

class KeyAnimationTarget
{
public:
	virtual void startKeyAnimation(const KeyAnimation* pAnim, float duration) = 0;
	virtual void startKeyAnimation(std::string str, float rotate) = 0;
};

class AnimationSet : public CCObject, public SelectorProtocol
{
public :
	AnimationSet()
		: m_curFrame(0)
		, m_frameRate(24)
		, m_frameTimer(NULL)
		, m_elapsedTime(0.f)
	{};

	virtual bool ParseAnimationSet(xmlNode* node);

	virtual bool PlayKeyAnimation(KeyAnimationTarget* pTarget);


	virtual void update(ccTime dt);
	void		startAnimation(int sequence);

	virtual void tick(ccTime dt);
	int						m_frameRate;
	KeyAnimationVec			m_AnimVec;
	
	int						m_curFrame;
	int						m_curSequence;
	int						m_destFrame;	

	CCTimer*				m_frameTimer;
	KeyAnimationTarget*		m_curAnimTarget;

	float					m_elapsedTime;

	std::string				m_nodeName;
	float					m_initX;
	float					m_initY;
	float					m_initRot;
};
typedef vector<AnimationSet*> AnimSetVec;

class WAnimation : public CCObject
{
public :
	WAnimation(KeyAnimationTarget* target)
		: m_pAnimTarget(target)
	{

	}

	WAnimation(const char* fileName, KeyAnimationTarget* target)
		:m_pAnimTarget(target)
	{
		LoadAnimation(fileName);
	}
	bool LoadAnimation(const char* fileName);
	void PlayAnimation();

	void setAnimationTarget(KeyAnimationTarget* pTarget)
	{
		m_pAnimTarget = pTarget;
	}
	
	KeyAnimationTarget*	getAnimationTarget()
	{
		return m_pAnimTarget;
	}

protected : 
	KeyAnimationTarget*			m_pAnimTarget;
	AnimSetVec					m_animSetVec;
};
typedef vector<WAnimation*> WAnimationVec;

class NodeAnimator
{
public :
	NodeAnimator();
	virtual ~NodeAnimator();

	virtual void SetRotation(const KeyAnimation* anim) = 0;
	virtual void SetMove(const KeyAnimation* anim) = 0;
	
	KeyAnimationVec		m_anims;	
};



#endif