
#include "CCMask.h"
USING_NS_CC;
/*Useage
CCSize size = CCDirector::sharedDirector()->getWinSize();
	
	CCSprite* maskNode = CCSprite::spriteWithFile("Menu\\mask.png");
	CCSprite* rootNode = CCSprite::spriteWithFile("Menu\\button_name.png");
	CCMask* masked = CCMask::createMaskForObject(rootNode, maskNode);	
	addChild(masked, 2);
	masked->mask();
*/
CCMask* CCMask::createMaskForObject(cocos2d::CCNode *object, cocos2d::CCSprite *mask){
    CCMask* returnMask = new CCMask;
    returnMask->initWithObject(object, mask);
    returnMask->autorelease();
    return returnMask;
}

bool CCMask::initWithObject(cocos2d::CCNode *object, cocos2d::CCSprite *mask) {
    CCAssert(object != NULL, "Invalid sprite for object");
    CCAssert(mask != NULL, "Invalid sprite for mask");
    
    object->retain();
    objectSprite_ = object;
    
    mask->retain();
    maskSprite_ = mask;
        
    // Set up the burn sprite that will "knock out" parts of the darkness layer depending on the alpha value of the pixels in the image.
	ccBlendFunc bfMask = ccBlendFunc();  
	bfMask.src = GL_ZERO;  
	bfMask.dst = GL_ONE_MINUS_SRC_ALPHA; 
    maskSprite_->setBlendFunc(bfMask);
        
    // Get window size, we want masking over entire screen don't we?
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    
    // Create point with middle of screen
    CCPoint screenMid = ccp(size.width * 0.5f, size.height * 0.5f);
    
    // Create the rendureTextures for the mask
	ccBlendFunc bfTexture = ccBlendFunc();    
	bfTexture.src = GL_SRC_ALPHA; 
	bfTexture.dst = GL_ONE_MINUS_SRC_ALPHA;    
    masked = CCRenderTexture::renderTextureWithWidthAndHeight(size.width, size.height);
    masked->retain();
    masked->getSprite()->setBlendFunc(bfTexture);
    
    // Set render textures at middle of screen
    masked->setPosition(screenMid);
    
    // Add the masked object to the screen
    this->addChild(masked);
    
    return true;
}

void CCMask::mask()
{
	this->maskWithoutClear();
}

void CCMask::maskWithClear(float r, float g, float b, float a){
    masked->beginWithClear(r, g, b, a);
	// Limit drawing to the alpha channel
    objectSprite_->visit();
	glColorMask(0.0f, 0.0f, 0.0f, 1.0f);
    // Draw mask
    maskSprite_->visit();
    
    // Reset color mask
    glColorMask(1.0f, 1.0f, 1.0f, 1.0f);
    
    masked->end();
}

void CCMask::maskWithoutClear()
{
	masked->begin();	
    
    objectSprite_->visit();
	// Limit drawing to the alpha channel
	glColorMask(0.0f, 0.0f, 0.0f, 1.0f);
    
    // Draw mask
    maskSprite_->visit();
    
    // Reset color mask
    glColorMask(1.0f, 1.0f, 1.0f, 1.0f);
    
    masked->end();
}

void CCMask::reDrawMask()
{
	//presume object is already drawed in a previous frame 
    masked->begin();
	// Limit drawing to the alpha channel
	glColorMask(0.0f, 0.0f, 0.0f, 1.0f);
    
    // Draw mask
    maskSprite_->visit();
    
    // Reset color mask
    glColorMask(1.0f, 1.0f, 1.0f, 1.0f);
    
    masked->end();
}

void CCMask::setObject(cocos2d::CCNode *object) {
    CC_SAFE_RELEASE(objectSprite_);
    object->retain();
	objectSprite_ = object;
}


void CCMask::setMask(CCSprite* mask) {
    CC_SAFE_RELEASE(objectSprite_);
    mask->retain();
	objectSprite_ = mask;
	ccBlendFunc bfTexture = ccBlendFunc();    
	bfTexture.src = GL_ZERO; 
	bfTexture.dst = GL_ONE_MINUS_SRC_ALPHA;    
    maskSprite_->setBlendFunc(bfTexture);
}

CCSprite* CCMask::maskSprite()
{
	return maskSprite_;
}

CCMask::~CCMask() {
    CC_SAFE_FREE(masked);
    CC_SAFE_FREE(maskSprite_);
    CC_SAFE_FREE(objectSprite_);
}