#pragma warning  (once:4244)


#include "HelloWorldScene.h"
#include "Scenes/ScMainStreet.h"
#include "Scenes/ScCatWalk.h"
#include "Managers/SceneManager.h"
#include "Managers/TableManager.h"
#include "UI/UISceneLogin.h"

using namespace cocos2d;

CCScene* HelloWorld::scene()
{
    CCScene * scene = NULL;
    do 
    {
        // 'scene' is an autorelease object
        scene = CCScene::node();
        CC_BREAK_IF(! scene);

        // 'layer' is an autorelease object
        HelloWorld *layer = HelloWorld::node();
        CC_BREAK_IF(! layer);

// add layer as a child to scene
        scene->addChild(layer);
		

    } while (0);
	    // return the scene
    return scene;
}


// on "init" you need to initialize your instance
bool HelloWorld::init()
{	

    bool bRet = false;
    do 
    {
		m_pSkeletonRoot = NULL;
        //////////////////////////////////////////////////////////////////////////
        // super init first
        //////////////////////////////////////////////////////////////////////////

         //////////////////////////////////////////////////////////////////////////
        // add your codes below...
        //////////////////////////////////////////////////////////////////////////

        // 1. Add a menu item with "X" image, which is clicked to quit the program.

        // Create a "close" menu item with close icon, it's an auto release object.
        CCMenuItemImage *pCloseItem = CCMenuItemImage::itemFromNormalImage(
            "CloseNormal.png",
            "CloseSelected.png",
            this,
            menu_selector(HelloWorld::menuCloseCallback));
        CC_BREAK_IF(! pCloseItem);
        pCloseItem->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width - 20, 20));

		CCMenuItemImage *pSkinButton = CCMenuItemImage::itemFromNormalImage(
            "CloseNormal.png",
            "CloseSelected.png",
            this,
            menu_selector(HelloWorld::menuSkinCallback));
        CC_BREAK_IF(! pSkinButton);
        pSkinButton->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width - 20, 50));

		CCMenuItemImage *pAnimButton = CCMenuItemImage::itemFromNormalImage(
        "CloseNormal.png",
        "CloseSelected.png",
        this,
        menu_selector(HelloWorld::menuAnimCallback));
        CC_BREAK_IF(! pAnimButton);
        pAnimButton->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width - 20, 80));

		CCMenuItemImage *pClothButton = CCMenuItemImage::itemFromNormalImage(
        "CloseNormal.png",
        "CloseSelected.png",
        this,
        menu_selector(HelloWorld::menuClothCallback));
        CC_BREAK_IF(! pClothButton);
        pClothButton->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width - 20, 110));

		CCMenuItemImage *pCatWalkButton = CCMenuItemImage::itemFromNormalImage(
		"CloseNormal.png",
		"CloseSelected.png",
		this,
		menu_selector(HelloWorld::menuCatWalkCallback));
		CC_BREAK_IF( !pCatWalkButton );
		pCatWalkButton->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width - 20, 140));

        // Create a menu with the "close" menu item, it's an auto release object.
        CCMenu* pMenu = CCMenu::menuWithItems(pCloseItem, pSkinButton, pAnimButton, pClothButton, pCatWalkButton, NULL);
        pMenu->setPosition(CCPointZero);
        CC_BREAK_IF(! pMenu);

        // Add the menu to HelloWorld layer as a child layer.
        this->addChild(pMenu, 1);

        // 2. Add a label shows "Hello World".

		//ImageData* pData = TableManager::getInst()->getImageDataByResourceId(10001);//"BG_Street.jpg";
		//std::string strName = pData->getFullPathAsStr();
		//const char* cName = strName.c_str();
        {
			CCLabelTTF* pLabel = CCLabelTTF::labelWithString("Project W", "Thonburi", 64);
			CC_BREAK_IF(! pLabel);
			CCSize size = CCDirector::sharedDirector()->getWinSize();
			pLabel->setPosition(ccp(size.width / 2, size.height - 20));
			this->addChild(pLabel, 1);
		}

		{
			CCLabelTTF* pLabel = CCLabelTTF::labelWithString("Start", "Thonburi", 32);
			CC_BREAK_IF(! pLabel);
			CCSize size = CCDirector::sharedDirector()->getWinSize();
			pLabel->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width - 80, 20));
			this->addChild(pLabel, 1);
		}
		{
			CCLabelTTF* pLabel = CCLabelTTF::labelWithString("4", "Thonburi", 32);
			CC_BREAK_IF(! pLabel);
			CCSize size = CCDirector::sharedDirector()->getWinSize();
			pLabel->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width - 80, 50));
		//	this->addChild(pLabel, 1);
		}
		{
			CCLabelTTF* pLabel = CCLabelTTF::labelWithString("3", "Thonburi", 32);
			CC_BREAK_IF(! pLabel);
			CCSize size = CCDirector::sharedDirector()->getWinSize();
			pLabel->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width - 80, 80));
		//	this->addChild(pLabel, 1);
		}
		{
			CCLabelTTF* pLabel = CCLabelTTF::labelWithString("2", "Thonburi", 32);
			CC_BREAK_IF(! pLabel);
			CCSize size = CCDirector::sharedDirector()->getWinSize();
			pLabel->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width - 80, 110));
		//	this->addChild(pLabel, 1);
		}
		{
			CCLabelTTF* pLabel = CCLabelTTF::labelWithString("1", "Thonburi", 32);
			CC_BREAK_IF( !pLabel );
			CCSize size = CCDirector::sharedDirector()->getWinSize();
			pLabel->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width - 100, 140));
			//this->addChild(pLabel, 1);
		}
		CCSize size = CCDirector::sharedDirector()->getWinSize();
        // 3. Add add a splash screen, show the cocos2d splash image.
		CCSprite* pSprite = TableManager::getInst()->getSpriteByResourceId(100002);
        CC_BREAK_IF(! pSprite);

        // Place the sprite on the center of the screen
        pSprite->setPosition(ccp(size.width/2, size.height/2));

        // Add the sprite to HelloWorld layer as a child layer.
        this->addChild(pSprite, 0);

		UISceneLogin* loginUI = new UISceneLogin();
		this->addChild(loginUI);

	    bRet = true;

    } while (0);

    return bRet;
}

void HelloWorld::menuCloseCallback(CCObject* pSender)
{
    // "close" menu item clicked
   // CCDirector::sharedDirector()->end();
	//InitSkeletons();
	SceneManager::getInst()->ChangeScene(SceneManager::kScene_Town);
}

void HelloWorld::menuSkinCallback(CCObject* pSender)
{
	if( m_pCharacter )
	{
		WBone* pRoot = (WBone*)m_pCharacter->GetChildNode("root");
		if( pRoot )
		{
			pRoot->SetDefaultSkin();
		}
	}
}

void HelloWorld::menuAnimCallback(CCObject* pSender)
{
	if( m_pCharacter )
		m_pCharacter->playAnimation(0);
}

void HelloWorld::menuClothCallback(CCObject* pSender)
{
	SceneManager::getInst()->ChangeScene(SceneManager::kScene_Shop);
}

void HelloWorld::menuCatWalkCallback(CCObject* pSender)
{
	// TODO: Change Scene to CatWalk
	SceneManager::getInst()->ChangeScene(SceneManager::kScene_CatWalk);
}

void HelloWorld::InitSkeletons()
{
	if( m_pCharacter )
	{
		this->removeChild(m_pCharacter, true);
		m_pCharacter->release();		
	}	
	
}


void HelloWorld::onEnter()
{	
	SceneBase::onEnter();
	m_pCharacter = NULL;
	//menuCloseCallback(NULL);
	//InitSkeletons();
}   