#include "Cosmetic.h"
#include "Managers/TableManager.h"


Cosmetic::Cosmetic()
:m_selectedExpression(NULL)
{

}

Cosmetic::~Cosmetic()
{
	
}

Cosmetic* Cosmetic::cosmetic()
{
	Cosmetic* pCosmetic = new Cosmetic();
	if( pCosmetic )
	{
		pCosmetic->autorelease();
		return pCosmetic;
	}
	CC_SAFE_DELETE(pCosmetic);
	return NULL;
}

bool Cosmetic::checkExpression(unsigned int expressionIdx)
{
	if( expressionIdx < 0 || expressionIdx >= Expression_Count )
		return false;

	return true;
}

void Cosmetic::setCosmeticData(const CosmeticDataVec& vec)
{
	CosmeticDataVec::const_iterator itr = vec.begin();
	for(; itr != vec.end(); ++itr)
	{
		char szFileName[256];
		if( TableManager::getInst()->getFilePathByResourceId((*itr).resourceId, szFileName) )
		{
			WSprite* pSprite = WSprite::CreateNodeWSprite(szFileName, 0, 0, 0, 0, false, 0);			
			m_expressions.addObject(pSprite);
		}
	}
	m_selectedExpression = m_expressions.getObjectAtIndex(0);	
}

void Cosmetic::setExpression(unsigned int expressionIdx)
{
	if( checkExpression(expressionIdx) )
	{
		m_selectedExpression = m_expressions.getObjectAtIndex(expressionIdx);
	}
}

void Cosmetic::draw()
{
	if( m_selectedExpression )
	{
		m_selectedExpression->visit();
	}	
}