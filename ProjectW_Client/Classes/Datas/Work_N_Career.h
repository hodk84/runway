#pragma once;

#include "cocos2d.h"

using namespace cocos2d;

struct JobData
{
	static const int Max_Level = 3;

	unsigned int	jobId;
	CCString		name;
	CCString		description;
	
	int				qualification;
	int				workForStar;	
	int				consumptionPerWork;
	
	int				payPerWork[Max_Level];
		
	unsigned int	resourceId;

	int		getLevel(int workCount);
	int		getPay(int workCount);

};
typedef std::vector<JobData> JobVec;


struct Career : public CCObject
{
	unsigned int	jobId;
	unsigned int	workCount;
};
typedef CCMutableArray<Career*>	CareerArray;

class CareerStatus
{

public :
	Career*		getWork(unsigned  int jobId)
	{
		CCMutableArray<Career*>::CCMutableArrayIterator it;
		for (it = m_workArray.begin(); it != m_workArray.end(); ++it)
		{
			Career* job = (Career*)(*it);
			if (job && job->jobId == jobId)
			{
				return job;
			}
		}
	}

	Career* 		addWork(unsigned  int jobId, int count = 0)
	{
		Career* job = new Career();
		job->jobId = jobId;
		job->workCount = count;
		m_workArray.addObject(job);
		return job;
	};

	void			increaseWorkCount(unsigned int jobId, int count = 1)
	{
		Career* job = getWork(jobId);
		if( job )
		{
			job->workCount += count;
		}
	}

	CareerArray		m_workArray;
};
