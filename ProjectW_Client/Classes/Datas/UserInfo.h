#pragma once;
#include "cocos2d.h"

using namespace cocos2d;

struct UserInfo : public CCObject
{
	UserInfo();
	unsigned int	Id;
	std::string		email;
	std::string		userId;
};
