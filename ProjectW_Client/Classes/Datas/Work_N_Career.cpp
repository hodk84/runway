#include "Work_N_Career.h"


int	JobData::getLevel(int workCount)
{
	int level = workCount/workForStar;
	return level;
}

int	JobData::getPay(int workCount)
{
	int level = getLevel(workCount);
	if( level >= 0 && level < Max_Level)
	{
		return payPerWork[level];
	}
	return 0;
}