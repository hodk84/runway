#include "WClothes.h"

unsigned int WClothes::Type2Category(unsigned int type)
{
	switch(type)
	{
	case Tops : 
		return kCat_Top;
	case Outer :
		return kCat_Top;
	case Bottoms :
	case Starking :	
		return kCat_Bottom;
	case Shoes :
		return kCat_Shoes;
	case Bag :		
	case Neckless :
	case HairAcc :
		return kCat_Accessory;	
	}		
}

void Closet::addClothes(unsigned int category, unsigned int clothID, int state)
{
	ClosetClothes clothes;
	clothes.category = category;
	clothes.clothID = clothID;
	clothes.state = state;

	list.push_back(clothes);
}

void Closet::getClothes(unsigned int category, ClothesList& retList)
{
	ClothesList::iterator itrCloth = list.begin();
	for( itrCloth; itrCloth != list.end(); ++itrCloth )
	{
		if( 0 == category || itrCloth->category == category )
		{
			retList.push_back((*itrCloth));
		}
	}
}

Closet::ClosetClothes* Closet::getClothes(unsigned int clothID)
{
	ClothesList::iterator itrCloth = list.begin();
	for( itrCloth; itrCloth != list.end(); ++itrCloth )
	{
		if( itrCloth->clothID == clothID )
		{
			return &(*itrCloth);
		}
	}
	return NULL;
}