
#include "WBone.h"
#include "Animator.h"
#include "WSprite.h"

#define DebugHelper false

WBone::WBone()
	: m_bRelationLIne(DebugHelper)
	, m_bShowPoint(DebugHelper)
	, m_origRot(0)
	, m_origPos(0.f, 0.f)
	, r(1.f)
	, g(0.f)
	, b(0.f)
	, pointSize(8.f)
	, m_depth(0)
{

}	

void WBone::draw()
{
	CCNode::draw();
	if( m_bShowPoint)
	{
		CCSize size = this->getContentSize();
		CCPoint anchor = this->getAnchorPoint();
		//CCPoint pos = this->getPosition();
		CCPoint point((size.width * anchor.x), (size.height * anchor.y));
		glPointSize(pointSize);
		glColor4f(r, g, b, 1);
		ccDrawPoint(point);
	}

	if( m_bRelationLIne )
	{
		CCNode* pParent = this->getParent();
		if( pParent )
		{			
			CCPoint pos(0.f, 0.f);// = this->getPosition();
			CCSize size = this->getContentSize();

			CCPoint p1(pos.x,				pos.y);
			CCPoint p2(pos.x+size.width,	pos.y);
			CCPoint p3(pos.x+size.width,	pos.y+size.height);
			CCPoint p4(pos.x,				pos.y+size.height);

			glColor4f(0.1f*m_depth, g/m_depth ,r, 1);
			ccDrawLine(p1, p2);
			ccDrawLine(p2, p3);
			ccDrawLine(p3, p4);
			ccDrawLine(p4, p1);
		}
	}
}

void WBone::SetDebugMode( bool debugMode)
{
	m_bShowPoint = debugMode;
	m_bRelationLIne = debugMode;
}
	
WBone* WBone::CreateJoint(float width, float height, float ax, float ay, bool anchorConvert, int depth, bool showDebugPoint, const char* skinFile)
{
	WBone* pJoint  = new WBone();
	if( pJoint )
	{	
		if( anchorConvert )
		{			
			float convertedAx = width - (width - ax);
			float convertedAy = height - ay;

			ax = convertedAx/width;
			ay = convertedAy/height;
		}
		pJoint->setContentSize(CCSize(width, height));
		pJoint->setAnchorPoint(CCPoint(ax, ay));			
		pJoint->SetDefaultSkinName(skinFile);
		pJoint->m_depth = depth;		
		return pJoint;
	}
	return NULL;
}

void WBone::SetRotate(const KeyAnimation* anim, float duration)
{
	if( NULL == anim ) return;
	if( anim->_rotate.activated )
	{
		//CCActionInterval* a1 = CCRotateBy::actionWithDuration(duration, anim->_rotate.value);	
		CCActionInterval* a1 = CCRotateTo::actionWithDuration(duration, (m_origRot + anim->_rotate.value));	
	
		CCAction* action1 = (CCActionInterval*)(CCSequence::actions(a1, NULL));		
		runAction(action1);
	}
}
	
void WBone::SetMove(const KeyAnimation* anim, float duration)
{
	if( NULL == anim ) return;
	
	float targetX = m_origPos.x;
	float targetY = m_origPos.y;

	if( anim->_moveX.activated )
	{
		targetX = targetX + anim->_moveX.value ;//- pt.x;
	}
	if( anim->_moveX.activated )
	{
		targetY = targetY + anim->_moveY.value ;//- pt.y;
	}
	
	{
		//CCActionInterval* a1 = CCMoveBy::actionWithDuration(duration, CCPoint(targetX, targetY));	
		
		CCActionInterval* a1 = CCMoveTo::actionWithDuration(duration, CCPoint(targetX, targetY));	
		CCAction* action1 = (CCActionInterval*)(CCSequence::actions(a1, NULL));
		runAction(action1);			
	}
}

void WBone::SetDefaultSkinName(const char* fileName)
{
	if( NULL == fileName ) return;	
	m_defaultSkinName = fileName;
}

void WBone::SetDefaultSkin()
{	
	

	//CCArray* children = this->getChildren();
	//if( NULL == children ) return;
	//for(int childCnt = 0; childCnt < children->count(); ++childCnt)
	//{
	//	WBone* pChildBone = (WBone*)children->objectAtIndex(childCnt);
	//	if( pChildBone )
	//	{
	//		pChildBone->SetDefaultSkin();
	//	}
	//}
}