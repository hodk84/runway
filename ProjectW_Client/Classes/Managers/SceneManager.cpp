#include "SceneManager.h"

#include "Scenes/ScCatWalk.h"
#include "Scenes/SceneHome.h"
#include "Scenes/ScMainStreet.h"
#include "Scenes/ScMainStreet2.h"
#include "Scenes/ScShop.h"
#include "Scenes/ScShopElevator.h"
#include "Scenes/ScCareerElevator.h"
#include "Scenes/ScCareer.h"
#include "Scenes/ScATM.h"
#include "Scenes/ScMyCloset.h"
#include "Scenes/ScCafe.h"
#include "Scenes/ScSalon.h"
#include "Scenes/ScClub.h"
#include "Scenes/ScTown.h"
#include "UI/UISceneDebug.h"
#include "HelloWorldScene.h"

SceneManager* SceneManager::instance = NULL;

SceneBase* SceneManager::getScene(int type)
{
	SceneBase* pScene = NULL;
	CharacterManager::getInst()->resetMyCharacter();
	switch( type )
	{
	case kScene_MainMenu:
		pScene = new HelloWorld();
		break;
	case kScene_MainStreet:
		pScene = new ScMainStreet();		
		break;
	case kScene_MyRoom:
		pScene = new SceneHome();
		break;
	case kScene_CatWalk:
		pScene = new ScCatWalk();
		break;
	case kScene_ShopElevator :
		pScene = new ScShopElevator();
		break;
	case kScene_Shop :
		pScene = new ScShop();
		break;
	case kScene_CareerElevator:
		pScene = new ScCareerElevator();
		break;
	case kScene_Career:
		pScene = new ScCareer();
		break;
	case kScene_ATM:
		pScene = new ScATM();
		break;
	case kScene_MyCloset:
		pScene = new ScMyCloset();
		break;
	case kScene_Cafe :
		pScene = new ScCafe();
		break;
	case kScene_Salon :
		pScene = new ScSalon();
		break;
	case kScene_Club :
		pScene = new ScClub();
		break;
	case kScene_Town :
		pScene = new ScMainStreet();
		break;
	case kScene_MainStreet2:
		pScene = new ScMainStreet();		
		break;

	}
	pScene->init();
	m_lastSceneType = type;
	m_pLastScene = pScene;
	enableCheat();
	return pScene;
}

bool SceneManager::ChangeScene(int sceneType)
{
	CCDirector::sharedDirector()->setDepthTest(false);
	SceneBase* pScene = getScene(sceneType);
	if( pScene == NULL ) return false;
	
	CCDirector *pDirector = CCDirector::sharedDirector();
	if( pDirector  )
	{				
		pDirector->replaceScene(CCTransitionFade::transitionWithDuration(1, pScene, ccBLACK));
		return true;
	}
	return false;
}

void SceneManager::enableCheat()
{
	CCNode* pLayer = m_pLastScene->getUILayer();
	if( pLayer )
	{
		UISceneDebug* pDebugMenu = new UISceneDebug();
		pDebugMenu->setPosition(ccp(0, -580));
		pLayer->addChild(pDebugMenu, 5);
	}
}

void SceneManager::disableCheat()
{

}