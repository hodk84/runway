

#include "CharacterManager.h"
#include "XmlUtils.h"

CharacterManager* CharacterManager::instance = NULL;

bool CharacterManager::initialize()
{
	CharacterStatus data;
	data.cash = 10;
	data.clothCount = 10;
	data.energy = 0;
	data.gameMoney = 1000;
	data.level = 5;
	m_pMyCharacter = createCharacter(data);

	loadCharacterConfig();
		
	return true;
}

WCharacter* CharacterManager::createCharacter(const CharacterStatus& data)
{
	WCharacter* pCharacter = new WCharacter(data);
	if( pCharacter )
		pCharacter->loadDefaultCharacterData();
	return pCharacter;
}

bool CharacterManager::loadCharacterConfig()
{
	const char* filePath = CCFileUtils::fullPathFromRelativePath("Config.xml");
	
	xmlDoc *doc = NULL;
	xmlNode *root_element = NULL;
	doc = xmlReadFile(filePath, NULL, 0);

	root_element = xmlDocGetRootElement(doc);
	if( NULL == root_element)
	{
		//assert(root_element);
		return false;
	}

	xmlNode* pChild = root_element->children;
	xmlChar *cName = NULL;
	xmlChar *cFileName = NULL;
	xmlChar *cCharName = NULL;
	xmlChar *cPX = NULL;
	xmlChar *cPY = NULL;
	xmlChar *cSX = NULL;
	xmlChar *cSY = NULL;
	while(pChild)
	{
		if(0 == xmlStrcmp(pChild->name, (const xmlChar *)"BG"))
		{				
			cName = xmlGetProp(pChild, (const xmlChar *)"name");
			cFileName = xmlGetProp(pChild, (const xmlChar *)"file");
		}
		else if(0 == xmlStrcmp(pChild->name, (const xmlChar *)"Character"))
		{	
			cCharName = xmlGetProp(pChild, (const xmlChar *)"file");
			cPX = xmlGetProp(pChild, (const xmlChar *)"posX");
			cPY = xmlGetProp(pChild, (const xmlChar *)"posY");
			m_charPos = CCPoint(atoi((const char*)cPX), atoi((const char*)cPY));				
			cSX = xmlGetProp(pChild, (const xmlChar *)"scaleX");
			cSY = xmlGetProp(pChild, (const xmlChar *)"scaleY");
			m_charScale = CCPoint(atof((const char*)cSX), atof((const char*)cSY));
		}
		pChild = pChild->next;
	}
	
	xmlFreeDoc(doc);

	return true;
}


WCharacter* CharacterManager::loadCharacterByXml(const char* fileName)
{
	const char* filePath = CCFileUtils::fullPathFromRelativePath(fileName);
	
	xmlDoc *doc = NULL;
	xmlNode *root_element = NULL;
	doc = xmlReadFile(filePath, NULL, 0);

	root_element = xmlDocGetRootElement(doc);
	if( NULL == root_element)
	{
		//assert(root_element);
		return false;
	}
	bool briefLoaded = false;
	bool lookLoaded = false;
					
	WCharacter* pCharacter = new WCharacter();
	CharacterStatus status;
	if(0 == xmlStrcmp(root_element->name, (const xmlChar *)"UserCharacter"))
	{
		xmlNode* part = root_element->children;
		while(part)
		{
			if(0 == xmlStrcmp(part->name, (const xmlChar *)"characterBrief"))
			{
				status.characterName = GetPropertyString(part, "characterName");
				unsigned int charId = GetPropertyUInt(part, "characterId");
				status.clothCount = GetPropertyUInt(part, "clothesCount");
				status.energy = GetPropertyUInt(part, "energy");
				status.gameMoney = GetPropertyUInt(part, "gameMoney");
				status.level = GetPropertyUInt(part, "level");
				
				pCharacter->setCharacterId(charId);
				pCharacter->setCharacterStatus(status);
				pCharacter->updateCharacterStatus();
				
				briefLoaded = true;
			}
			else if(0 == xmlStrcmp(part->name, (const xmlChar *)"userProfile"))
			{	
				std::string userName = GetPropertyString(part, "userName");
				unsigned int userId = GetPropertyUInt(part, "userId");
				unsigned int cash = GetPropertyUInt(part, "cash");
				float	gpsX = GetPropertyFloat(part, "gpsX");
				float	gpsY = GetPropertyFloat(part, "gpsY");
			}
			else if(0 == xmlStrcmp(part->name, (const xmlChar *)"characterLook"))
			{				
				unsigned int sex = GetPropertyUInt(part, "sex");
				unsigned int skin = GetPropertyUInt(part, "skin");
								
				pCharacter->LoadCharacterSkeleton("Skeleton_A.xml");
				
				WAnimation* pAnimation = new WAnimation((KeyAnimationTarget*)pCharacter);	
				pAnimation->LoadAnimation("Animations.xml");
				pCharacter->addAnimationSet(pAnimation);

				CharacterSkin* pSkin = new CharacterSkin("DefaultSkin.xml");
				pCharacter->setSkin(pSkin);	
				pCharacter->reloadSkin();

				xmlNode* partLook = part->children;
				while(partLook)
				{
					if(0 == xmlStrcmp(partLook->name, (const xmlChar *)"clothes"))
					{
						unsigned int clothId = GetPropertyUInt(partLook, "id");	
						pCharacter->setClothes(clothId);
						lookLoaded = true;
					}
					else if(0 == xmlStrcmp(partLook->name, (const xmlChar *)"cosmetic"))
					{
						unsigned int cosmeticId = GetPropertyUInt(partLook, "id");
						pCharacter->setCosmetic(cosmeticId);
					}

					partLook = partLook->next;
				}			
			}
			part = part->next;
		}
		pCharacter->applyClothes();
		pCharacter->applyCosmetic();
	}
	if( briefLoaded == false )
		return NULL;

	else if( briefLoaded && lookLoaded == false )
	{
		pCharacter->loadDefaultCharacterData();
	}

	xmlFreeDoc(doc);
	return pCharacter;
}

bool CharacterManager::addCharacterFromXml(const char* fileName)
{
	WCharacter* pCharacter = loadCharacterByXml(fileName);
	if( NULL == pCharacter )
		return false;

	m_characterArray.addObject(pCharacter);
	return true;
}

void CharacterManager::clearCharacterArray()
{
	m_characterArray.removeAllObjects();
}

WCharacter* CharacterManager::getCharacter(unsigned int characterId)
{
	CCMutableArray<WCharacter*>::CCMutableArrayIterator itr = m_characterArray.begin();
	for( itr ; itr != m_characterArray.end(); ++itr)
	{
		if((*itr)->getCharacterId() == characterId)
			return (*itr);
	}
	return NULL;
}

void CharacterManager::addUserInfo(UserInfo* usrInf)
{
	m_userArray.addObject(usrInf);
}

// 유저아이디로 DB로 부터 캐릭터 정보를 받아옴.
// send id , recieve characterInfo

UserInfo* CharacterManager::getUserInfo(unsigned int id)
{
	CCMutableArray<UserInfo*>::CCMutableArrayIterator itr = m_userArray.begin();
	for( itr ; itr != m_userArray.end(); ++itr)
	{
		if((*itr)->Id == id)
			return (*itr);
	}
	return NULL;
}

WCharacter* CharacterManager::createCharacterFromUserInfo(unsigned int id)
{
	UserInfo* userInfo = getUserInfo(id);
	CharacterStatus status;
	status.characterName = userInfo->userId;	
	return createCharacter(status);
}