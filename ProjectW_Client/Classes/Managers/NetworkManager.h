#include <map>
#include "cocos2d.h"
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>

class NetworkManager // Singleton class
{
private : 
	NetworkManager()	
	{
		
	}

	static NetworkManager* instance;
//	static size_t parseWeather(void* ptr, size_t size, size_t nmemb, void* out);

public :
	static NetworkManager* getInst()
	{
		if( instance == NULL )
		{
			instance = new NetworkManager();			
		}
		return instance;
	}
	bool	getClubUserInfo();

	bool	initialize();
	bool	connectionTest();

	bool	getHasLogin() { return hasLogin; }
	bool	setHasLogin(bool login) { hasLogin = login; }

private : 
	bool	hasLogin;


};