#include "TableManager.h"

#include "Scenes/ScCatWalk.h"
#include "Scenes/SceneHome.h"
#include "Scenes/ScMainStreet.h"
#include "Scenes/ScShop.h"
#include "HelloWorldScene.h"



TableManager* TableManager::instance = NULL;


bool TableManager::Initialize()
{
	if( false == loadResourceTable("ResourceTable.xml") )
	{
		assert(0);
		return false;
	}
	if( false == loadClotheTable("ClothesTable.xml") )
	{
		assert(0);
		return false;
	}
	if( false == loadItemTable("ItemTable.xml") )
	{
		assert(0);
		return false;
	}
	if( false == loadMachandiseTable("MerchandiseTable.xml") )
	{
		assert(0);
		return false;
	}
	
	if( false == loadCosmeticTable("CosmeticTable.xml") )
	{
		assert(0);
		return false;
	}

	if( false == loadJobTable("JobTable.xml") )
	{
		//assert(0);
		return false;
	}
	

	return true;
}

bool TableManager::loadResourceTable(const char* fileName)
{
	const char* filePath = CCFileUtils::fullPathFromRelativePath(fileName);	
	xmlDoc *doc = NULL;
	xmlNode *root_element = NULL;
	doc = xmlReadFile(filePath, NULL, 0);	
	root_element = xmlDocGetRootElement(doc);	
	OutputDebugStringA(filePath);
	assert(root_element);	
	
	m_ImageDataTable.clear();//reset table
	
	if(0 == xmlStrcmp(root_element->name, (const xmlChar *)"ResourceTable"))
	{			
		//xmlChar *cName = xmlGetProp(root_element, (const xmlChar *)"name");
		xmlNode* part = root_element->children;
		while(part)
		{
			if(0 == xmlStrcmp(part->name, (const xmlChar *)"resource"))
			{
				ImageData data;
				xmlChar *cID = xmlGetProp(part, (const xmlChar *)"ID");
				if( cID )
				{
					data.ID = atoi((const char*)cID);
					xmlFree(cID);
				}
				xmlChar *cType = xmlGetProp(part, (const xmlChar *)"type");
				if( cType )
				{
					data.type = atoi((const char*)cType);
					xmlFree(cType);
				}				
				xmlChar *cFolder = xmlGetProp(part, (const xmlChar *)"folder");
				if( cFolder )
				{
					data.folder = (const char*)cFolder;
					xmlFree(cFolder);
				}
				xmlChar *cFileName = xmlGetProp(part, (const xmlChar *)"fileName");
				if( cFileName )
				{
					data.fileName = (const char*)cFileName;
					xmlFree(cFileName);
				}
				xmlChar *cExtension = xmlGetProp(part, (const xmlChar *)"extension");
				if( cExtension )
				{
					data.extension = (const char*)cExtension;
					xmlFree(cExtension);
				}				
				m_ImageDataTable.insert(std::pair<unsigned int, ImageData>(data.ID, data));				
			}
			part = part->next;			
		}		
	}
	else
		return false;
	
	return true;
}

bool TableManager::getFilePathByResourceId(unsigned int resourceId, char* szRet)
{
	ImageDataMap::iterator itrFound = m_ImageDataTable.find(resourceId);
	if( itrFound != m_ImageDataTable.end())
	{
		sprintf(szRet, "%s", (*itrFound).second.getFullPathAsStr().c_str());		
		return true;
	}
	szRet = NULL;

	return false;
}

CCSprite* TableManager::getSpriteByResourceId(unsigned int resourceId)
{
	ImageData* pData = getImageDataByResourceId(resourceId);
	if( pData )
	{
		return CCSprite::spriteWithFile( pData->getFullPathAsStr().c_str() );
	}
	return NULL;
}

ImageData* TableManager::getImageDataByResourceId(unsigned int resourceId)
{
	ImageDataMap::iterator itrFound = m_ImageDataTable.find(resourceId);
	if( itrFound != m_ImageDataTable.end())
	{
		return &(*itrFound).second;
	}
	return NULL;
}


bool TableManager::parseClothesPart(xmlNode* node, WClothes::Part& part)
{	
	xmlChar *cPartID = xmlGetProp(node, (const xmlChar *)"partID");
	if( cPartID )
	{
		part.partID = atoi((const char*)cPartID);
		xmlFree(cPartID);
	}
	xmlChar *cResourceID = xmlGetProp(node, (const xmlChar *)"resourceID");
	if( cResourceID )
	{
		part.resourceID = atoi((const char*)cResourceID);
		xmlFree(cResourceID);
	}
	xmlChar *cDepth = xmlGetProp(node, (const xmlChar *)"depth");
	if( cDepth )
	{
		part.depth = atoi((const char*)cDepth);
		xmlFree(cDepth);
	}

	return true;
}


bool TableManager::parseItemOption(xmlNode* node, WClothes::Option& option)					
{	
	xmlChar *cType = xmlGetProp(node, (const xmlChar *)"type");
	if( cType )
	{
		option.type = atoi((const char*)cType);
		xmlFree(cType);
	}
	xmlChar *cValue = xmlGetProp(node, (const xmlChar *)"value");
	if( cValue )
	{
		option.value = atoi((const char*)cValue);
		xmlFree(cValue);
	}
	return true;
}

bool TableManager::loadItemTable(const char* fileName)
{
	const char* filePath = CCFileUtils::fullPathFromRelativePath(fileName);	
	xmlDoc *doc = NULL;
	xmlNode *root_element = NULL;
	doc = xmlReadFile(filePath, NULL, 0);	
	root_element = xmlDocGetRootElement(doc);
	assert(root_element);	
		
	if(0 == xmlStrcmp(root_element->name, (const xmlChar *)"ItemTable"))
	{			
		xmlNode* nodeItem = root_element->children;
		while(nodeItem)
		{
			if(0 == xmlStrcmp(nodeItem->name, (const xmlChar *)"item"))
			{
				Item* pItem = new Item;				
				unsigned int itemID = GetPropertyUInt(nodeItem, "ID");
				pItem->m_type = GetPropertyUInt(nodeItem, "type");
				pItem->m_thumnailID = GetPropertyUInt(nodeItem, "thumbnail");
				pItem->m_name = GetPropertyString(nodeItem, "name");
				pItem->m_desc = GetPropertyString(nodeItem, "description");
				
				xmlNode* nodeProp = nodeItem->children;
				while(nodeProp)
				{
					bool ret = false;
					if(0 == xmlStrcmp(nodeProp->name, (const xmlChar *)"option"))
					{
						Item::Option op;
						if( parseItemOption(nodeProp, op) )
						{
							pItem->m_options.push_back(op);
						}
						pItem->m_options.push_back(op);
					}					
					nodeProp = nodeProp->next;
				}
				m_itemMap.insert(ItemMap::value_type(itemID, pItem));
			}
			nodeItem = nodeItem->next;
		}		
	}
	else
	{
		return false;
	}
	
	return m_itemMap.size();
}

bool TableManager::loadClotheTable(const char* fileName)
{
	const char* filePath = CCFileUtils::fullPathFromRelativePath(fileName);	
	xmlDoc *doc = NULL;
	xmlNode *root_element = NULL;
	doc = xmlReadFile(filePath, NULL, 0);	
	root_element = xmlDocGetRootElement(doc);
	assert(root_element);	
		
	if(0 == xmlStrcmp(root_element->name, (const xmlChar *)"ClothesTable"))
	{			
		xmlNode* nodeCloth = root_element->children;
		while(nodeCloth)
		{
			if(0 == xmlStrcmp(nodeCloth->name, (const xmlChar *)"clothes"))
			{
				WClothes* pClothes = new WClothes;				
				unsigned int clothesID = GetPropertyUInt(nodeCloth, "ID");
				pClothes->m_type = GetPropertyUInt(nodeCloth, "type");
				pClothes->m_thumnailID = GetPropertyUInt(nodeCloth, "thumbnail");
				pClothes->m_name = GetPropertyString(nodeCloth, "name");
				pClothes->m_desc = GetPropertyString(nodeCloth, "description");
				
				xmlNode* nodeProp = nodeCloth->children;
				while(nodeProp)
				{
					bool ret = false;
					if(0 == xmlStrcmp(nodeProp->name, (const xmlChar *)"option"))
					{
						Item::Option op;
						if( parseItemOption(nodeProp, op) )
						{
							pClothes->m_options.push_back(op);
						}
						pClothes->m_options.push_back(op);
					}
					else if( 0 == xmlStrcmp(nodeProp->name, (const xmlChar *)"part"))
					{
						WClothes::Part pt;
						if( parseClothesPart(nodeProp, pt) )
						{
							pClothes->m_parts.push_back(pt);
						}
						pClothes->m_parts.push_back(pt);
					}
					nodeProp = nodeProp->next;
				}
				m_clothesMap.insert(ClothesMap::value_type(clothesID, pClothes));
			}
			nodeCloth = nodeCloth->next;
		}		
	}
	else
	{
		return false;
	}
	
	return m_clothesMap.size();
}

Item* TableManager::getItem(unsigned int itemID)
{
	ItemMap::iterator itrFound = m_itemMap.find(itemID);
	if( itrFound != m_itemMap.end() )
	{
		return (*itrFound).second;
	}
	return NULL;
}


WClothes* TableManager::getClothes(unsigned int clothesID)
{
	ClothesMap::iterator itrFound = m_clothesMap.find(clothesID);
	if( itrFound != m_clothesMap.end() )
	{
		return (*itrFound).second;
	}
	return NULL;
}

bool TableManager::loadMachandiseTable(const char* fileName)
{
	const char* filePath = CCFileUtils::fullPathFromRelativePath(fileName);	
	xmlDoc *doc = NULL;
	xmlNode *root_element = NULL;
	doc = xmlReadFile(filePath, NULL, 0);	
	root_element = xmlDocGetRootElement(doc);
	assert(root_element);	
	if( NULL == root_element) return false;

	if(0 == xmlStrcmp(root_element->name, (const xmlChar *)"MerchandiseTable"))
	{			
		xmlNode* nodeMerc = root_element->children;
		while(nodeMerc)
		{
			if(0 == xmlStrcmp(nodeMerc->name, (const xmlChar *)"merchandise"))
			{
				MerchandiseList* pMerchandise = new MerchandiseList;
				unsigned int clothesID = 0;
				xmlChar *cID = xmlGetProp(nodeMerc, (const xmlChar *)"id");
				if( cID )
				{
					pMerchandise->id = atoi((const char*)cID);
					xmlFree(cID);
				}

				xmlChar *cName = xmlGetProp(nodeMerc, (const xmlChar *)"name");
				if( cName )
				{
					pMerchandise->name.assign( (const char*)cName );
					xmlFree(cName);
				}

				xmlChar *cSeason = xmlGetProp(nodeMerc, (const xmlChar *)"season");
				if( cSeason )
				{
					pMerchandise->season = atoi((const char*)cSeason);
					xmlFree(cSeason);
				}
				pMerchandise->type = GetPropertyUInt(nodeMerc, "type");

				xmlNode* nodeProp = nodeMerc->children;
				while(nodeProp)
				{
					bool ret = false;
					if(0 == xmlStrcmp(nodeProp->name, (const xmlChar *)"item"))
					{
						MerchandiseItem* pItem = parseMachandiseItem(nodeProp);
						if( pItem )
						{
							pItem->tableType = pMerchandise->type;
							pMerchandise->items.insert(MerchandiseItemMap::value_type(pItem->merchandiseID, pItem));
						}
					}
					nodeProp = nodeProp->next;
				}
				m_merchandiseMap.insert(MerchandiseMap::value_type(pMerchandise->id, pMerchandise));
			}
			nodeMerc = nodeMerc->next;
		}		
	}
	else
	{
		return false;
	}
	
	return m_clothesMap.size();
}

MerchandiseItem* TableManager::parseMachandiseItem(xmlNode* node)
{
	if( NULL == node ) return NULL;

	MerchandiseItem* pItem = new MerchandiseItem;
	pItem->merchandiseID = GetPropertyUInt(node, "merchandiseID");
	pItem->category = GetPropertyUInt(node, "category");
	pItem->itemID = GetPropertyUInt(node, "itemID");
	//pItem->thumbnailID = GetPropertyUInt(node, "thumbnailID");
	pItem->priceType = GetPropertyUInt(node, "priceType");
	pItem->price = GetPropertyUInt(node, "price");

	return pItem;
}

void TableManager::getMerchandiseSet(unsigned int setId, unsigned int category, MerchandiseItemVec& ret)
{
	MerchandiseMap::iterator itrFound = m_merchandiseMap.find(setId);
	if( itrFound == m_merchandiseMap.end() || NULL == (itrFound->second))
		return;
	
	itrFound->second->getItemsByCategory(category, ret);
}

MerchandiseItem* TableManager::getMerchandise(unsigned int setId, unsigned int mrchdsId)
{
	MerchandiseMap::iterator itrFound = m_merchandiseMap.find(setId);
	if( itrFound == m_merchandiseMap.end() || NULL == (itrFound->second))
		return NULL;

	MerchandiseItem* pItem = itrFound->second->getItem(mrchdsId);

	return pItem;
}


bool TableManager::loadCosmeticTable(const char* fileName)
{
	const char* filePath = CCFileUtils::fullPathFromRelativePath(fileName);	
	xmlDoc *doc = NULL;
	xmlNode *root_element = NULL;
	doc = xmlReadFile(filePath, NULL, 0);	
	root_element = xmlDocGetRootElement(doc);
	assert(root_element);	
	
	m_cosmeticVec.clear();//reset table
	
	if(0 == xmlStrcmp(root_element->name, (const xmlChar *)"CosmeticTable"))
	{			
		//xmlChar *cName = xmlGetProp(root_element, (const xmlChar *)"name");
		xmlNode* part = root_element->children;
		while(part)
		{
			if(0 == xmlStrcmp(part->name, (const xmlChar *)"cosmetic"))
			{
				CosmeticData data;
				data.idx = GetPropertyUInt(part, "idx");
				data.id = GetPropertyUInt(part, "id");
				data.resourceId = GetPropertyUInt(part, "resourceId");
				data.expression = GetPropertyUInt(part, "expression");				
				data.partId = GetPropertyUInt(part, "partId");
				m_cosmeticVec.push_back(data);
			}
			part = part->next;
		}		
	}
	else
		return false;
	
	return true;
}

void TableManager::getCosmetic(unsigned int cosmeticId, CosmeticDataVec& retCosmetic)
{
	CosmeticDataVec::iterator itr = m_cosmeticVec.begin();
	for(; itr != m_cosmeticVec.end(); ++itr)
	{
		if( (*itr).id == cosmeticId )
		{
			retCosmetic.push_back((*itr));
		}
	}
}


bool TableManager::loadJobTable(const char* fileName)
{
	const char* filePath = CCFileUtils::fullPathFromRelativePath(fileName);	
	xmlDoc *doc = NULL;
	xmlNode *root_element = NULL;
	doc = xmlReadFile(filePath, NULL, 0);	
	root_element = xmlDocGetRootElement(doc);
	
	if( NULL == root_element ) 
		return false;

	m_jobTable.clear();
	
	if(0 == xmlStrcmp(root_element->name, (const xmlChar *)"JobTable"))
	{			
		xmlNode* part = root_element->children;
		while(part)
		{
			if(0 == xmlStrcmp(part->name, (const xmlChar *)"job"))
			{
				JobData data;
				data.jobId = GetPropertyUInt(part, "id");
				data.name.m_sString = GetPropertyString(part, "name");
				data.description.m_sString = GetPropertyString(part, "description");
				data.qualification = GetPropertyUInt(part, "qualification");
				data.payPerWork[0] = GetPropertyUInt(part, "payPerWorkLv1");
				data.payPerWork[1] = GetPropertyUInt(part, "payPerWorkLv2");
				data.payPerWork[2] = GetPropertyUInt(part, "payPerWorkLv3");

				data.workForStar = GetPropertyUInt(part, "workForStar");
				data.consumptionPerWork = GetPropertyUInt(part, "consumptionPerWork");
				data.resourceId = GetPropertyUInt(part, "resourceId");

				m_jobTable.push_back(data);
			}
			part = part->next;
		}		
	}
	else
		return false;
	
	return true;
}
bool TableManager::getJob(unsigned int jobId, JobData& jobData)
{
	JobVec::iterator itrJob = m_jobTable.begin();
	for(itrJob; itrJob != m_jobTable.end(); ++itrJob)
	{
		if( (*itrJob).jobId == jobId )
		{
			jobData = (*itrJob);
			return true;
		}
	}
	return false;
}