#pragma once;

#include <map>
#include "cocos2d.h"
#include "XmlUtils.h"
#include "DrawDebug.h"
#include "WSprite.h"
#include "WCharacter.h"
#include "WClothes.h"
#include "Merchandise.h"
#include "Cosmetic.h"
#include "Datas/Work_N_Career.h"


struct ImageData
{
	ImageData()
		:ID(0)		
	{

	}
	unsigned int		ID;
	int					type;
	
	std::string folder;
	std::string fileName;
	std::string extension;
	
	const std::string getFullPathAsStr()
	{
		std::string result;
		if( !folder.empty() )
		{
			result = folder+ "\\";
		}

		result += fileName;

		if( !extension.empty() )
		{
			result += "." + extension;
		}
		
		return result;
	}
};
typedef std::map<unsigned int, ImageData> ImageDataMap;

class TableManager // Singleton class
{
private : 
	TableManager()		
	{
		
	}
	static TableManager* instance;

public :
	
	static TableManager* getInst()
	{
		if( instance == NULL )
		{
			instance = new TableManager();			
		}
		return instance;
	}

	bool Initialize();
	

	CCSprite* getSpriteByResourceId(unsigned int resourceId);
	bool getFilePathByResourceId(unsigned int resourceId, char* szRet);
	
	ImageData*			getImageDataByResourceId(unsigned int resourceId);
	
	Item*				getItem(unsigned int itemID);
	WClothes*			getClothes(unsigned int clothesID);
	
	void				getMerchandiseSet(unsigned int setId, unsigned int category, MerchandiseItemVec& ret);
	MerchandiseItem*	getMerchandise(unsigned int setId, unsigned int mrchdsId);

	void				getCosmetic(unsigned int cosmeticId, CosmeticDataVec& retCosmatic);

	bool				getJob(unsigned int jobId, JobData& jobData);

private :
	
	bool loadResourceTable(const char* fileName);
	bool loadItemTable(const char* fileName);
	bool loadClotheTable(const char* fileName);
	bool loadMachandiseTable(const char* fileName);
	bool loadCosmeticTable(const char* fileName);
	bool loadJobTable(const char* fileName);

	bool parseItemOption(xmlNode* node, WClothes::Option& option);
	bool parseClothesPart(xmlNode* node, WClothes::Part& part);
	

	MerchandiseItem*	parseMachandiseItem(xmlNode* node);

	ImageDataMap		m_ImageDataTable;
	
	ItemMap				m_itemMap;
	ClothesMap			m_clothesMap;

	MerchandiseMap		m_merchandiseMap;
	CosmeticDataVec		m_cosmeticVec;
	JobVec				m_jobTable;
};