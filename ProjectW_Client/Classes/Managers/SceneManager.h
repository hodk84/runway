#pragma once;

#include <map>
#include "cocos2d.h"
#include "Scenes\SceneBase.h"

class SceneManager // Singleton class
{
private : 
	SceneManager()	
		: m_pLastScene(NULL)
		, m_lastSceneType(kScene_None)
	{
		
	}

	static SceneManager* instance;

public :
	
	enum
	{
		kScene_None = 0,
		kScene_MainMenu = 1,
		kScene_MainStreet = 2,
		kScene_MyRoom = 3,
		kScene_ShopElevator = 4,
		kScene_Shop = 5,
		kScene_CareerElevator = 6,
		kScene_Career = 7,
		kScene_CatWalk = 8,
		kScene_ATM = 9,
		kScene_MyCloset = 10,
		kScene_Cafe = 11,
		kScene_Salon = 12,
		kScene_Club = 13,
		kScene_Town = 14,
		kScene_MainStreet2 = 15,
	};
	static SceneManager* getInst()
	{
		if( instance == NULL )
		{
			instance = new SceneManager();			
		}
		return instance;
	}
		
	int getCurrentSceneAsType()
	{
		return m_lastSceneType;
	}
	bool ChangeScene(int sceneType);
	SceneBase* getCurScene() { return m_pLastScene; }

	void enableCheat();
	void disableCheat();

private :

	SceneBase* getScene(int sceneType);
	
	SceneBase*				m_pLastScene;
	int						m_lastSceneType;

		
};