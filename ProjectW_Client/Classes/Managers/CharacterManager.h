#pragma once;

#include <map>
#include "cocos2d.h"
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>
#include "DrawDebug.h"
#include "WSprite.h"
#include "WCharacter.h"
#include "WClothes.h"
#include "Datas/UserInfo.h"

class CharacterManager // Singleton class
{
private : 
	CharacterManager()	
	{
		
	}

	static CharacterManager* instance;

public :
	static CharacterManager* getInst()
	{
		if( instance == NULL )
		{
			instance = new CharacterManager();			
		}
		return instance;
	}

	WCharacter* getMyCharacter()
	{			
		return m_pMyCharacter;
	}

	void resetMyCharacter()
	{
		if( m_pMyCharacter )
		{				
			CCNode* pCharacter = dynamic_cast<CCNode*>(CharacterManager::getInst()->getMyCharacter());
			if( NULL != pCharacter )
			{
				CCNode* pCharParent = pCharacter->getParent();
				if( NULL != pCharParent )
				{
					pCharParent->removeChild(pCharacter, true);
				}
			}
			m_pMyCharacter->applyClothes();
		}
	}

	WCharacter* getCharacter(unsigned int characterId);

	bool initialize();

	WCharacter* createCharacter(const CharacterStatus& data);
	WCharacter* loadCharacterByXml(const char* fileName);
	bool		addCharacterFromXml(const char* fileName);
	void		clearCharacterArray();
	
	bool loadCharacterConfig();	


	void		addUserInfo(UserInfo* usrInf);

	UserInfo*	getUserInfo(unsigned int id);	
	WCharacter* createCharacterFromUserInfo(unsigned int id);
		
	
private :

	CCMutableArray<WCharacter*>	m_characterArray;
	CCMutableArray<UserInfo*>	m_userArray;

	WCharacter*			m_pMyCharacter;
	cocos2d::CCPoint	m_charPos;
	cocos2d::CCPoint	m_charScale;
		
};