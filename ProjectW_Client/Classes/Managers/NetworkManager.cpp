#include "NetworkManager.h"
#include "win32/curl/curl.h"
#include "Managers/SceneManager.h"
#include "Managers/CharacterManager.h"
#include "Datas/UserInfo.h"

NetworkManager* NetworkManager::instance = NULL;

bool NetworkManager::initialize()
{
	connectionTest();
	getClubUserInfo();
	return true;
}

bool NetworkManager::connectionTest()
{
	CURL *curl;
	CURLcode res;
	char buffer[10];

	curl = curl_easy_init();
	
	char* url = "https://urlofapi"; // set REST URL 
	char* api_token = "apihashkey";
	char* xml = "<xml>";
	char* user_agent = "SoEasy REST API Client 0.1";

	char* filename = "test.xml"; 
	char* type = "Content-Type: application/xml";
	//handle = fopen($filename, "r"); 
	//$XPost = fread($handle, filesize($filename));
	//fclose($handle);

	curl_easy_setopt(curl, CURLOPT_POST, true); // Tell curl to use HTTP POST; 
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, xml); // Tell curl that this is the body of the POST 
	curl_easy_setopt(curl, CURLOPT_USERPWD, api_token); // define userpassword api token
	curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY); // defining REST basic authentication 
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, type); // define header 
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false); // ignore ssl cert for debug purposes curl_setopt($session, CURLOPT_USERAGENT, $user_agent); // user agent so the api knows what for some unknown reason 
	curl_easy_setopt(curl, CURLOPT_HEADER, 1); // Tell curl not to return headers, but do return the response 
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, true); // allow redirects curl_setopt($session, CURLOPT_FOLLOWLOCATION, true);

	CURLcode response = curl_easy_perform(curl);
	curl_easy_cleanup(curl);
	if( response != CURLE_OK )
	{
		return false;
	}
	return true;
}
struct MemoryStruct 
{ 
	char *memory;    size_t size;
};

static void *myrealloc(void *ptr, size_t size)
{    /* There might be a realloc() out there that doesn't like reallocing      
	 NULL pointers, so we take care of it here */   
	if(ptr) 
		return realloc(ptr, size);    
	else   
		return malloc(size);
}

std::string parseKey(char* str, int& idx, int size)
{
	std::string key;
	key.assign(str);

	int p2 = key.find("\":", idx);

	std::string result = key.substr(idx, p2 - idx);
	
	idx = p2+2;

	return result;
}

std::string parseValue(char* str, int& idx, int size)
{
	std::string value;
	value.assign(str);

	int p2 = value.find(",", idx);
	if( p2 == -1 )
	{
		p2 = value.find("}", idx);
	}

	std::string result = value.substr(idx, p2 - idx);
	
	while(1)
	{
		int ers = result.find("\"");
		if( ers != -1)
			result.erase(ers, 1);
		else
			break;
	}
	
	idx = ++p2;
	return result;
}

static size_t WriteMemoryCallback(void *ptr, size_t size, size_t nmemb, void *data) 
{   
	size_t realsize = size * nmemb;
	struct MemoryStruct *mem = (struct MemoryStruct *)data;   
	mem->memory = (char *)myrealloc(mem->memory, mem->size + realsize + 1); 
	if (mem->memory) 
	{  
		memcpy(&(mem->memory[mem->size]), ptr, realsize);  
		mem->size += realsize;
		mem->memory[mem->size] = 0;  
	} 

	std::string str;
	str.assign(mem->memory);

	std::string keyword;
	std::string value;
	int phase = 0;

	UserInfo* info = new UserInfo;

	for(int i = 2; i < str.size(); ++i)
	{
		std::string key = parseKey(mem->memory, i, str.size());
		std::string value = parseValue(mem->memory, i, str.size());

		if(key.compare("Id") == 0)
		{
			unsigned int id = atoi(value.c_str());
			info->Id = id;
		}
		else if(key.compare("email") == 0)
		{
			info->email = value;
		}
		else if(key.compare("userid") == 0)
		{
			info->userId = value;
		}
	}
	CharacterManager::getInst()->addUserInfo(info);
	WCharacter* pCharacter = CharacterManager::getInst()->createCharacterFromUserInfo(info->Id);
	SceneBase* pScene = SceneManager::getInst()->getCurScene();
	if( pScene )
	{
		pCharacter->tmpSetTagName(info->userId.c_str());
		pCharacter->setPosition(ccp(780.f, 300.f));
		pScene->addChild(pCharacter, 99);
	}
	
	return realsize;
}

bool NetworkManager::getClubUserInfo()
{
		CURL *curl;
	CURLcode res;	
	struct MemoryStruct chunk;
	chunk.memory=NULL; 
	chunk.size = 0;    

	curl = curl_easy_init();	
	curl_easy_setopt(curl, CURLOPT_URL, "http://218.234.17.97:8080/Service1/1");
	curl_easy_setopt(curl, CURLOPT_NOPROGRESS , true) ;
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);  

	const CURLcode rc = curl_easy_perform(curl);
	if (CURLE_OK != rc)
	{   
		
	}
	else
	{   
		double statDouble ;
		long statLong ;
		char* statString = NULL ;
		// HTTP 응답코드를 얻어온다. 
		if( CURLE_OK == curl_easy_getinfo( curl , CURLINFO_HTTP_CODE , &statLong ) ){
			
		}
		// Content-Type 를 얻어온다.
		if( CURLE_OK == curl_easy_getinfo( curl , CURLINFO_CONTENT_TYPE , &statString ) ){
			
		}
		// 다운로드한 문서의 크기를 얻어온다.
		if( CURLE_OK == curl_easy_getinfo( curl , CURLINFO_SIZE_DOWNLOAD , &statDouble ) ){
			
		}
		// 
		if( CURLE_OK == curl_easy_getinfo( curl , CURLINFO_SPEED_DOWNLOAD , &statDouble ) ){
			
		}		
	}

    curl_easy_cleanup( curl ) ;
   
	return true;
}