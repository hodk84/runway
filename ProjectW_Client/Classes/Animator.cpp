#include "Animator.h"
#include "WSkeleton.h"

#include "XmlUtils.h"

bool KeyAnimation::ParseXmlKeyAnim(xmlNodePtr pNode)
{	
	if(0 != xmlStrcmp(pNode->name, (const xmlChar *)"Keyframe"))
	return false;
	
	_beginFrame = GetPropertyInt(pNode, "beginFrame");
	_endFrame = GetPropertyInt(pNode, "endFrame");	

	float rotation = GetPropertyFloat(pNode, "rotation");
	if( rotation )
	{
		_rotate.setValue( rotation );	
	}
	float x = GetPropertyFloat(pNode, "x");
	if( x )
	{
		_moveX.setValue(x);	
	}

	float y = GetPropertyFloat(pNode, "y");
	if( y )
	{
		_moveX.setValue(y);	
	}

	/* For extended properties;
	xmlNode* child = pNode->children;
	while(child)
	{	
		//tween, ease
		child = child->next;
	}*/

	return true;
}

///////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////

bool AnimationSet::ParseAnimationSet(xmlNode* pMotion)
{	
	if(0 == xmlStrcmp(pMotion->name, (const xmlChar *)"Motion"))
	{	
		m_frameRate = GetPropertyInt(pMotion, "frameRate");
		if( m_frameRate == 0)
			m_frameRate = 24;

		m_nodeName = GetPropertyString(pMotion, "symbolName");
		
		m_initX = GetPropertyFloat(pMotion, "x");
		m_initY = GetPropertyFloat(pMotion, "y");
		m_initRot = GetPropertyFloat(pMotion, "rot");

		xmlNode* pKeyframe = pMotion->children;
		while(pKeyframe)	
		{
			KeyAnimation* anim = new KeyAnimation;
			if( anim->ParseXmlKeyAnim(pKeyframe) )
			{
				anim->_frameRate = m_frameRate;
				anim->_nodeName = m_nodeName;
				m_AnimVec.push_back(anim);
			}
			pKeyframe = pKeyframe->next;
		}
	}
	else
	{
		return false;
	}
		
	return true;
}

bool AnimationSet::PlayKeyAnimation(KeyAnimationTarget* pTarget)
{
	if( m_frameRate <= 0 ) return false;
	if( pTarget == NULL ) return false;
	if( m_AnimVec.empty()) return false;

	const KeyAnimation* animation = m_AnimVec[0];

	m_destFrame =  animation->_beginFrame;
	m_curSequence = 0;	
	m_curFrame = 0;

	m_curAnimTarget = pTarget;	
	m_curAnimTarget->startKeyAnimation(m_nodeName, m_initRot);
	float frameGap = 1.f/m_frameRate;		
	CCScheduler::sharedScheduler()->scheduleSelector(schedule_selector(AnimationSet::update), this, frameGap, false);		
	
	return true;
}


void AnimationSet::tick(ccTime dt)
{
	printf("Frames:%d\n", m_curFrame);
}

void AnimationSet::update(ccTime dt)
{
	// 무조건 첫번째 시퀀스로 부터 시작함.
	// 0프레임 부터 시작하는 애니메이션은 문제가 없으나
	// 특정 프레임 이후 시작하는 애니메이션은 미리 시작하는 형태가 되어버린다.
	// m_destFrame을 먼저 첫번째 시퀀스의 시작 시퀀스로 세팅해야함.
	// playKeyAnimation()함수에 관련 코드를 추가하면 될것 같다.
	if( m_destFrame <= m_curFrame)
	{	
		if( m_curSequence >= m_AnimVec.size() )
		{
			m_curSequence = 0;
			CCScheduler::sharedScheduler()->unscheduleAllSelectorsForTarget(this);
			
			return;
		}	

		const KeyAnimation* pAnimation = m_AnimVec[m_curSequence];
		
		float durFrame = pAnimation->_endFrame - pAnimation->_beginFrame;
		float duration =  durFrame * (1.f/(float)m_frameRate);	
		
		m_curFrame = pAnimation->_beginFrame;
		m_destFrame = pAnimation->_endFrame;

		m_curAnimTarget->startKeyAnimation(pAnimation, duration);
		++m_curSequence;		
	}
	++m_curFrame;
}

///////////////////////////////////////////////////////
NodeAnimator::NodeAnimator()
{

}

NodeAnimator::~NodeAnimator()
{

}

bool WAnimation::LoadAnimation(const char* fileName)
{
	const char* animFilePath = CCFileUtils::fullPathFromRelativePath(fileName);
	
	xmlDoc *animDoc = NULL;
	xmlNode *root_anim_element = NULL;
	animDoc = xmlReadFile(animFilePath, NULL, 0);	
	if( NULL == animDoc )
	{
		return false;
	}
	root_anim_element = xmlDocGetRootElement(animDoc);
	if( NULL == root_anim_element )
	{
		return false;
	}
	
	xmlNode* pMotion = root_anim_element->children;
	while(pMotion)
	{
		AnimationSet* animSet = new AnimationSet;
		if( animSet->ParseAnimationSet(pMotion) )
		{
			m_animSetVec.push_back(animSet);
		}
		pMotion = pMotion->next;
	}
	return true;
}

void WAnimation::PlayAnimation()
{
	AnimSetVec::iterator itrAnim = m_animSetVec.begin();
	for( itrAnim; itrAnim!=m_animSetVec.end(); ++itrAnim)
	{
		(*itrAnim)->PlayKeyAnimation(m_pAnimTarget);
	}
}