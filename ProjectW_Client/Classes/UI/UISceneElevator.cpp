
#include "UISceneElevator.h"
#include "Managers\CharacterManager.h"
#include "Managers/SceneManager.h"
#include "UICtrlItemTag.h"
#include "CCMask.h"

UISceneElevator::UISceneElevator()
: m_fTick(0.f)
, m_bAccelation(false)	
, m_fAccelPerTime(0.f)
, m_touchBeginPoint(ccp(0.f, 0.f))
, m_touchBeginTime(0.f)
, m_rootNode(NULL)
, m_fSpring(0.f)
, m_fVelo(0.f)
, m_result(NULL)
, m_pMask(NULL)
, m_selectedButton(ButtonID_1st)
{
	
}

UISceneElevator::~UISceneElevator()
{

}

bool UISceneElevator::init()
{
	bool bRet = false;
    do 
    {
		this->setAnchorPoint(ccp(0.f, 0.f));

		std::vector<CCMenuItemImage*> btnVec;
		int x = -3; 
		int y = -23;
		int add = -106;
		{
			CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
			"ui\\common_elevator_5.png","ui\\common_elevator_5_1.png",
			this, menu_selector(UISceneElevator::menuBackCallback));
				
			pButton->setPosition(ccp(x, y));
			pButton->setTag(ButtonID_5th);
			btnVec.push_back(pButton);
		}
		y += add;
		{
			CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
			"ui\\common_elevator_4.png","ui\\common_elevator_4_1.png",
			this, menu_selector(UISceneElevator::menuBackCallback));
		
			pButton->setPosition(ccp(x, y));
			pButton->setTag(ButtonID_4th);
			btnVec.push_back(pButton);
		}
		y += add;
		
		{
			CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
			"ui\\common_elevator_3.png","ui\\common_elevator_3_1.png",
			this, menu_selector(UISceneElevator::menuBackCallback));
		
			pButton->setPosition(ccp(x, y));
			pButton->setTag(ButtonID_3rd);
			btnVec.push_back(pButton);
		}
		y += add;
		{
			CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
			"ui\\common_elevator_2.png","ui\\common_elevator_2_1.png",
			this, menu_selector(UISceneElevator::menuBackCallback));
					
			pButton->setPosition(ccp(x, y));
			pButton->setTag(ButtonID_2nd);			
			btnVec.push_back(pButton);
		}
		y += add;
		{
			CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
			"ui\\common_elevator_1.png","ui\\common_elevator_1_1.png",
			this, menu_selector(UISceneElevator::menuBackCallback));		
		
			pButton->setPosition(ccp(x, y));
			pButton->setTag(ButtonID_1st);			
			btnVec.push_back(pButton);		
		}

		{
			m_pTitle = CCLabelTTF::labelWithString("", "Thonburi", 40);
			m_pTitle->setPosition(ccp(425.f, 480));
			addChild(m_pTitle, 10);
		}		
		{
			m_pDesc = CCLabelTTF::labelWithString("", "Thonburi", 32);
			m_pDesc->setPosition(ccp(380, 350));
			addChild(m_pDesc, 10);
		}
		{
			CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
			"ui\\common_admission.png","ui\\common_admission_1.png",
			this, menu_selector(UISceneElevator::menuBackCallback));		
		
			pButton->setPosition(ccp(340, -420));
			pButton->setTag(ButtonID_Admission);			
			btnVec.push_back(pButton);		
		}
		
		m_pCategory = CCMenu::menuWithItems(btnVec[0],btnVec[1], btnVec[2], btnVec[3],btnVec[4],btnVec[5], NULL);
		m_pCategory->setPosition(ccp(60, 480));		
		addChild(m_pCategory);
		setIsTouchEnabled( true );
		bRet = true;
	
		setSelectFloor(ButtonID_1st);

	} while(0);

	return bRet;
}

void UISceneElevator::menuBackCallback(CCObject* pSender)
{
	CCMenuItemImage* item = dynamic_cast<CCMenuItemImage*>(pSender);
	int tag = item->getTag();
	if( tag == ButtonID_Admission )
	{
		loadItems(m_selectedButton);
	}
	else
	{
		setSelectFloor(item->getTag());
	}
}

void UISceneElevator::loadItems(int type)
{	
	int scene = SceneManager::getInst()->getInst()->getCurrentSceneAsType();
	if( scene == SceneManager::kScene_CareerElevator ) 
	{
		SceneManager::getInst()->ChangeScene(SceneManager::kScene_Career);
	}
	else if(scene == SceneManager::kScene_ShopElevator ) 
	{
		SceneManager::getInst()->ChangeScene(SceneManager::kScene_Shop);
	}
}

void UISceneElevator::selectItemCallback(CCObject* pSender)
{

}

void UISceneElevator::buyItemCallback(CCObject* pSender)
{

}
	
void UISceneElevator::visit()
{
	
//	//if( m_result )
//		//m_result->visit();
//	/*CCRenderTexture* rt = CCRenderTexture::renderTextureWithWidthAndHeight((int)m_maskNode->getContentSize().width, (int)m_maskNode->getContentSize().height); 
//	rt->begin();
//	m_rootNode->visit();
//	m_maskNode->visit();
//	rt->end();
//	CCSprite* pOutcome = CCSprite::spriteWithTexture(rt->getSprite()->getTexture()); 
//	pOutcome->setFlipY(true);
//	pOutcome->setPosition(ccp(0, 0));
//	pOutcome->visit();*/
	
	
	UISceneBase::visit();
}


void UISceneElevator::registerWithTouchDispatcher()
{
    CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
}

bool UISceneElevator::ccTouchBegan(CCTouch* touch, CCEvent* event)
{	
	m_fSpring = 0.f;
	m_touchBeginPoint = touch->locationInView(0);
	m_touchBeginTime = m_fTick;
	return true;
}

void UISceneElevator::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	CCPoint ptEnd = touch->locationInView(0);	
	m_fAccelPerTime = (ptEnd.x - m_touchBeginPoint.x) / (m_fTick - m_touchBeginTime);
	if( m_fAccelPerTime )
	{
		//m_pBuildingMenu->ccTouchCancelled(touch, event);
	}
	else
	{
		//m_pBuildingMenu->ccTouchEnded(touch, event);
	}
}

void UISceneElevator::ccTouchCancelled(CCTouch* touch, CCEvent* event)
{
}

void UISceneElevator::ccTouchMoved(CCTouch* touch, CCEvent* event)
{
	if( NULL == m_pMenu ) return;

	CCPoint touchLocation = touch->locationInView( touch->view() );	
	CCPoint prevLocation = touch->previousLocationInView( touch->view() );	

	touchLocation = CCDirector::sharedDirector()->convertToGL( touchLocation );
	prevLocation = CCDirector::sharedDirector()->convertToGL( prevLocation );

	CCPoint diff = ccpSub(touchLocation,prevLocation);
	diff.y = 0.f;
	//CCNode* pNode = this->getChildByTag(10);
	CCPoint currentPos = m_pMenu->getPosition();
	
	//CCPoint pt = ccpAdd(currentPos, diff);	
	//if( pt.x < -1840)
	//{
	//	float offset = pt.x + 1840;	

	//	if( diff.x < -50.f) diff.x = -50;
	//	diff.x *= 1-(offset / -50.f);
	//}
	//if( pt.x > -10)
	//{
	//	float offset = pt.x - 10;
	//	
	//	if( diff.x > 50.f) diff.x = 50.f;
	//	diff.x *= 1-(offset / 50.f);
	//}
	
	CCPoint rst = ccpAdd(currentPos, diff);
	m_pMenu->setPosition(rst);

	if( m_pMask )
	{
		m_pMask->maskWithClear(0.f, 0.f, 0.f, 0.f);
		m_pMask->reDrawMask();
	}
}

void UISceneElevator::setSelectFloor(int buttonID)
{
	ccArray *arrayData = m_pCategory->getChildren()->data;
    for(int i=0 ; i < arrayData->num; i++ )
    {
		CCMenuItemImage* pNode = (CCMenuItemImage*) arrayData->arr[i];
		if( pNode->getTag() == buttonID )
		{
			pNode->selected();
		}
		else
		{
			pNode->unselected();
		}
	}
	m_selectedButton = buttonID;

	switch(buttonID)
	{
	case ButtonID_1st :
		m_pTitle->setString("Alba 1");
		m_pDesc->setString("low pay, hard works");
		break;
	case ButtonID_2nd :
		m_pTitle->setString("Alba 2");
		m_pDesc->setString("low pay, easy works");
		break;
	case ButtonID_3rd :
		m_pTitle->setString("Alba 3");
		m_pDesc->setString("no pay, hard works\n\n bongsahwaldong");
		break;
	case ButtonID_4th :
		m_pTitle->setString("Alba 4");
		m_pDesc->setString("low pay, easy works");
		break;
	case ButtonID_5th :
		m_pTitle->setString("Jungjikwon");
		m_pDesc->setString("high pay, easy works");
		break;
	}
}