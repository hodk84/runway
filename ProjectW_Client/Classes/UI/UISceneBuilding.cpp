#include "UISceneBuilding.h"
#include "Scenes/SceneHome.h"
#include "Managers/SceneManager.h"

UISceneBuilding::UISceneBuilding()
{
	init();
}

UISceneBuilding::~UISceneBuilding()
{

}

bool UISceneBuilding::init()
{
	float x = -60.f;
	float y = 170.f;
	
	std::vector<CCMenuItem*> ButtonVec;
	{
		CCMenuItemImage *pBackButton = CCMenuItemImage::itemFromNormalImage(
       "Building\\home.png","Building\\home_down.png",
       this, menu_selector(UISceneBuilding::menuBackCallback));
	
		pBackButton->setAnchorPoint(ccp(0.f, 0.f));
		pBackButton->setPosition(ccp(x, y));
		pBackButton->setTag(ButtonID_Home);
		//m_pMenu->addChild(pBackButton, 1);
		CCSize size = pBackButton->getContentSize();
		x += size.width;
		ButtonVec.push_back(pBackButton);
	}
	{
		CCMenuItemImage *pBackButton = CCMenuItemImage::itemFromNormalImage(
       "Building\\office.png","Building\\office_down.png",
       this, menu_selector(UISceneBuilding::menuBackCallback));
	
		pBackButton->setAnchorPoint(ccp(0.f, 0.f));
		pBackButton->setPosition(ccp(x, 185));
		pBackButton->setTag(ButtonID_Office);
		//m_pMenu->addChild(pBackButton, 1);
		CCSize size = pBackButton->getContentSize();
		x += size.width;
		ButtonVec.push_back(pBackButton);
	}
	{
		CCMenuItemImage *pBackButton = CCMenuItemImage::itemFromNormalImage(
       "Building\\shop.png","Building\\shop_down.png",
       this, menu_selector(UISceneBuilding::menuBackCallback));
	
		pBackButton->setAnchorPoint(ccp(0.f, 0.f));
		pBackButton->setPosition(ccp(x, 190));
		pBackButton->setTag(ButtonID_Shop);
		//m_pMenu->addChild(pBackButton, 1);
		CCSize size = pBackButton->getContentSize();
		x += size.width;
		ButtonVec.push_back(pBackButton);
	}
	{
		CCMenuItemImage *pBackButton = CCMenuItemImage::itemFromNormalImage(
       "Building\\atm.png","Building\\atm_down.png",
       this, menu_selector(UISceneBuilding::menuBackCallback));
	
		pBackButton->setAnchorPoint(ccp(0.f, 0.f));
		pBackButton->setPosition(ccp(x, 200));
		pBackButton->setTag(ButtonID_Atm);
		//m_pMenu->addChild(pBackButton, 1);
		CCSize size = pBackButton->getContentSize();
		x += size.width;
		ButtonVec.push_back(pBackButton);
	}
	{
		CCMenuItemImage *pBackButton = CCMenuItemImage::itemFromNormalImage(
       "Building\\club.png","Building\\club_down.png",
       this, menu_selector(UISceneBuilding::menuBackCallback));
	
		pBackButton->setAnchorPoint(ccp(0.f, 0.f));
		pBackButton->setPosition(ccp(x, 170));
		pBackButton->setTag(ButtonID_Club);
		//m_pMenu->addChild(pBackButton, 1);
		CCSize size = pBackButton->getContentSize();
		x += size.width;
		ButtonVec.push_back(pBackButton);
	}
	{
		CCMenuItemImage *pBackButton = CCMenuItemImage::itemFromNormalImage(
       "Building\\catwalk.png","Building\\catwalk_down.png",
       this, menu_selector(UISceneBuilding::menuBackCallback));
			
		pBackButton->setAnchorPoint(ccp(0.f, 0.f));
		pBackButton->setPosition(ccp(x, 190));
		pBackButton->setTag(ButtonID_CatWalk);
		//m_pMenu->addChild(pBackButton, 1);
		CCSize size = pBackButton->getContentSize();
		x += size.width;
		ButtonVec.push_back(pBackButton);
	}
	{
		CCMenuItemImage *pBackButton = CCMenuItemImage::itemFromNormalImage(
       "Building\\salon.png","Building\\salon_down.png",
       this, menu_selector(UISceneBuilding::menuBackCallback));
	
		pBackButton->setAnchorPoint(ccp(0.f, 0.f));
		pBackButton->setPosition(ccp(x, 190));
		pBackButton->setTag(ButtonID_Salon);
		//m_pMenu->addChild(pBackButton, 1);
		CCSize size = pBackButton->getContentSize();
		x += size.width;
		ButtonVec.push_back(pBackButton);
	}
	{
		CCMenuItemImage *pBackButton = CCMenuItemImage::itemFromNormalImage(
       "Building\\cafe.png","Building\\cafe_down.png",
       this, menu_selector(UISceneBuilding::menuBackCallback));
	
		pBackButton->setAnchorPoint(ccp(0.f, 0.f));
		pBackButton->setPosition(ccp(x, 200));
		pBackButton->setTag(ButtonID_Cafe);
		//m_pMenu->addChild(pBackButton, 1);
		CCSize size = pBackButton->getContentSize();
		x += size.width;
		ButtonVec.push_back(pBackButton);
	}
	setAnchorPoint(ccp(0,0));
	m_pMenu = DragableMenu::menuWithItems(ButtonVec);
	m_pMenu->setPosition(ccp(0, 10));
	addChild(m_pMenu);
	return true;
}

void UISceneBuilding::menuBackCallback(CCObject* pSender)
{
	CCMenuItemImage* item = dynamic_cast<CCMenuItemImage*>(pSender);
	if( item )
	{
		int tag = item->getTag();		
		if( UISceneBuilding::ButtonID_Home == tag )
		{
			SceneManager::getInst()->ChangeScene(SceneManager::kScene_MyCloset);
		}
		else if( UISceneBuilding::ButtonID_Shop == tag )
		{
			SceneManager::getInst()->ChangeScene(SceneManager::kScene_ShopElevator);
		}
		else if( UISceneBuilding::ButtonID_Office == tag )
		{
			SceneManager::getInst()->ChangeScene(SceneManager::kScene_CareerElevator);
		}
		else if( UISceneBuilding::ButtonID_CatWalk == tag )
		{
			SceneManager::getInst()->ChangeScene(SceneManager::kScene_CatWalk);
		}
		else if( UISceneBuilding::ButtonID_Atm == tag )
		{
			SceneManager::getInst()->ChangeScene(SceneManager::kScene_ATM);
		}
		else if( UISceneBuilding::ButtonID_Cafe == tag )
		{
			SceneManager::getInst()->ChangeScene(SceneManager::kScene_Cafe);
		}
		else if( UISceneBuilding::ButtonID_Salon == tag )
		{
			SceneManager::getInst()->ChangeScene(SceneManager::kScene_Salon);
		}
		else if( UISceneBuilding::ButtonID_Club == tag )
		{
			SceneManager::getInst()->ChangeScene(SceneManager::kScene_Club);
		}
	
		return;
	}
}