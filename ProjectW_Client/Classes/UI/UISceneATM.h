#pragma once
#include "UISceneBase.h"

class CCMask;
class UICtrlItemTag;
class UISceneATM : public UISceneBase
{
public : 

	static const int TagMarginX = 3;
	static const int TagMarginY = 2;

	static const int ButtonID_All = 201;
	static const int ButtonID_Dress = 202;
	static const int ButtonID_Top = 203;
	static const int ButtonID_Bottom = 204;
	static const int ButtonID_Shoes = 205;
	static const int ButtonID_Accessory = 206;	

	UISceneATM();
	virtual ~UISceneATM();

	 virtual bool init();
	void loadItems(int type);
	
	void registerWithTouchDispatcher();
	virtual void visit();
	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);
	virtual void ccTouchCancelled(CCTouch* touch, CCEvent* event);
	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);

protected :
	void menuBackCallback(CCObject* pSender);
	void selectItemCallback(CCObject* pSender);
	void buyItemCallback(CCObject* pSender);
	virtual void update(ccTime dt);

	void resetShowcase();

	float	m_fTick;
	bool	m_bAccelation;	
	float	m_fAccelPerTime;
	CCPoint	m_touchBeginPoint;
	float	m_touchBeginTime;
	
	CCSprite*	m_maskNode;	
	CCSprite*	m_rootNode;
	CCSprite*	m_result;

	CCSprite*	m_pMaskFront;
	CCSprite*	m_pMaskEnd;

	CCMask*		m_pMask;

	float	m_fSpring;
	float	m_fVelo;

	int		m_edgeBegin;
	int		m_edgeEnd;

	UICtrlItemTag* m_pSelectedItem;
};
	