#pragma once
#include "UISceneBase.h"

class WCharacter;
class UICtrlProgressBar;
class UISceneTopMenu : public UISceneBase
{
public : 
	static const int ButtonID_Back = 101;	

public : 
	UISceneTopMenu();
	virtual ~UISceneTopMenu();

	virtual bool init();
	virtual void update(ccTime dt);
	
	void	updateStatus();

protected :
	void menuBackCallback(CCObject* pSender);
	CCLabelTTF* m_pTimer;
	
	CCLabelTTF* m_labelGameMoeny;
	CCLabelTTF* m_labelCash;
	CCLabelTTF* m_labelLevel;
	CCLabelTTF* m_labelClothes;
	CCLabelTTF* m_labelEnergy;
	
	float m_fTick;
	UICtrlProgressBar* m_bar;

	WCharacter* m_myCharacter;
};	