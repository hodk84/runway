
#include "UICtrlProgressBar.h"
#include "WSprite.h"


void UICtrlProgressBar::setBarImageFile(const char* fileName)
{
	this->setAnchorPoint(ccp(0, 0));
	m_pBarSprite = WSprite::WSpriteWithFile(fileName);
	if( m_pBarSprite )
	{
		m_pBarSprite->setAnchorPoint(ccp(0, 0));
		m_originalWidth = m_pBarSprite->getContentSize().width;
		m_pBarSprite->setScaleX(1.f);
		m_pBarSprite->setScaleY(1.f);
		m_pBarSprite->setPosition(ccp(-2, -2));
		addChild(m_pBarSprite);
	}
	setProgress(1.f);
}
float UICtrlProgressBar::getCurrentProgress()
{
	return m_fProgress ;
}

void UICtrlProgressBar::setProgress(float progress)
{
	if( m_fProgress == progress ) return;
	
	CCAction* action1 = NULL;
	if( progress > 1.f )
	{
		CCActionInterval* a1 = CCScaleTo::actionWithDuration(m_fSpeed, progress, 1);
		CCActionInterval* a2 = CCScaleTo::actionWithDuration(0.1f, 0.f, 1);

		action1 = (CCActionInterval*)(CCSequence::actions(a1, a2, NULL));		
	}
	else
	{
		CCActionInterval* a1 = CCScaleTo::actionWithDuration(m_fSpeed, progress, 1);

		action1 = (CCActionInterval*)(CCSequence::actions(a1, NULL));		
	}
	runAction(action1);
	m_fProgress = progress;	
}