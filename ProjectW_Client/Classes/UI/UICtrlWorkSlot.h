#pragma once;

#include <cocos2d.h>
#include "Datas/Work_N_Career.h"

using namespace cocos2d;

class UICtrlProgressBar;
class UICtrlWorkSlot : public CCNode
{
public :
				UICtrlWorkSlot();
	virtual		~UICtrlWorkSlot();

	virtual void init();
	void		menuWorkCallback(CCObject* pSender);
	
	void		setMasterStar(unsigned int masterLevel);
	void		clearMasterStar();

	void		setWorkInfo(const JobData& info);	

protected : 
	void		popStar(CCNode* base);

	CCSprite*		m_masterStar1;
	CCSprite*		m_masterStar2;
	CCSprite*		m_masterStar3;

	CCParticleSystem* m_emitter;

	CCSprite*	m_background;
	CCMenuItemImage* m_btnDoWork;

	JobData		m_jobData;
	UICtrlProgressBar* m_bar;

	CCLabelTTF*		m_labelPay;
	CCLabelTTF*		m_labelDayPoint;
	CCLabelTTF*		m_labelEnergy;
};