#include "UISceneShowcase.h"
#include "Managers\CharacterManager.h"
#include "Managers/SceneManager.h"
#include "Managers/TableManager.h"
#include "UICtrlItemTag.h"
#include "CCMask.h"
#include "UICtrlMessageBox.h"

UISceneShowcase::UISceneShowcase()
: m_fTick(0.f)
, m_bAccelation(false)	
, m_fAccelPerTime(0.f)
, m_touchBeginPoint(ccp(0.f, 0.f))
, m_touchBeginTime(0.f)
, m_rootNode(NULL)
, m_fSpring(0.f)
, m_fVelo(0.f)
, m_result(NULL)
, m_pMask(NULL)
, m_edgeBegin(0)
, m_edgeEnd(0)
, m_pMaskFront(NULL)
, m_pMaskEnd(NULL)
, m_pSelectedItem(NULL)
{
	
}

UISceneShowcase::~UISceneShowcase()
{
	CCTouchDispatcher::sharedDispatcher()->removeAllDelegates();
}

bool UISceneShowcase::init()
{
	bool bRet = false;
    do 
    {
		m_pMaskFront = CCSprite::spriteWithFile("UI\\shop_cover_l.png");
		m_pMaskFront->setAnchorPoint(ccp(0.f, 0.f));
		m_pMaskFront->setPosition(ccp(3, -23));
		m_pMaskFront->setIsVisible(false);
		addChild(m_pMaskFront, 10);

		m_pMaskEnd = CCSprite::spriteWithFile("UI\\shop_cover_r.png");
		m_pMaskEnd->setAnchorPoint(ccp(0.f, 0.f));
		m_pMaskEnd->setPosition(ccp(574, -23));
		addChild(m_pMaskEnd, 10);
	
		this->setAnchorPoint(ccp(0.f, 0.f));
		setIsTouchEnabled( true );
		this->schedule( schedule_selector(UISceneShowcase::update) ); 

		m_pMenu = DragableMenu::menu();
		if( NULL == m_pMenu ) return NULL;
		
		m_pMenu->setAnchorPoint(ccp(0, 0));
		m_pMenu->setPosition(ccp(10, 0));
		addChild(m_pMenu);
		
		bRet = true;

	} while(0);

	return bRet;
}

void UISceneShowcase::resetShowcase()
{	
	m_pSelectedItem = NULL;
	if( m_pMenu )
	{
		m_pMenu->setPosition(ccp(10, 0));
		m_pMenu->removeAllChildrenWithCleanup(true);
	}
	if( m_pMaskFront )
	{
		m_pMaskFront->setIsVisible(false);
	}

}
void UISceneShowcase::addItemTag(UICtrlItemTag* pTag)
{
	if( NULL == pTag || NULL == m_pMenu)
	{
		return;
	}
	
	int count = 0;
	if( m_pMenu->getChildren() )
	{
		count = m_pMenu->getChildren()->count();
	}

	int marginX = pTag->getContentSize().width + TagMarginX;
	int marginY = pTag->getContentSize().height + TagMarginY;

	int ppx = 5+(count/2)*marginX;
	int ppy = 205+(count%2)*(-marginY);
		
	m_edgeEnd = ppx + marginX;

	pTag->setAnchorPoint(ccp(0, 0));
	pTag->setPosition(ccp(ppx, ppy));		
	
	m_pMenu->addChild(pTag, 1);
}

void UISceneShowcase::loadItems(int type, SelectorProtocol* protocol, SEL_MenuHandler selector, bool abilitySlot)
{	
	return;
	resetShowcase();

	MerchandiseItemVec items;
	TableManager::getInst()->getMerchandiseSet(1, type, items);
	
	if( items.empty() ) return;

	MerchandiseItemVec::iterator itr = items.begin();	
	std::vector<CCMenuItem*> tagVec;
	int i = 0;
	m_edgeEnd = 0;
	for( itr; itr != items.end(); ++itr, ++i)
	{
		UICtrlItemTag* pTag = UICtrlItemTag::itemFromNormalImage("UI\\shop_slot_01.png", "UI\\shop_slot_01_o.png",
			NULL, protocol, selector);
		if( NULL == pTag ) continue;
		pTag->setInfo((*itr));
		pTag->setTag((*itr)->merchandiseID);
		int marginX = pTag->getContentSize().width + TagMarginX;
		int marginY = pTag->getContentSize().height + TagMarginY;

		int ppx = 5+(i/2)*marginX;
		int ppy = 205+(i%2)*(-marginY);
		
		m_edgeEnd = ppx + marginX;

		pTag->setAnchorPoint(ccp(0, 0));
		pTag->setPosition(ccp(ppx, ppy));		
		tagVec.push_back(pTag);		
	}	
	m_pMenu = DragableMenu::menuWithItems(tagVec);
	m_pMenu->setAnchorPoint(ccp(0, 0));
	m_pMenu->setPosition(ccp(0, 0));
	addChild(m_pMenu);
	
	
	if( m_edgeEnd > 660 )
	{		
		if( m_pMaskEnd )
			m_pMaskEnd->setIsVisible(true);
	}	
}

void UISceneShowcase::selectItemCallback(CCObject* pSender)
{
	UICtrlItemTag* item = dynamic_cast<UICtrlItemTag*>(pSender);
	if( item )
	{
		if( m_pSelectedItem )		
			m_pSelectedItem->unselected();
		
		item->selected();
		m_pSelectedItem = item;
		unsigned int merchandiseID = item->getMerchandiseID();
		MerchandiseItem* pMech = TableManager::getInst()->getMerchandise(1, merchandiseID);
		if( NULL == pMech ){ assert(0); return; }
		
		WCharacter* pMyCharacter = CharacterManager::getInst()->getMyCharacter();
		if( pMyCharacter )
		{
			pMyCharacter->setClothesApply(pMech->itemID);
		}

		//UICtrlMessageBox::CreateMessageBox("Buy?", 1);
	}
}

void UISceneShowcase::registerWithTouchDispatcher()
{
    CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
}

bool UISceneShowcase::ccTouchBegan(CCTouch* touch, CCEvent* event)
{	
	m_fSpring = 0.f;
	m_touchBeginPoint = touch->locationInView(0);
	m_touchBeginTime = m_fTick;
	return true;
}

void UISceneShowcase::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	CCPoint ptEnd = touch->locationInView(0);	
	m_fAccelPerTime = (ptEnd.x - m_touchBeginPoint.x) / (m_fTick - m_touchBeginTime);
	if( m_fAccelPerTime )
	{
		if( m_pMenu )
			m_pMenu->ccTouchCancelled(touch, event);
	}
	else
	{
		if( m_pMenu )
			m_pMenu->ccTouchEnded(touch, event);
	}
}

void UISceneShowcase::ccTouchCancelled(CCTouch* touch, CCEvent* event)
{
}

void UISceneShowcase::ccTouchMoved(CCTouch* touch, CCEvent* event)
{
	if( NULL == m_pMenu ) return;

	CCPoint touchLocation = touch->locationInView( touch->view() );	
	CCPoint prevLocation = touch->previousLocationInView( touch->view() );	

	touchLocation = CCDirector::sharedDirector()->convertToGL( touchLocation );
	prevLocation = CCDirector::sharedDirector()->convertToGL( prevLocation );

	CCPoint diff = ccpSub(touchLocation,prevLocation);
	diff.y = 0.f;
	//CCNode* pNode = this->getChildByTag(10);
	CCPoint currentPos = m_pMenu->getPosition();	
	

	//왼쪽에 상품 더 있음

	
	if( currentPos.x + diff.x < -(m_edgeEnd-100))
	{
		diff.x *= 1-(diff.x /50.f);
	}
	else if( currentPos.x + diff.x > 550 )
	{
		diff.x *= 1-(diff.x/50.f);
	}

	

	CCPoint rst = ccpAdd(currentPos, diff);
	m_pMenu->setPosition(rst);

	if( rst.x <= 0 )
	{
		if( m_pMaskFront )
			m_pMaskFront->setIsVisible(true);		
	}
	else
	{
		if( m_pMaskFront )
			m_pMaskFront->setIsVisible(false);
	}

	//오른쪽에 상품 더 있음
	if( rst.x + m_edgeEnd > 660 )
	{		
		if( m_pMaskEnd )
			m_pMaskEnd->setIsVisible(true);
	}
	else
	{
		if( m_pMaskEnd )
			m_pMaskEnd->setIsVisible(false);
	}

	//if( m_pMask )
	//{
	//	m_pMask->maskWithClear(0.f, 0.f, 0.f, 0.f);
	//	m_pMask->reDrawMask();
	//}
}

 void UISceneShowcase::update(ccTime dt)
 {	
	 if( fabs(m_fAccelPerTime) > 0.01f )
	 {
		CCNode* pNode = m_pMenu;
		CCPoint pt = pNode->getPosition();
		pt.x += m_fAccelPerTime*dt;
		
		if( pt.x <= -(m_edgeEnd-100) ) // 왼쪽 제한에 걸림
		{
			m_fVelo = 0.f;
			m_fSpring = -(m_edgeEnd-100);
			m_fAccelPerTime = 0.f;
		}
		if( pt.x >= 550) // 오른쪽 제한에 걸림.
		{
			m_fVelo = 0.f;
			m_fSpring = 550;
			m_fAccelPerTime = 0.f;
		}

		m_fAccelPerTime *= 0.9f;//감속
		pNode->setPosition(pt);
		if( fabs(m_fAccelPerTime) < 0.1f)
		{
			m_bAccelation = false;
		}

		pNode->setPosition(pt);
		
	 }
	 
	if( m_fSpring != 0.f )
	{
		const float k = 2;
		const float m = 100;
		const float d = 0.92f;
	
		CCNode* pNode = m_pMenu;
		CCPoint pt = pNode->getPosition();
		float dx = m_fSpring - pt.x;		
		float ax = k/m*dx;	
		
		m_fVelo += ax;		
		m_fVelo *= d;
		if( fabs(m_fVelo) < 0.01f)
		{
			m_fSpring = 0.f;
		}

		pt.x += m_fVelo;
		pNode->setPosition(pt);
	}

	m_fTick += dt;

	
 }

 
void UISceneShowcase::setSelectedItem(UICtrlItemTag* pItem)
{
	if( m_pSelectedItem )		
	{
		m_pSelectedItem->unselected();
	}
	if( pItem )
	{
		pItem->selected();
	}
	m_pSelectedItem = pItem;
}