#pragma once
#include "UISceneBase.h"

class UISceneLogin : public UISceneBase
{
public : 
	
	static const int ButtonID_Home = 101;
	static const int ButtonID_Office = 102;
	static const int ButtonID_Shop = 103;
	static const int ButtonID_Atm = 104;
	static const int ButtonID_Club = 105;
	static const int ButtonID_Catwalk = 106;
	static const int ButtonID_Salon = 107;
	static const int ButtonID_Cafe = 108;

public : 
	UISceneLogin();
	virtual ~UISceneLogin();

	virtual bool init();
	
protected :
	void loginBtnCallback(CCObject* pSender);
	void signInBtnBackCallback(CCObject* pSender);
	void facebookBtnCallback(CCObject* pSender);
	void tweeterBtnCallback(CCObject* pSender);
};	