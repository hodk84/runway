#include "UISceneLogin.h"
#include "Managers\CharacterManager.h"
#include "TextInputTest.h"
#include "UICtrlTextInput.h"


////////////////////
//
/////////////////////

UISceneLogin::UISceneLogin()
{
	init();
}

UISceneLogin::~UISceneLogin()
{

}

bool UISceneLogin::init()
{
	CCSprite* pMenuBG = CCSprite::spriteWithFile("UI\\background_01.png");
	pMenuBG->setAnchorPoint(ccp(0.f, 0.f));
	pMenuBG->setPosition(ccp(0, 0));
	addChild(pMenuBG, -1);

	{
		CCLabelTTF* pLabel = CCLabelTTF::labelWithString("Login", "BlackoakStd", 36);		
		pLabel->setPosition(ccp(357,510));
		pMenuBG->addChild(pLabel, 1);		
	}
	 
	// 레알 텍스트 인풋
	{
		UICtrlTextInput* pTextInput = new UICtrlTextInput();
		pTextInput->setPosition(ccp(-100, 130));
		pMenuBG->addChild(pTextInput);
	}
	{
		UICtrlTextInput* pTextInput = new UICtrlTextInput();
		pTextInput->setPosition(ccp(-100, 80));
		pMenuBG->addChild(pTextInput);
	}
	
	
	CCMenuItemImage *pLoginButton = CCMenuItemImage::itemFromNormalImage(
	"UI\\back_up.png","UI\\back_down.png",
	this, menu_selector(UISceneLogin::loginBtnCallback));				
	pLoginButton->setAnchorPoint(ccp(0.f, 0.f));
	pLoginButton->setPosition(ccp(-100, 0));

	CCMenuItemImage *pSignInButton = CCMenuItemImage::itemFromNormalImage(
	"UI\\back_up.png","UI\\back_down.png",
	this, menu_selector(UISceneLogin::loginBtnCallback));				
	pSignInButton->setAnchorPoint(ccp(0.f, 0.f));
	pSignInButton->setPosition(ccp(100, 0));

	CCMenuItemImage *pFacebookButton = CCMenuItemImage::itemFromNormalImage(
	"UI\\back_up.png","UI\\back_down.png",
	this, menu_selector(UISceneLogin::loginBtnCallback));				
	pFacebookButton->setAnchorPoint(ccp(0.f, 0.f));
	pFacebookButton->setPosition(ccp(0, -150));

	CCMenuItemImage *pTweeterButton = CCMenuItemImage::itemFromNormalImage(
	"UI\\back_up.png","UI\\back_down.png",
	this, menu_selector(UISceneLogin::loginBtnCallback));				
	pTweeterButton->setAnchorPoint(ccp(0.f, 0.f));
	pTweeterButton->setPosition(ccp(0, -200));

	CCMenu* pMenu = CCMenu::menuWithItems(pLoginButton, pSignInButton, pFacebookButton, pTweeterButton, NULL);
	pMenu->setPosition(ccp(350, 300));
	pMenu->setAnchorPoint(ccp(0, 0));
    pMenuBG->addChild(pMenu);

	return true;
}

void UISceneLogin::loginBtnCallback(CCObject* pSender)
{

}

void UISceneLogin::signInBtnBackCallback(CCObject* pSender)
{

}

void UISceneLogin::facebookBtnCallback(CCObject* pSender)
{

}

void UISceneLogin::tweeterBtnCallback(CCObject* pSender)
{

}