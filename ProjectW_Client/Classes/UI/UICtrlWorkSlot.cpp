#include "UICtrlWorkSlot.h"
#include "Managers/TableManager.h"
#include "Managers/CharacterManager.h"
#include "UI/UICtrlMessageBox.h"
#include "UI/UICtrlWorkSlot.h"
#include "UI/UICtrlProgressBar.h"

///별 심기. 아이콘 숫자. 별 이펙트!

UICtrlWorkSlot::UICtrlWorkSlot()
:m_masterStar1(NULL)
,m_masterStar2(NULL)
,m_masterStar3(NULL)
{
	
}

UICtrlWorkSlot::~UICtrlWorkSlot()
{

}
void UICtrlWorkSlot::setWorkInfo(const JobData& info)
{
	m_jobData = info;
	int workCount = 0;
	
	WCharacter* myCharacter = CharacterManager::getInst()->getMyCharacter();
	Career*	career = myCharacter->getCareer(m_jobData.jobId);
	if( career )
		workCount = career->workCount;	
	
	if(m_labelPay)
	{
		char str[16];
		sprintf(str, "%d", m_jobData.qualification);		
		m_labelPay->setString(str);
	}
	if(m_labelDayPoint)
	{		
		int pay = m_jobData.getPay(workCount);
		char str[16];
		sprintf(str, "%d", pay);
		m_labelDayPoint->setString(str);
	}
	if(m_labelEnergy)
	{
		char str[16];
		sprintf(str, "%d", m_jobData.consumptionPerWork);
		m_labelEnergy->setString(str);
	}
		
	setMasterStar(m_jobData.getLevel(workCount));
}


void UICtrlWorkSlot::init()
{
	m_background = CCSprite::spriteWithFile("UI\\workslot.png");
	this->addChild(m_background);

	m_btnDoWork = CCMenuItemImage::itemFromNormalImage(
	"UI\\ui_career_start.png",
	"UI\\ui_career_start_1.png",
	this,
	menu_selector(UICtrlWorkSlot::menuWorkCallback));	
	m_btnDoWork->setPosition(ccp(240, -3));
    
	CCMenu* pMenu = CCMenu::menuWithItems(m_btnDoWork, NULL);
    pMenu->setPosition(CCPointZero);    
    this->addChild(pMenu, 1);

	{
		m_labelPay = CCLabelTTF::labelWithString("0", CCSizeMake(100, 40), CCTextAlignmentCenter, "Thonburi", 24);
		//m_labelPay->setColor(ccYELLOW);
		m_labelPay->setPosition(ccp(-270, -53));
		this->addChild(m_labelPay, 10);
	}
	{
		m_labelDayPoint = CCLabelTTF::labelWithString("0", CCSizeMake(100, 40), CCTextAlignmentCenter, "Thonburi", 24);		
		//m_labelDayPoint->setColor(ccYELLOW);
		m_labelDayPoint->setPosition(ccp(-208, -53));
		this->addChild(m_labelDayPoint, 10);
	}
	{
		m_labelEnergy = CCLabelTTF::labelWithString("0", CCSizeMake(100, 40), CCTextAlignmentCenter, "Thonburi", 24);
		//m_labelEnergy->setColor(ccYELLOW);
		m_labelEnergy->setPosition(ccp(-141, -53));
		this->addChild(m_labelEnergy, 10);
	}

	m_masterStar1 = CCSprite::spriteWithFile("UI\\ui_career_experience_1.png");		
	m_masterStar1->setPosition(ccp(-11, -21));
	m_masterStar1->setIsVisible(false);
	this->addChild(m_masterStar1);
	
	m_masterStar2 = CCSprite::spriteWithFile("UI\\ui_career_experience_1.png");		
	m_masterStar2->setPosition(ccp(38, -21));
	m_masterStar2->setIsVisible(false);
	this->addChild(m_masterStar2);
	
	m_masterStar3 = CCSprite::spriteWithFile("UI\\ui_career_experience_1.png");		
	m_masterStar3->setIsVisible(false);
	m_masterStar3->setPosition(ccp(86, -21));
	this->addChild(m_masterStar3);	

	m_bar = new UICtrlProgressBar();
	m_bar->setBarImageFile("UI\\ui_career_experience.png");
	m_bar->setPosition(ccp(267, 10));
	m_bar->setProgress(0.0f);
	m_background->addChild(m_bar, -2);

}

void UICtrlWorkSlot::popStar(CCNode* base)
{
	m_emitter = NULL;
	m_emitter = new CCParticleSystemQuad();
	m_emitter->initWithTotalParticles(10);
	base->addChild(m_emitter, 10);
	m_emitter->setTexture( CCTextureCache::sharedTextureCache()->addImage("UI\\ui_career_experience_1.png") );
	
	m_emitter->setDuration(1);	
	m_emitter->setGravity(ccp(0.f, -1000.f));	

	m_emitter->setAngle(90);
	m_emitter->setAngleVar(100);	
	m_emitter->setSpeed(600);
	m_emitter->setSpeedVar(100);	

	m_emitter->setRadialAccel(-120);
	m_emitter->setRadialAccelVar(10);	
	m_emitter->setTangentialAccel(100);
	m_emitter->setTangentialAccelVar(10);	
	
	m_emitter->setLife(1);
	m_emitter->setLifeVar(1);

	m_emitter->setStartSpin(0);
	m_emitter->setStartSpinVar(0);
	m_emitter->setEndSpin(0);
	m_emitter->setEndSpinVar(1000);	
	
	ccColor4F startColor = {1.f, 1.f, 1.f, 1.0f};
	m_emitter->setStartColor(startColor);
	
	ccColor4F endColorVar = {1.f, 1.f, 1.f, 1.0f};
	m_emitter->setEndColorVar(endColorVar);
		
	m_emitter->setStartSize(40.0f);
	m_emitter->setStartSizeVar(5.f);
	m_emitter->setEndSize(kParticleStartSizeEqualToEndSize);
	
	m_emitter->setEmissionRate(5);	
	m_emitter->setIsBlendAdditive(false);	
	m_emitter->setPosition( ccp(5, 20) );
	m_emitter->setPosVar( ccp(5, 5) );
}

void UICtrlWorkSlot::menuWorkCallback(CCObject* pSender)
{	
	WCharacter* myCharacter = CharacterManager::getInst()->getMyCharacter();
	Career*	career = myCharacter->getCareer(m_jobData.jobId);
	if( NULL == career )
	{
		career = myCharacter->addCareer(m_jobData.jobId);
	}

	if( career->workCount > (m_jobData.workForStar * JobData::Max_Level))
	{
		UICtrlMessageBox::CreateMessageBox("Skill is max!!");
		return;
	}
	
	CharacterStatus* data = myCharacter->getCharacterStatus();	

	//if( 0 > data->energy - m_jobData.consumptionPerWork )
	{
		//UICtrlMessageBox::CreateMessageBox("not enough energy");
		//return;
	}
	//data->energy -= m_jobData.consumptionPerWork;
	data->gameMoney += m_jobData.getPay(career->workCount);
	++career->workCount;
	
	if( (career->workCount % m_jobData.workForStar) == 0 )
	{
		setMasterStar(m_jobData.getLevel(career->workCount));		
	}
	else
	{
		float workCount = career->workCount % m_jobData.workForStar;
		float progress = workCount / float(m_jobData.workForStar);
		
		m_bar->setProgress(progress);
	}
}

void  UICtrlWorkSlot::setMasterStar(unsigned int masterLevel)
{
	clearMasterStar();

	if( masterLevel == 1 )
	{		
		m_masterStar1->setIsVisible(true);	
		popStar(m_masterStar1);
		m_bar->setProgress(1.1f);
	}
	else if( masterLevel == 2 )
	{
		m_masterStar1->setIsVisible(true);	
		m_masterStar2->setIsVisible(true);
		m_bar->setProgress(1.1f);
		popStar(m_masterStar2);
	}
	else if( masterLevel == 3 )
	{
		m_masterStar1->setIsVisible(true);	
		m_masterStar2->setIsVisible(true);	
		m_masterStar3->setIsVisible(true);			
		m_bar->setProgress(1.f);
		popStar(m_masterStar3);
	}

}

void UICtrlWorkSlot::clearMasterStar()
{
	if(m_masterStar1)
		m_masterStar1->setIsVisible(false);
	if(m_masterStar2)
		m_masterStar1->setIsVisible(false);
	if(m_masterStar3)
		m_masterStar1->setIsVisible(false);
}