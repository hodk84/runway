#pragma once;

#include <cocos2d.h>

using namespace cocos2d;

class UICtrlMessageBox : public CCNode
{
public :
	static const int DEFAULT_TAG = 999;
	static const int Button_Yes = 1;
	static const int Button_No = 2;


	UICtrlMessageBox():m_pMessage(NULL){};
	virtual ~UICtrlMessageBox(){};
	
	static bool CreateMessageBox(const char* message, int tag = DEFAULT_TAG);
	static UICtrlMessageBox* CreateMessageBox();


	virtual bool initWithTarget(SelectorProtocol *rec=NULL, SEL_MenuHandler selector1=NULL, SEL_MenuHandler selector2=NULL, char* bgFileName=NULL );
	void	setMessage(const char* message) { if(m_pMessage) m_pMessage->setString(message); }

protected : 
	void	yesButtonCallback(CCObject* pSender);
	void	noButtonCallback(CCObject* pSender);
	CCLabelTTF* m_pMessage;
};