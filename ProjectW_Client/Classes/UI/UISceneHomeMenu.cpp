#include "UISceneHomeMenu.h"
#include "Managers\CharacterManager.h"

UISceneHomeMenu::UISceneHomeMenu()
{
	init();
}

UISceneHomeMenu::~UISceneHomeMenu()
{

}

bool UISceneHomeMenu::init()
{
	CCSprite* pMenuBG = CCSprite::spriteWithFile("UI\\background_01.png");
	pMenuBG->setAnchorPoint(ccp(0.f, 0.f));
	pMenuBG->setPosition(ccp(0, 0));
	addChild(pMenuBG, -1);

	CCSprite* pMenuName = CCSprite::spriteWithFile("UI\\button_name.png");
	pMenuName->setAnchorPoint(ccp(0.f, 0.f));
	pMenuName->setPosition(ccp(22, 8));
	addChild(pMenuName, -1);
	
	CCSprite* pCityBG = CCSprite::spriteWithFile("UI\\city_namebar.png");
	pCityBG->setAnchorPoint(ccp(0.f, 0.f));
	pCityBG->setPosition(ccp(105,480));
	pMenuBG->addChild(pCityBG, -1);

	{
		CCLabelTTF* pLabel = CCLabelTTF::labelWithString("Seoul", "BlackoakStd", 48);		
		pLabel->setPosition(ccp(357,510));
		pMenuBG->addChild(pLabel, 1);		
	}
	std::vector<CCMenuItem*> btnVec;
	float begin = -407.f;
	float gapX = 134;
	float gapY = 165;
	float px = begin;
	float py = 80.f;
	{
		CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
		"UI\\home_1.png","UI\\home_2.png",
		this, menu_selector(UISceneHomeMenu::menuBackCallback));	
		pButton->setPosition(ccp(px, py));
		//pButton->setTag(ButtonID_Home);
		px += gapX;
		btnVec.push_back(pButton);
	}
	{
		CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
		"UI\\studio_1.png","UI\\studio_2.png",
		this, menu_selector(UISceneHomeMenu::menuBackCallback));	
		pButton->setPosition(ccp(px, py));
		//pButton->setTag(ButtonID_Home);
		btnVec.push_back(pButton);
		px += gapX;
	}
	{
		CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
		"UI\\shopping_1.png","UI\\shopping_2.png",
		this, menu_selector(UISceneHomeMenu::menuBackCallback));	
		pButton->setPosition(ccp(px, py));
		//pButton->setTag(ButtonID_Home);
		btnVec.push_back(pButton);
		px += gapX;
	}
	{
		CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
		"UI\\bank_1.png","UI\\bank_2.png",
		this, menu_selector(UISceneHomeMenu::menuBackCallback));	
		pButton->setPosition(ccp(px, py));
		//pButton->setTag(ButtonID_Home);
		btnVec.push_back(pButton);
		px += gapX;
	}
	{
		CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
		"UI\\club_1.png","UI\\club_2.png",
		this, menu_selector(UISceneHomeMenu::menuBackCallback));	
		pButton->setPosition(ccp(px, py));
		//pButton->setTag(ButtonID_Home);
		btnVec.push_back(pButton);
		px = begin;
		py -= gapY;
	}
	
	{
		CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
		"UI\\catwalk_1.png","UI\\catwalk_2.png",
		this, menu_selector(UISceneHomeMenu::menuBackCallback));	
		pButton->setPosition(ccp(px, py));
		//pButton->setTag(ButtonID_Home);
		btnVec.push_back(pButton);
		px += gapX;
	}
	{
		CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
		"UI\\salon_1.png","UI\\salon_2.png",
		this, menu_selector(UISceneHomeMenu::menuBackCallback));	
		pButton->setPosition(ccp(px, py));
		btnVec.push_back(pButton);
		
		px += gapX;
	}
	{
		CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
		"UI\\cafe_1.png","UI\\cafe_2.png",
		this, menu_selector(UISceneHomeMenu::menuBackCallback));	
		pButton->setPosition(ccp(px, py));
		//pButton->setTag(ButtonID_Home);
		btnVec.push_back(pButton);
		px += gapX;
	}
	{
		CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
		"UI\\gamecenter_1.png","UI\\gamecenter_2.png",
		this, menu_selector(UISceneHomeMenu::menuBackCallback));	
		pButton->setPosition(ccp(px, py));
		//pButton->setTag(ButtonID_Home);
		btnVec.push_back(pButton);
		px += gapX;
	}
	{
		CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
		"UI\\setting_1.png","UI\\setting_2.png",
		this, menu_selector(UISceneHomeMenu::menuBackCallback));	
		pButton->setPosition(ccp(px, py));
		//pButton->setTag(ButtonID_Home);
		btnVec.push_back(pButton);
		px = begin + 40;
		py -= gapY - 5;
	}
	{
		CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
		"UI\\get_point_1.png","UI\\get_point_2.png",
		this, menu_selector(UISceneHomeMenu::menuBackCallback));	
		pButton->setPosition(ccp(px, py));
		//pButton->setTag(ButtonID_Home);
		btnVec.push_back(pButton);
		px += 225;
	}
	{
		CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
		"UI\\get_clothes_1.png","UI\\get_clothes_2.png",
		this, menu_selector(UISceneHomeMenu::menuBackCallback));	
		pButton->setPosition(ccp(px, py));
		//pButton->setTag(ButtonID_Home);
		btnVec.push_back(pButton);
		px += 225;
	}
	{
		CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
		"UI\\get_energy_1.png","UI\\get_energy_2.png",
		this, menu_selector(UISceneHomeMenu::menuBackCallback));	
		pButton->setPosition(ccp(px, py));
		//pButton->setTag(ButtonID_Home);
		btnVec.push_back(pButton);
		//px += 200;
	}

	m_pMenu = DragableMenu::menuWithItems(btnVec);
	addChild(m_pMenu);
	return true;
}

void UISceneHomeMenu::menuBackCallback(CCObject* pSender)
{
}