#pragma once
#include "UISceneBase.h"


class UISceneHomeMenu : public UISceneBase
{
public : 
		static const int ButtonID_Home = 101;
	static const int ButtonID_Office = 102;
	static const int ButtonID_Shop = 103;
	static const int ButtonID_Atm = 104;
	static const int ButtonID_Club = 105;
	static const int ButtonID_Catwalk = 106;
	static const int ButtonID_Salon = 107;
	static const int ButtonID_Cafe = 108;

public : 
	UISceneHomeMenu();
	virtual ~UISceneHomeMenu();

	virtual bool init();
	

protected :
	void menuBackCallback(CCObject* pSender);
	
};	