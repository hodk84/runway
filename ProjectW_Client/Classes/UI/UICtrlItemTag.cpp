#include "UICtrlItemTag.h"
#include "Managers/TableManager.h"
#include "Cosmetic.h"


UICtrlItemTag::UICtrlItemTag()
{

}

UICtrlItemTag::~UICtrlItemTag()
{

}

UICtrlItemTag * UICtrlItemTag::itemFromNormalImage(const char *normalImage, const char *selectedImage, const char *disabledImage, SelectorProtocol* target, SEL_MenuHandler selector)
{
	UICtrlItemTag *pRet = new UICtrlItemTag();
	if (pRet && pRet->initFromNormalImage(normalImage, selectedImage, disabledImage, target, selector))
	{
		pRet->autorelease();
		return pRet;
	}
	CC_SAFE_DELETE(pRet);
	return NULL;
}

void UICtrlItemTag::setInfo(unsigned int merchandiseID, const char* itemImage, int priceType, int price,  int type, int value)
{
	char* szType = NULL;
	switch( type )
	{
	case Type_Day:
		szType = "UI\\shop_sun_icon.png";
		break;
	case Type_Night :
		szType = "UI\\shop_moon_icon.png";
		break;
	case Type_DayNight :
		szType = "UI\\shop_a_day_icon.png";
		break;	
	};

	char* szMoney = NULL;
	switch(priceType)
	{
	case Price_Cash :
		szMoney = "UI\\shop_cash_icon.png";
		break;
	case Price_Point :
		szMoney = "UI\\shop_point_icon.png";
		break;
	}
	setAnchorPoint(ccp(0, 0));
	if( szType )
	{
		CCSprite* pTypeIcon = CCSprite::spriteWithFile(szType);
		pTypeIcon->setAnchorPoint(ccp(0.f, 0.f));
		pTypeIcon->setPosition(ccp(6,175));
		addChild(pTypeIcon, 1);
		char typeStat[8];
		sprintf(typeStat, "%d", value); 	
		CCLabelTTF* pTypeLabel = CCLabelTTF::labelWithString(typeStat, "Thonburi", 24);
		//pTypeLabel->setColor(ccYELLOW);
		pTypeLabel->setPosition(ccp(18, 14));
		pTypeIcon->addChild(pTypeLabel, 10);
	}
	
	if( szMoney )
	{
		CCSprite* pMoneyIcon = CCSprite::spriteWithFile(szMoney);
		pMoneyIcon->setAnchorPoint(ccp(0.f, 0.f));
		pMoneyIcon->setPosition(ccp(40,0));
		addChild(pMoneyIcon, 1);
		char moneyPrice[8];
		sprintf(moneyPrice, "%d", price); 	
		CCLabelTTF* typeMoney = CCLabelTTF::labelWithString(moneyPrice, "Thonburi", 28);
		typeMoney->setColor(ccYELLOW);
		typeMoney->setPosition(ccp(60, 15));
		pMoneyIcon->addChild(typeMoney, 10);
	}

	CCSprite* pClothIcon = CCSprite::spriteWithFile(itemImage);
	pClothIcon->setAnchorPoint(ccp(0.f, 0.f));
	pClothIcon->setPosition(ccp(0,40));
	addChild(pClothIcon, 1);

	m_merchandiseID = merchandiseID;
}

bool UICtrlItemTag::setInfo(MerchandiseItem* pMercItem)
{
	if( NULL == pMercItem ) return false;
	
	int optionType = 0;
	int optionValue = 0;
	int thumbnailID = 0;

	if( pMercItem->tableType == MerchandiseList::Type_Clothes )
	{
		WClothes* pClothes = TableManager::getInst()->getClothes(pMercItem->itemID);
		if( pClothes )
		{
			thumbnailID = pClothes->m_thumnailID;
			optionType = pClothes->m_options[0].type;
			optionValue = pClothes->m_options[0].value;
		}
		else
		{
			assert(0);
			return false;
		}
	}
	else if( pMercItem->tableType == MerchandiseList::Type_Money )
	{
		Item* pItem = TableManager::getInst()->getItem(pMercItem->itemID);
		if( pItem )
		{
			thumbnailID = pItem->m_thumnailID;
			optionType = pItem->m_options[0].type;
			optionValue = pItem->m_options[0].value;
		}
		else
		{
			assert(0);
			return false;
		}
	}
	else if( pMercItem->tableType == MerchandiseList::Type_Cosmetic)
	{
		CosmeticDataVec datas;
		TableManager::getInst()->getCosmetic(pMercItem->itemID, datas);
		
		if( datas.empty() ) return false;

		thumbnailID = datas[0].resourceId;
	}
	else
	{
		assert(0);
		return false;
	}
	
	ImageData* pData = TableManager::getInst()->getImageDataByResourceId(thumbnailID);
	if( NULL == pData )
	{
		assert(0);
		return false;
	}

	setInfo(pMercItem->merchandiseID, pData->getFullPathAsStr().c_str(),
		 pMercItem->priceType, pMercItem->price, optionType, optionValue);
	
	return true;
}