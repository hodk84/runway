#pragma once


#include <cocos2d.h>
#include <CCMenu.h>

using namespace cocos2d;


class DragableMenu : public CCMenu
{
public :
	static DragableMenu* menu();
	static DragableMenu* menuWithItems(vector<CCMenuItem*> items);

	bool initWithItems(std::vector<CCMenuItem*> items);
	bool initWithNothing();

	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);
	virtual void ccTouchCancelled(CCTouch* touch, CCEvent* event);
	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
};
	
class UISceneBase : public CCLayer
{
public :
	UISceneBase();
	virtual ~UISceneBase();
	virtual bool init();
	bool loadUIXml(char* xmlFileName);
	
	DragableMenu*		m_pMenu;

protected :
	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);
	virtual void ccTouchCancelled(CCTouch* touch, CCEvent* event);
	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);

};
