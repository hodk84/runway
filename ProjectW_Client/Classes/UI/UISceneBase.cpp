#include "UISceneBase.h"

DragableMenu * DragableMenu::menu()
{
	DragableMenu *pRet = new DragableMenu();
	if (pRet && pRet->initWithNothing())
	{
		pRet->autorelease();	
		return pRet;
	}	
	CC_SAFE_DELETE(pRet)
	return NULL;
}

bool DragableMenu::initWithNothing()
{
	if (CCLayer::init())
	{
		this->m_bIsTouchEnabled = true;

		// menu in the center of the screen
		CCSize s = CCDirector::sharedDirector()->getWinSize();

		this->m_bIsRelativeAnchorPoint = false;
		setAnchorPoint(ccp(0.5f, 0.5f));
		this->setContentSize(s);

		// XXX: in v0.7, winSize should return the visible size
		// XXX: so the bar calculation should be done there
		CCRect r;
        CCApplication::sharedApplication().statusBarFrame(&r);
		ccDeviceOrientation orientation = CCDirector::sharedDirector()->getDeviceOrientation();
		if (orientation == CCDeviceOrientationLandscapeLeft || orientation == CCDeviceOrientationLandscapeRight)
		{
			s.height -= r.size.width;
		}
		else
		{
			s.height -= r.size.height;
		}
		setPosition(ccp(s.width/2, s.height/2));

		m_pSelectedItem = NULL;
		m_eState = kCCMenuStateWaiting;
		return true;
	}
}

bool DragableMenu::initWithItems(std::vector<CCMenuItem*> items)
{
	if (CCLayer::init())
	{
		this->m_bIsTouchEnabled = true;

		// menu in the center of the screen
		CCSize s = CCDirector::sharedDirector()->getWinSize();

		this->m_bIsRelativeAnchorPoint = false;
		setAnchorPoint(ccp(0.5f, 0.5f));
		this->setContentSize(s);

		// XXX: in v0.7, winSize should return the visible size
		// XXX: so the bar calculation should be done there
		CCRect r;
        CCApplication::sharedApplication().statusBarFrame(&r);
		ccDeviceOrientation orientation = CCDirector::sharedDirector()->getDeviceOrientation();
		if (orientation == CCDeviceOrientationLandscapeLeft || orientation == CCDeviceOrientationLandscapeRight)
		{
			s.height -= r.size.width;
		}
		else
		{
			s.height -= r.size.height;
		}
		setPosition(ccp(s.width/2, s.height/2));

		int z=0;
		
		vector<CCMenuItem*>::iterator itr = items.begin();
		for( itr; itr != items.end(); ++itr)
		{
			this->addChild((*itr), z);
			z++;			
		}

		m_pSelectedItem = NULL;
		m_eState = kCCMenuStateWaiting;
		return true;
	}

	return false;
}

DragableMenu * DragableMenu::menuWithItems(vector<CCMenuItem*> items)
{
	DragableMenu *pRet = new DragableMenu();
	if (pRet && pRet->initWithItems(items))
	{
		pRet->autorelease();	
		return pRet;
	}	
	CC_SAFE_DELETE(pRet)
	return NULL;
}

bool DragableMenu::ccTouchBegan(CCTouch* touch, CCEvent* event)
{
	CCMenu::ccTouchBegan( touch,  event);
	return false;	
}

void DragableMenu::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	if( m_eState != kCCMenuStateTrackingTouch )
		return;
	CCMenu::ccTouchEnded( touch,  event);
}

void DragableMenu::ccTouchCancelled(CCTouch* touch, CCEvent* event)
{
	if( m_eState != kCCMenuStateTrackingTouch )
		return;
	CCMenu::ccTouchCancelled( touch,  event);
}

void DragableMenu::ccTouchMoved(CCTouch* touch, CCEvent* event)
{
	CCMenu::ccTouchMoved( touch,  event);
}

///////////////////////////////////////////
//
/////////////////////////////////////////
UISceneBase::UISceneBase()
	: m_pMenu(NULL)
{
	
}

UISceneBase::~UISceneBase()
{

}

bool UISceneBase::init()
{
	
	return CCLayer::init();
	//{
	//	this->m_bIsTouchEnabled = true;

	//	// menu in the center of the screen
	//	CCSize s = CCDirector::sharedDirector()->getWinSize();

	//	this->m_bIsRelativeAnchorPoint = false;
	//	setAnchorPoint(ccp(0.5f, 0.5f));
	//	this->setContentSize(s);

	//	// XXX: in v0.7, winSize should return the visible size
	//	// XXX: so the bar calculation should be done there
	//	CCRect r;
 //       CCApplication::sharedApplication().statusBarFrame(&r);
	//	ccDeviceOrientation orientation = CCDirector::sharedDirector()->getDeviceOrientation();
	//	if (orientation == CCDeviceOrientationLandscapeLeft || orientation == CCDeviceOrientationLandscapeRight)
	//	{
	//		s.height -= r.size.width;
	//	}
	//	else
	//	{
	//		s.height -= r.size.height;
	//	}
	//	setPosition(ccp(s.width/2, s.height/2));
	//	m_pSelectedItem = NULL;
	//	m_eState = kCCMenuStateWaiting;
	//}
	//CCMenu::onEnter();	
	//onInit();
}

bool UISceneBase::loadUIXml(char* xmlFileName)
{
	return false;
}

bool UISceneBase::ccTouchBegan(CCTouch* touch, CCEvent* event)
{
	return CCLayer::ccTouchBegan( touch,  event);	
}

void UISceneBase::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	CCLayer::ccTouchEnded( touch,  event);
}

void UISceneBase::ccTouchCancelled(CCTouch* touch, CCEvent* event)
{
	CCLayer::ccTouchCancelled( touch,  event);
}

void UISceneBase::ccTouchMoved(CCTouch* touch, CCEvent* event)
{
	CCLayer::ccTouchMoved( touch,  event);
}

