#include "UISceneTownBuilding.h"
#include "Scenes/SceneHome.h"
#include "Managers/SceneManager.h"

UISceneTownBuilding::UISceneTownBuilding()
{
	init();
}

UISceneTownBuilding::~UISceneTownBuilding()
{

}

bool UISceneTownBuilding::init()
{
	std::vector<CCMenuItem*> ButtonVec;

	{
		CCMenuItemImage *pBackButton = CCMenuItemImage::itemFromNormalImage(
       "Town\\pizza1.png","Town\\pizza2.png",
       this, menu_selector(UISceneTownBuilding::menuBackCallback));
	
		pBackButton->setAnchorPoint(ccp(0.f, 0.f));
		pBackButton->setPosition(ccp(685, 160));
		pBackButton->setTag(ButtonID_Salon);
		
		ButtonVec.push_back(pBackButton);
	}
	{
		CCMenuItemImage *pBackButton = CCMenuItemImage::itemFromNormalImage(
       "Town\\library1.png","Town\\library2.png",
       this, menu_selector(UISceneTownBuilding::menuBackCallback));
			
		pBackButton->setAnchorPoint(ccp(0.f, 0.f));
		pBackButton->setPosition(ccp(160, -50));
		pBackButton->setTag(ButtonID_CatWalk);
	
		ButtonVec.push_back(pBackButton);
	}
	
	{
		CCMenuItemImage *pBackButton = CCMenuItemImage::itemFromNormalImage(
       "Town\\big_house1.png","Town\\big_house2.png",
       this, menu_selector(UISceneTownBuilding::menuBackCallback));
	
		pBackButton->setAnchorPoint(ccp(0.f, 0.f));
		pBackButton->setPosition(ccp(290, 310));
		pBackButton->setTag(ButtonID_Office);		
		ButtonVec.push_back(pBackButton);
	}
	{
		CCMenuItemImage *pBackButton = CCMenuItemImage::itemFromNormalImage(
       "Town\\burger1.png","Town\\burger2.png",
       this, menu_selector(UISceneTownBuilding::menuBackCallback));
	
		pBackButton->setAnchorPoint(ccp(0.f, 0.f));
		pBackButton->setPosition(ccp(608, -80));
		pBackButton->setTag(ButtonID_Shop);		
		ButtonVec.push_back(pBackButton);
	}
	
	{
		CCMenuItemImage *pBackButton = CCMenuItemImage::itemFromNormalImage(
       "Town\\house1.png","Town\\house2.png",
       this, menu_selector(UISceneTownBuilding::menuBackCallback));
	
		pBackButton->setAnchorPoint(ccp(0.f, 0.f));
		pBackButton->setPosition(ccp(154, 215));
		pBackButton->setTag(ButtonID_Club);
	
		ButtonVec.push_back(pBackButton);
	}
	{
		CCMenuItemImage *pBackButton = CCMenuItemImage::itemFromNormalImage(
       "Town\\flowershop.png","Town\\flowershop2.png",
       this, menu_selector(UISceneTownBuilding::menuBackCallback));
	
		pBackButton->setAnchorPoint(ccp(0.f, 0.f));
		pBackButton->setPosition(ccp(30, 150));
		pBackButton->setTag(ButtonID_Atm);		
		ButtonVec.push_back(pBackButton);
	}
	{
		CCMenuItemImage *pBackButton = CCMenuItemImage::itemFromNormalImage(
       "Town\\alabjip1.png","Town\\alabjip2.png",
       this, menu_selector(UISceneTownBuilding::menuBackCallback));
	
		pBackButton->setAnchorPoint(ccp(0.f, 0.f));
		pBackButton->setPosition(ccp(-95, 35));
		pBackButton->setTag(ButtonID_Home);		
		ButtonVec.push_back(pBackButton);
	}
	

	setAnchorPoint(ccp(0,0));
	m_pMenu = DragableMenu::menuWithItems(ButtonVec);	
	m_pMenu->setPosition(ccp(0, 0));
	addChild(m_pMenu);
	return true;
}

void UISceneTownBuilding::menuBackCallback(CCObject* pSender)
{
	CCMenuItemImage* item = dynamic_cast<CCMenuItemImage*>(pSender);
	if( item )
	{
		int tag = item->getTag();		
		if( UISceneTownBuilding::ButtonID_Home == tag )
		{
			SceneManager::getInst()->ChangeScene(SceneManager::kScene_MainStreet);
		}
		else if( UISceneTownBuilding::ButtonID_Shop == tag )
		{
			SceneManager::getInst()->ChangeScene(SceneManager::kScene_MainStreet);
		}
		else if( UISceneTownBuilding::ButtonID_Office == tag )
		{
			SceneManager::getInst()->ChangeScene(SceneManager::kScene_MainStreet);
		}
		else if( UISceneTownBuilding::ButtonID_CatWalk == tag )
		{
			SceneManager::getInst()->ChangeScene(SceneManager::kScene_MainStreet);
		}
		else if( UISceneTownBuilding::ButtonID_Atm == tag )
		{
			SceneManager::getInst()->ChangeScene(SceneManager::kScene_MainStreet2);
		}
		else if( UISceneTownBuilding::ButtonID_Cafe == tag )
		{
			SceneManager::getInst()->ChangeScene(SceneManager::kScene_MainStreet2);
		}
		else if( UISceneTownBuilding::ButtonID_Salon == tag )
		{
			SceneManager::getInst()->ChangeScene(SceneManager::kScene_MainStreet2);
		}
		else if( UISceneTownBuilding::ButtonID_Club == tag )
		{
			SceneManager::getInst()->ChangeScene(SceneManager::kScene_MainStreet2);
		}
	
		return;
	}
}