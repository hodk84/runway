#pragma once;

#include <cocos2d.h>

using namespace cocos2d;

class WSprite;
class UICtrlProgressBar : public CCNode
{
public :
	
	UICtrlProgressBar()
	:m_pBarSprite(NULL)
	, m_fProgress(0.f)
	, m_fSpeed(0.2f)
	{};
	virtual ~UICtrlProgressBar(){};
	
	void	setBarImageFile(const char* fileName);
	void	setProgress(float progress);
	float	getCurrentProgress();

	void	setSpeed(float speed) { m_fSpeed = speed; }
	float	getSpeed() { return m_fSpeed; }

protected : 
	float			m_originalWidth;
	float			m_fProgress;
	float			m_fSpeed;
	WSprite*		m_pBarSprite;
};