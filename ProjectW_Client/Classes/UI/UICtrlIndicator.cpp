#include "UICtrlIndicator.h"

UICtrlIndicator::UICtrlIndicator()
	: m_isIndicatorMoved(false)
{
		init();
}

UICtrlIndicator::~UICtrlIndicator()
{
	
}

// on "init" you need to initialize your instance
bool UICtrlIndicator::init()
{	
    bool bRet = false;
    do 
    {			
		this->setAnchorPoint(ccp(0, 0));
		m_pBG = WSprite::WSpriteWithFile("UI\\indiBg.png");
		if( m_pBG )
		{
			m_pBG->setAnchorPoint(ccp(0, 0));
			addChild(m_pBG);
		}
		m_pIndicatorImg = WSprite::WSpriteWithFile("UI\\indicator.png");
		if( m_pIndicatorImg )
		{
			m_pIndicatorImg->setAnchorPoint(ccp(0, 0));
			addChild(m_pIndicatorImg);
		}

		CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, -129, true);
        bRet = true;
    } while (0);

    return bRet;
}

bool UICtrlIndicator::ccTouchBegan(CCTouch* touch, CCEvent* event)
{	
	CCPoint touchLocation = touch->locationInView(touch->view());
	touchLocation = CCDirector::sharedDirector()->convertToGL(touchLocation);

	CCSize size = m_pBG->getContentSize();
	CCRect bounding(0, 0, size.width, size.height);
	CCPoint local = m_pBG->convertToNodeSpace(touchLocation);
	
	if( CCRect::CCRectContainsPoint(bounding, local) )
	{
		m_fSpring = 0.f;
		m_touchBeginPoint = touch->locationInView(0);
		m_touchBeginTime = m_fTick;
		return true;
	}
	return false;	
}

void UICtrlIndicator::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	CCPoint ptEnd = touch->locationInView(0);	
	m_fAccelPerTime = (ptEnd.x - m_touchBeginPoint.x) / (m_fTick - m_touchBeginTime);
	m_isIndicatorMovingStoped = true;
}

void UICtrlIndicator::ccTouchCancelled(CCTouch* touch, CCEvent* event)
{
}

void UICtrlIndicator::ccTouchMoved(CCTouch* touch, CCEvent* event)
{
	CCPoint touchLocation = touch->locationInView( touch->view() );	
	CCPoint prevLocation = touch->previousLocationInView( touch->view() );	

	touchLocation = CCDirector::sharedDirector()->convertToGL( touchLocation );
	prevLocation = CCDirector::sharedDirector()->convertToGL( prevLocation );

	CCPoint diff = ccpSub(touchLocation,prevLocation);
	diff.y = 0.f;
	CCNode* node = m_pIndicatorImg;
	CCPoint currentPos = node->getPosition();
	
	CCPoint resultPos = ccp(currentPos.x + diff.x, currentPos.y);
	if( resultPos.x >= 0 && resultPos.x <= (m_pBG->getContentSize().width - node->getContentSize().width))
	{
		node->setPosition(resultPos);
		m_isIndicatorMoved = true;	
		m_isIndicatorMovingStoped = false;
	}	
}
 
void UICtrlIndicator::touchDelegateRetain()
{
	retain();
}

void UICtrlIndicator::touchDelegateRelease()
{
	release();
}
 void UICtrlIndicator::update(ccTime dt)
 {	
	
 }

 void UICtrlIndicator::setIndicatorMoveProgress(float progress)
 {
	 float range = m_pBG->getContentSize().width - m_pIndicatorImg->getContentSize().width;// /2
	 float px = range * progress;
	 CCPoint pt = ccp(px, m_pIndicatorImg->getPosition().y);
	 m_pIndicatorImg->setPosition(pt);
 }
 
 float UICtrlIndicator::getIndicatorMoveProgress()
 {
	 float range = m_pBG->getContentSize().width - m_pIndicatorImg->getContentSize().width;
	 float px = m_pIndicatorImg->getPosition().x;
	 float progress = px/range;
	 m_isIndicatorMoved = false;
	 
	 return -progress;
 }

 bool UICtrlIndicator::getIndicatorMoved()
 {
	bool ret = m_isIndicatorMoved;
	m_isIndicatorMoved = false;
	return ret;
}

