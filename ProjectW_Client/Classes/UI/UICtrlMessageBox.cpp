#include "UICtrlMessageBox.H"
#include "Managers\SceneManager.h"


bool UICtrlMessageBox::CreateMessageBox(const char* message, int tag)
{
	UICtrlMessageBox *pRet = new UICtrlMessageBox();
	if (pRet)
	{
		pRet->autorelease();
		
		SceneBase* pScene = SceneManager::getInst()->getCurScene();
		if( pScene )
		{
			pScene->removeChildByTag(tag, true);
			pRet->initWithTarget(pScene, menu_selector(SceneBase::messageBoxCallback), menu_selector(SceneBase::messageBoxCallback));	
			pScene->addChild(pRet, 90, tag);

			CCSize wSize = CCDirector::sharedDirector()->getWinSize();
			CCSize cSize = pRet->getContentSize();
			pRet->setPosition( ccp( (wSize.width/2) - (cSize.width/2), (wSize.height/2) + (cSize.height/2) - 30) );
		}
		pRet->setMessage(message);
		return true;
	}
	CC_SAFE_DELETE(pRet);
	return false;
}

UICtrlMessageBox* UICtrlMessageBox::CreateMessageBox()
{
	UICtrlMessageBox *pRet = new UICtrlMessageBox();
	if (pRet)
	{
		pRet->autorelease();
		CCSize wSize = CCDirector::sharedDirector()->getWinSize();
		CCSize cSize = pRet->getContentSize();
		pRet->setPosition( ccp( (wSize.width/2) - (cSize.width/2), (wSize.height/2) + (cSize.height/2) - 30) );

		return pRet;
	}
	else
	{
		CC_SAFE_DELETE(pRet);
		return NULL;
	}
}

bool UICtrlMessageBox::initWithTarget(SelectorProtocol *rec, SEL_MenuHandler selector1, SEL_MenuHandler selector2, char* bgFileName )
{
	bool bRet = false;
    do 
    {
		if( bgFileName == NULL )
		{
			bgFileName = "UI\\common_button_01.png";
		}

		CCSprite* pBg = CCSprite::spriteWithFile(bgFileName);
		pBg->setAnchorPoint(ccp(0.f, 0.f));
		pBg->setPosition(ccp(0, 0));
		addChild(pBg, -1);

		std::vector<CCMenuItemImage*> btnVec;
		{
			CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
			"ui\\common_admission.png","ui\\common_admission_1.png",
			rec, selector1);
			
			pButton->setScale(0.5f);
			pButton->setPosition(ccp(50, 25));
			pButton->setTag(Button_Yes);
			btnVec.push_back(pButton);
		}
		
		{
			CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
			"ui\\common_hud_back.png","ui\\common_hud_back_1.png",
			rec, selector2);
		
			pButton->setPosition(ccp(140, 25));
			pButton->setTag(Button_No);
			btnVec.push_back(pButton);
		}
		CCMenu* pButtons = CCMenu::menuWithItems(btnVec[0],btnVec[1], NULL);
		pButtons->setPosition(ccp(0, 0));
		pBg->setAnchorPoint(ccp(0, 0));
		pBg->addChild(pButtons, 1);
		
		{
			m_pMessage = CCLabelTTF::labelWithString("", "Thonburi", 32);			
			m_pMessage->setPosition(ccp(80, 130));
			pBg->addChild(m_pMessage, 1);
		}	

		bRet = true;

	} while(0);

	return bRet;
}

void UICtrlMessageBox::yesButtonCallback(CCObject* pSender)
{

}

void UICtrlMessageBox::noButtonCallback(CCObject* pSender)
{

}