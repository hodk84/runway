
#include "UICtrlTextInput.h"
#include "TextInputTest.h"

UICtrlTextInput::UICtrlTextInput()
{

}

UICtrlTextInput::~UICtrlTextInput()
{

}


void UICtrlTextInput::onEnter()
{
	CCNode::onEnter();

		// 레알 텍스트 인풋
	TextInputTest* pContainerLayer = new TextInputTest;
    pContainerLayer->autorelease();
	
	// 키보드 처리 클래스
    KeyboardNotificationLayer* pTestLayer =  new TextFieldTTFDefaultTest();
    pTestLayer->autorelease();

    pContainerLayer->addKeyboardNotificationLayer(pTestLayer);
	
	this->addChild(pContainerLayer);
	
}