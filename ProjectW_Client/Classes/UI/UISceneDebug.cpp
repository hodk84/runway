#include "UISceneDebug.h"
#include "Scenes\ScMainStreet.h"
#include "Managers\CharacterManager.h"
#include "Managers/SceneManager.h"
#include "Managers/NetworkManager.h"

UISceneDebug::UISceneDebug()
	: m_pTimer(NULL)
{
	init();
}

UISceneDebug::~UISceneDebug()
{

}

bool UISceneDebug::init()
{	
	CCSprite* pMenuBG = CCSprite::spriteWithFile("UI\\debug_bg.png");		
	pMenuBG->setAnchorPoint(ccp(0.f, 0.f));
	pMenuBG->setPosition(ccp(0, 640-pMenuBG->getContentSize().height));
	addChild(pMenuBG, -1);

	this->setAnchorPoint(ccp(0.f, 0.f));
	
	CCMenuItemImage *pButton1 = CCMenuItemImage::itemFromNormalImage(
	"UI\\back_up.png","UI\\back_down.png",
	this, menu_selector(UISceneDebug::menuBackCallback));
	pButton1->setAnchorPoint(ccp(0.f, 0.f));
	pButton1->setPosition(ccp(-470, 270));
	pButton1->setTag(ButtonID_1);	

	CCMenuItemImage *pButton2 = CCMenuItemImage::itemFromNormalImage(
	"UI\\back_up.png","UI\\back_down.png",
	this, menu_selector(UISceneDebug::menuBackCallback));
	pButton2->setAnchorPoint(ccp(0.f, 0.f));
	pButton2->setPosition(ccp(-320, 270));
	pButton2->setTag(ButtonID_2);	

	CCMenuItemImage *pButton3 = CCMenuItemImage::itemFromNormalImage(
	"UI\\back_up.png","UI\\back_down.png",
	this, menu_selector(UISceneDebug::menuBackCallback));
	pButton3->setAnchorPoint(ccp(0.f, 0.f));
	pButton3->setPosition(ccp(-170, 270));
	pButton3->setTag(ButtonID_3);	

	CCMenuItemImage *pButton4 = CCMenuItemImage::itemFromNormalImage(
	"UI\\back_up.png","UI\\back_down.png",
	this, menu_selector(UISceneDebug::menuBackCallback));
	pButton4->setAnchorPoint(ccp(0.f, 0.f));
	pButton4->setPosition(ccp(-20, 270));
	pButton4->setTag(ButtonID_4);	

	CCMenuItemImage *pButton5 = CCMenuItemImage::itemFromNormalImage(
	"UI\\back_up.png","UI\\back_down.png",
	this, menu_selector(UISceneDebug::menuBackCallback));
	pButton5->setAnchorPoint(ccp(0.f, 0.f));
	pButton5->setPosition(ccp(130, 270));
	pButton5->setTag(ButtonID_5);				
	
	CCMenu* pMenu = CCMenu::menuWithItems(pButton1, pButton2, pButton3, pButton4, pButton5, NULL);
    addChild(pMenu);
	
	{
		m_pTimer = CCLabelTTF::labelWithString("[Debug Mode]", "Thonburi", 32);
		m_pTimer->setColor(ccORANGE);
		m_pTimer->setPosition(ccp(810.f, 640-28.f));
		addChild(m_pTimer, 10);
	}
	
	this->schedule( schedule_selector(UISceneDebug::update) ); 

	this->setIsVisible(false);
	return true;
}

void UISceneDebug::menuBackCallback(CCObject* pSender)
{
	CCMenuItemImage* item = dynamic_cast<CCMenuItemImage*>(pSender);
	int tag = item->getTag();
	
	WCharacter* pCharacter = CharacterManager::getInst()->getMyCharacter();
	if( tag == ButtonID_1 )
	{
		NetworkManager::getInst()->getClubUserInfo();
		//pCharacter->setExpression(7, 1);
		//m_pTimer->setString("expression eye-1");
	}
	else if( tag == ButtonID_2 )
	{
		pCharacter->setExpression(7, 2);
		m_pTimer->setString("expression eye-2");
	}	
	else if( tag == ButtonID_3 )
	{
		pCharacter->setExpression(8, 1);
		m_pTimer->setString("expression lip-1");
	}
	else if( tag == ButtonID_4 )
	{
		pCharacter->setExpression(8, 2);
		m_pTimer->setString("expression lip-2");
	}	
	else if( tag == ButtonID_5 )
	{
		pCharacter->playAnimation(0);
		//pCharacter->setExpression(7, 0);
		//pCharacter->setExpression(8, 0);
		//m_pTimer->setString("expression reset");
	}	
}

void UISceneDebug::update(ccTime dt)
{
	
}