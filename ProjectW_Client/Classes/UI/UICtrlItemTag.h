#pragma once;

#include <cocos2d.h>
#include "Merchandise.h"

using namespace cocos2d;

class UICtrlItemTag : public CCMenuItemImage
{
public :

	static const int Type_Day = 1;
	static const int Type_Night = 2;
	static const int Type_DayNight = 3;

	static const int Price_Cash = 1;
	static const int Price_Point = 2;

	UICtrlItemTag();
	virtual ~UICtrlItemTag();
	
	static UICtrlItemTag* itemFromNormalImage(const char *normalImage, const char *selectedImage, const char *disabledImage, SelectorProtocol* target, SEL_MenuHandler selector);
	
	void setInfo(unsigned int merchandiseID, const char* itemImage, int priceType, int price, int optionType, int optionValue);

	bool setInfo(MerchandiseItem* pItem);

	unsigned int getMerchandiseID() { return m_merchandiseID; }

protected : 

	unsigned int	m_merchandiseID;
};