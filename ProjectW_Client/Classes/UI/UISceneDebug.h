#pragma once
#include "UISceneBase.h"

class UISceneDebug : public UISceneBase
{
public : 
	static const int ButtonID_1 = 101;	
	static const int ButtonID_2 = 102;	
	static const int ButtonID_3 = 103;	
	static const int ButtonID_4 = 104;	
	static const int ButtonID_5 = 105;	

public : 
	UISceneDebug();
	virtual ~UISceneDebug();

	virtual bool init();
	virtual void update(ccTime dt);

protected :
	void menuBackCallback(CCObject* pSender);
	CCLabelTTF* m_pTimer;
};	