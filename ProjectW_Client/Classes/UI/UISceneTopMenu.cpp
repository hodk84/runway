#include "UISceneTopMenu.h"
#include "Scenes\ScMainStreet.h"
#include "Managers\CharacterManager.h"
#include "Managers/SceneManager.h"
#include "UI/UICtrlProgressBar.h"

UISceneTopMenu::UISceneTopMenu()
	: m_pTimer(NULL)
	, m_myCharacter(NULL)
{
	init();
}

UISceneTopMenu::~UISceneTopMenu()
{

}

bool UISceneTopMenu::init()
{	
	CCSprite* pMenuBG = CCSprite::spriteWithFile("UI\\hud.png");		
	pMenuBG->setAnchorPoint(ccp(0.f, 0.f));
	pMenuBG->setPosition(ccp(0, 640-pMenuBG->getContentSize().height));
	addChild(pMenuBG, -1);

	this->setAnchorPoint(ccp(0.f, 0.f));
	CCMenuItemImage *pBackButton = CCMenuItemImage::itemFromNormalImage(
	"UI\\back_up.png","UI\\back_down.png",
	this, menu_selector(UISceneTopMenu::menuBackCallback));
				
	pBackButton->setAnchorPoint(ccp(0.f, 0.f));
	pBackButton->setPosition(ccp(-470, 270));
	pBackButton->setTag(ButtonID_Back);	
	CCMenu* pMenu = CCMenu::menuWithItems(pBackButton, NULL);
    addChild(pMenu);

	m_myCharacter = CharacterManager::getInst()->getMyCharacter();
	assert(m_myCharacter);

	{
		CCLabelTTF* pLabel = CCLabelTTF::labelWithString("0", "Thonburi", 32);
		pLabel->setPosition(ccp(180.f, 640-28.f));
		addChild(pLabel, 10);
		m_labelGameMoeny = pLabel;
	}			
	{
		CCLabelTTF* pLabel = CCLabelTTF::labelWithString("0", "Thonburi", 32);
		pLabel->setPosition(ccp(330.f, 640-28.f));
		addChild(pLabel, 10);
		m_labelCash = pLabel;
	}			
	{
		CCLabelTTF* pLabel = CCLabelTTF::labelWithString("0", "Thonburi", 32);
		pLabel->setPosition(ccp(470.f, 640-28.f));
		addChild(pLabel, 10);
		m_labelLevel = pLabel;
	}			
	{
		CCLabelTTF* pLabel = CCLabelTTF::labelWithString("0", "Thonburi", 32);
		pLabel->setPosition(ccp(650.f, 640-28.f));
		addChild(pLabel, 10);
		m_labelClothes = pLabel;
	}
	{
		CCLabelTTF* pLabel = CCLabelTTF::labelWithString("0/15", "Thonburi", 32);
		pLabel->setColor(ccYELLOW);
		pLabel->setPosition(ccp(815.f, 640-28.f));
		addChild(pLabel, 10);
		m_labelEnergy = pLabel;
	}
	{
		m_pTimer = CCLabelTTF::labelWithString("0:00", "Thonburi", 32);
		m_pTimer->setColor(ccORANGE);
		m_pTimer->setPosition(ccp(910.f, 640-28.f));
		addChild(m_pTimer, 10);
	}
	m_fTick = 20;
	this->schedule( schedule_selector(UISceneTopMenu::update) ); 

	m_bar = new UICtrlProgressBar();
	m_bar->setBarImageFile("UI\\ui_energybar.png");
	m_bar->setPosition(ccp(757, 597));
	m_bar->setProgress(0.f);
	addChild(m_bar, -2);

	updateStatus();

	return true;
}

void UISceneTopMenu::updateStatus()
{
	if( NULL == m_myCharacter ) return;

	CharacterStatus* pData = m_myCharacter->getCharacterStatus();
	char curEnergy[8];
	sprintf(curEnergy, "%d \/ %d", pData->energy, WCharacter::MAX_ENERGY);
	
	char curMoney[8];
	sprintf(curMoney, "%d", pData->gameMoney);

	char curCash[8];
	sprintf(curCash, "%d", pData->cash);
	
	char curClothes[8];
	sprintf(curClothes, "%d", pData->clothCount);
	
	char curLevel[8];
	sprintf(curLevel, "%d", pData->level);

	m_labelGameMoeny->setString(curMoney);	
	m_labelCash->setString(curCash);	
	m_labelLevel->setString(curLevel);	
	m_labelClothes->setString(curClothes);
	m_labelEnergy->setString(curEnergy);

	m_bar->setProgress(float(pData->energy)/float(WCharacter::MAX_ENERGY));
}

void UISceneTopMenu::menuBackCallback(CCObject* pSender)
{
	SceneManager::getInst()->ChangeScene(SceneManager::kScene_MainStreet);	
}

void UISceneTopMenu::update(ccTime dt)
{
	if( m_pTimer )
	{
		char timeStr[16];
		sprintf(timeStr,"%2d:%02d", int(m_fTick)/60, int(m_fTick)%60);
		m_pTimer->setString(timeStr);
		updateStatus();
	}
	m_fTick -= dt;
	if( m_fTick <= 0 )
	{
		m_fTick = 20;//20초에 한번 에너지 충전		
		m_bar->setProgress(0.f);
		if( m_myCharacter )
		{
			CharacterStatus* pStatus = m_myCharacter->getCharacterStatus();
			pStatus->energy += 1;
			if( pStatus->energy >= WCharacter::MAX_ENERGY )
			{
				pStatus->energy = WCharacter::MAX_ENERGY;
			}
		}
	}	
}