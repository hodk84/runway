#pragma once
#include "UISceneBase.h"

class CCMask;
class UISceneCareer : public UISceneBase
{
public : 

	static const int ButtonID_1st = 201;
	static const int ButtonID_2nd = 202;
	static const int ButtonID_3rd = 203;
	static const int ButtonID_4th = 204;
	static const int ButtonID_5th = 205;	

	UISceneCareer();
	virtual ~UISceneCareer();

	 virtual bool init();
	void loadItems(int type);
	
	void registerWithTouchDispatcher();
	virtual void visit();
	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);
	virtual void ccTouchCancelled(CCTouch* touch, CCEvent* event);
	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);

protected :
	void menuBackCallback(CCObject* pSender);
	void selectItemCallback(CCObject* pSender);
	void buyItemCallback(CCObject* pSender);
	
	float	m_fTick;
	bool	m_bAccelation;	
	float	m_fAccelPerTime;
	CCPoint	m_touchBeginPoint;
	float	m_touchBeginTime;
	
	CCSprite*	m_maskNode;	
	CCSprite*	m_rootNode;
	CCSprite*	m_result;
	CCMask*		m_pMask;

	float	m_fSpring;
	float	m_fVelo;

	CCMenu* m_pCategory;
};
	