
#pragma once;

#include "cocos2d.h"
#include "WCharacter.h"

using namespace cocos2d;

class UICtrlIndicator : public CCNode, CCTouchDelegate
{

public :

	UICtrlIndicator();

	virtual ~UICtrlIndicator();

	virtual bool init();

	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);

	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);

	virtual void ccTouchCancelled(CCTouch* touch, CCEvent* event);

	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
	
	virtual void touchDelegateRetain();
	virtual void touchDelegateRelease();

	virtual void update(ccTime dt);

	void	setIndicatorMoveProgress(float progress);
	float	getIndicatorMoveProgress();
	bool	getIndicatorMoved();

	bool	getIndicatorMovingStop(){ return m_isIndicatorMovingStoped; }
	void	setIndicatorMovingStop(bool b) { m_isIndicatorMovingStoped = b; }

protected:

	float			m_fTick;
	bool			m_bAccelation;	
	float			m_fAccelPerTime;
	CCPoint			m_touchBeginPoint;
	float			m_touchBeginTime;

	float			m_fSpring;
	float			m_fVelo;

	bool			m_isIndicatorMoved;
	bool			m_isIndicatorMovingStoped;

	WSprite*		m_pBG;
	WSprite*		m_pIndicatorImg;
};