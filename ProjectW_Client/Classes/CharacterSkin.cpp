#include "CharacterSkin.h"

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>
#include <libxml/xmlreader.h>


void CharacterSkin::addSkinPart(unsigned partId, unsigned resourceID)
{
	if( partId < 0 || partId > skinParts.size()) return;

	skinParts[partId] = resourceID;		
}

bool CharacterSkin::loadSkin(const char* fileName)
{
	const char* filePath = CCFileUtils::fullPathFromRelativePath(fileName);	
	xmlDoc *doc = NULL;
	xmlNode *root_element = NULL;
	doc = xmlReadFile(filePath, NULL, 0);	
	root_element = xmlDocGetRootElement(doc);
	assert(root_element);	
	if( NULL == root_element ) return false;

	if(0 == xmlStrcmp(root_element->name, (const xmlChar *)"CharacterSkin"))
	{			
		//xmlChar *cName = xmlGetProp(root_element, (const xmlChar *)"name");
		xmlNode* part = root_element->children;
		while(part)
		{
			if(0 == xmlStrcmp(part->name, (const xmlChar *)"part"))
			{
				int partId = 0;
				int resourceId = 0;
				xmlChar *cPartID = xmlGetProp(part, (const xmlChar *)"partID");
				if( cPartID )
				{
					partId = atoi((const char*)cPartID);
					xmlFree(cPartID);
				}
				xmlChar *cResID = xmlGetProp(part, (const xmlChar *)"resourceID");
				if( cResID )
				{
					resourceId = atoi((const char*)cResID);
					xmlFree(cResID);
				}

				if( 0 != resourceId )
				{
					addSkinPart(partId, resourceId);
				}
			}
			part = part->next;
		}
	}
	else
	{
		return false;
	}

	return true;
}
