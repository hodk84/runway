#include "XmlUtils.h"

string GetPropertyString(xmlNodePtr node, const char* prop)
{
	string strRet;
	xmlChar *cProp = xmlGetProp(node, (const xmlChar *)prop);
	if( cProp )
	{
		strRet.assign((const char*)cProp);
		xmlFree(cProp);
	}
	return strRet;
}

int GetPropertyInt(xmlNodePtr node, const char* prop)
{
	int ret = 0;
	xmlChar *cProp = xmlGetProp(node, (const xmlChar *)prop);
	if( cProp )
	{
		ret = atoi((const char*)cProp);
		xmlFree(cProp);
	}
	return ret;
}

unsigned int GetPropertyUInt(xmlNodePtr node, const char* prop)
{
	unsigned int ret = 0;
	xmlChar *cProp = xmlGetProp(node, (const xmlChar *)prop);
	if( cProp )
	{
		ret = atoi((const char*)cProp);
		xmlFree(cProp);
	}
	return ret;
}

float GetPropertyFloat(xmlNodePtr node, const char* prop)
{
	float ret = 0;
	xmlChar *cProp = xmlGetProp(node, (const xmlChar *)prop);
	if( cProp )
	{
		ret = atof((const char*)cProp);
		xmlFree(cProp);
	}
	return ret;
}

