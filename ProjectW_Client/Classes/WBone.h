#pragma once;

#include "Cocos2d.h"

using namespace cocos2d;

struct KeyAnimation;

class WBone : public CCNode
{
public :
	WBone();
		
	virtual void draw();	

	void SetDebugMode( bool debugMode);
		
	static WBone* CreateJoint(float width, float height, float ax = 0.5f, float ay=0.5f, bool anchorConvert = false, int depth = 1, bool showDebugPoint = true, const char* skinFile = NULL);

	//for animations
	void SetRotate(const KeyAnimation* anim, float duration);

	void SetMove(const KeyAnimation* anim, float duration);

	void SetDefaultSkinName(const char* fileName);
	void SetDefaultSkin();
	
	unsigned int m_id;
	float		m_origRot;
	CCPoint		m_origPos;
	float		pointSize;
	float		r, g, b;
	bool		m_bShowPoint;
	bool		m_bRelationLIne;
	string		m_defaultSkinName;
	int			m_depth;
};
typedef std::map<string, WBone*> WBoneNameMap;
typedef std::vector<WBone*> WBoneVec;
