#include "WSprite.h"
#include "DrawDebug.h"


WSprite::~WSprite()
{

}

void WSprite::SetRotate(const KeyAnimation* anim, float duration)
{
	if( NULL == anim ) return;
	if( anim->_rotate.activated )
	{
		//CCActionInterval* a1 = CCRotateBy::actionWithDuration(duration, anim->_rotation.angle);	
		CCActionInterval* a1 = CCRotateTo::actionWithDuration(duration, m_origRot + anim->_rotate.value);	
	
		CCAction* action1 = (CCActionInterval*)(CCSequence::actions(a1, NULL));		
		runAction(action1);
	}
}
	
void WSprite::SetMove(const KeyAnimation* anim, float duration)
{
	if( NULL == anim ) return;
	
	float targetX = m_origPos.x;
	float targetY = m_origPos.y;

	if( anim->_moveX.activated )
	{
		targetX = targetX + anim->_moveX.value ;//- pt.x;
	}
	if( anim->_moveX.activated )
	{
		targetY = targetY + anim->_moveY.value ;//- pt.y;
	}

	{
		//CCActionInterval* a1 = CCMoveBy::actionWithDuration(duration, CCPoint(targetX, targetY));	
		
		CCActionInterval* a1 = CCMoveTo::actionWithDuration(duration, CCPoint(targetX, targetY));	
		CCAction* action1 = (CCActionInterval*)(CCSequence::actions(a1, NULL));
		runAction(action1);			
	}
}

WSprite* WSprite::WSpriteWithFile(const char *pszFileName)
{
	WSprite *pobSprite = new WSprite();
    if (pobSprite && pobSprite->initWithFile(pszFileName))
    {		
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
	return NULL;
}


WSprite * WSprite::CreateNodeWSprite(const char* fileName, float px, float py, float ax, float ay, bool anchorConvert, int depth)
{	
	WSprite* sprite = WSprite::WSpriteWithFile(fileName);		
	if( sprite) 
	{
		if( anchorConvert )
		{
			CCSize size = sprite->getContentSizeInPixels();
			float convertedAx = size.width - (size.width - ax);
			float convertedAy = size.height - ay;
			
			DebugDraw*  dp = DebugDraw::create();
			dp->appendPoint(CCPoint(convertedAx, convertedAy), 1.f, 0.f, 0.f);
			//dp->appendLine(CCPoint(convertedAx, convertedAy), CCPoint(100, 500));
			sprite->addChild(dp);

			ax = convertedAx/size.width;
			ay = convertedAy/size.height;
		}
		
		sprite->setAnchorPoint(CCPoint(ax, ay));
		sprite->setPosition(CCPoint(px, py));
		sprite->m_origPos = CCPoint(px, py);
		//scale rotation
	}

	return sprite;
}