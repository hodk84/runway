#pragma once

#include "Cocos2d.h"
#include "Animator.h"
#include "WSprite.h"
#include "WSkeleton.h"
#include "CharacterSkin.h"
#include "WClothes.h"
#include "Datas\Work_N_Career.h"


using namespace cocos2d;

struct CharacterStatus
{
	std::string	characterName;
	int			energy;
	int			gameMoney;
	int			cash;
	int			level;
	int			clothCount;	
};

class WClothes;
class Cosmetic;
class WCharacter : public WSkeleton, KeyAnimationTarget
{
public : 
	static const int CosmeticTagOffset = 100;
	static const int MAX_ENERGY = 15;
	WCharacter();
	WCharacter(const CharacterStatus& data)
		: WSkeleton()
		, m_characterStatus(data)
		, m_characterId(0)
	{
		
	}

	virtual ~WCharacter();
	virtual bool playAnimation(int animationID);
	virtual void startKeyAnimation(const KeyAnimation* pAnim, float duration);
	virtual void startKeyAnimation(std::string str, float rotate);

	void	addAnimationSet(WAnimation* pAnimation);
	
	void	setSkin(CharacterSkin* skin);
	void	reloadSkin();

	void	applyClothes();
	
	void	setClothesApply(unsigned int clothID, bool isMine = false);
	void	setClothes(unsigned int clothId);
	void	setClothes(WClothes* pCloth);
	
	bool	loadDefaultCharacterData();

	void	addClothesCloset(unsigned int category, unsigned int clothID, bool wear = false);
	bool	hasClothes(unsigned int clothesID);

	void	applyCosmetic();
	void	setCosmetic(unsigned int cosmeticId);
	
	Closet					m_closet;

	void	setExpression(unsigned int coemeticPart, unsigned int expressionIdx);

	void	increaseCharacterEnergy(int add)
	{
		m_characterStatus.energy += add;
	}

	void	setCharacterStatus(CharacterStatus& data)
	{
		m_characterStatus = data;
	}
	CharacterStatus* getCharacterStatus()
	{
		return &m_characterStatus;
	}

	void	updateCharacterStatus();

	Career* getCareer(unsigned int jobId)
	{
		return m_career.getWork(jobId);
	}

	Career*	addCareer(unsigned int jobId)
	{
		return m_career.addWork(jobId);
	}

	int		getCharacterDayPoint();
	int		getCharacterNightPoint();

	unsigned int	getCharacterId() { return m_characterId; }
	void			setCharacterId(unsigned int characterId)
	{ 
		m_characterId = characterId;
	}

	void			tmpSetTagName(const char* name)
	{
		if( m_nameTag )
		{
			m_nameTag->setString(name);
		}
	}


private :

	unsigned int			m_characterId;
	CharacterStatus			m_characterStatus;

	CharacterSkin*			m_pCharacterSkin;
	vector<unsigned int>	m_wearedClothes;
	vector<unsigned int>	m_wearedCosmetic;

	Cosmetic*				m_cosmeticEye;
	Cosmetic*				m_cosmeticLip;
	Cosmetic*				m_cosmeticHairFront;
	Cosmetic*				m_cosmeticHairBack;

	WAnimationVec			m_animations;
	CareerStatus			m_career;

	CCLabelTTF*				m_nameTag;
};