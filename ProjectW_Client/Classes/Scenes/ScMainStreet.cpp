
#include "ScMainStreet.h"
#include "Managers/CharacterManager.h"
#include "HelloWorldScene.h"
#include "UI/UISceneBuilding.h"
#include "UI/UISceneTopMenu.h"
#include "UI/UISceneShowcase.h"
#include "Managers/SceneManager.h"
#include "UI/UICtrlIndicator.h"

ScMainStreet::ScMainStreet()
	: m_bAccelation(false)
	, m_fAccelPerTime(0.f)
	, m_touchBeginTime(0.f)
	, m_fTick(0.f)	
	, m_pBuildingMenu(NULL)
	, m_pParalNode(NULL)
	, m_pCharacterNode(NULL)
	, m_fSpring(0.f)
	, m_pIndicator(NULL)
{
	
}
ScMainStreet::~ScMainStreet()
{
	
}

// on "init" you need to initialize your instance
bool ScMainStreet::init()
{	
    bool bRet = false;
    do 
    {	
		CCLayer* pUILayer = CCLayer::node();		
		this->addChild(pUILayer, SceneBase::UILayerDepth, SceneBase::UILayerTag);

		UISceneTopMenu* pTopMenu = new UISceneTopMenu();
		pUILayer->addChild(pTopMenu, 5);

		/*UICtrlIndicator* pIndicator = new UICtrlIndicator();
		m_pIndicator = pIndicator;
		m_pIndicator->setPosition(ccp(350, 100));
		pUILayer->addChild(pIndicator, 10);
		m_pIndicator->setIsVisible(false);*/
		
		// create a void node, a parent node
		m_pParalNode = CCParallaxNode::node();			
		m_pParalNode->setAnchorPoint(ccp(0.f, 0.f));
		m_pParalNode->setPosition(ccp(0.f, 0.f));
		addChild(m_pParalNode, 0, kTag_ParralaxNode);

		loadScene("MainStreet.xml", m_pParalNode);	
	
		m_pBuildingMenu = new UISceneBuilding();
		m_pBuildingMenu->setPosition(ccp(100, 100));
		m_pParalNode->addChild(m_pBuildingMenu, 10, ccp(1.f, 1.f), CCPointZero);

		WCharacter* pCharacter = CharacterManager::getInst()->getMyCharacter();
		if( pCharacter )
		{
		 	pCharacter->setParent(NULL);
			pCharacter->setIsVisible(true);
			m_pCharacterNode = CCNode::node();
			m_pCharacterNode->addChild(pCharacter);			
			
			pCharacter->setScale(0.45f);
			pCharacter->setPosition(ccp(120.f, 220.f));
			
			m_pParalNode->addChild(m_pCharacterNode, 12, ccp(1.1f, 1.1f), CCPointZero);
		}
		
		m_fTick = 0.f;
		this->schedule( schedule_selector(ScMainStreet::update) ); 
		this->registerWithTouchDispatcher();
        bRet = true;
    } while (0);

    return bRet;
}


void ScMainStreet::registerWithTouchDispatcher()
{
    CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
}

bool ScMainStreet::ccTouchBegan(CCTouch* touch, CCEvent* event)
{	
	m_fSpring = 0.f;
	m_touchBeginPoint = touch->locationInView(0);
	m_touchBeginTime = m_fTick;
	
	return true;
}

void ScMainStreet::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	CCPoint ptEnd = touch->locationInView(0);	
	m_fAccelPerTime = (ptEnd.x - m_touchBeginPoint.x) / (m_fTick - m_touchBeginTime);
	if( m_fAccelPerTime )
	{
		m_pBuildingMenu->m_pMenu->ccTouchCancelled(touch, event);
	}
	else
	{
		m_pBuildingMenu->m_pMenu->ccTouchEnded(touch, event);
	}
}

void ScMainStreet::ccTouchCancelled(CCTouch* touch, CCEvent* event)
{
}

void ScMainStreet::ccTouchMoved(CCTouch* touch, CCEvent* event)
{
	CCPoint touchLocation = touch->locationInView( touch->view() );	
	CCPoint prevLocation = touch->previousLocationInView( touch->view() );	

	touchLocation = CCDirector::sharedDirector()->convertToGL( touchLocation );
	prevLocation = CCDirector::sharedDirector()->convertToGL( prevLocation );

	CCPoint diff = ccpSub(touchLocation,prevLocation);
	diff.y = 0.f;
	CCNode* node = getChildByTag(kTag_ParralaxNode);
	CCPoint currentPos = node->getPosition();
	
	CCPoint pt = ccpAdd(currentPos, diff);	
	if( pt.x < STREET_END)
	{
		float offset = pt.x + abs(STREET_END);	

		if( diff.x < -MOVING_TOLENRANCE) diff.x = -MOVING_TOLENRANCE;
		diff.x *= 1-(offset / -MOVING_TOLENRANCE);
	}
	if( pt.x > STREET_START)
	{
		float offset = pt.x + STREET_START;
		
		if( diff.x > MOVING_TOLENRANCE) diff.x = 50.f;
		diff.x *= 1-(offset / MOVING_TOLENRANCE);
	}
	
	CCPoint rst = ccpAdd(currentPos, diff);
	node->setPosition(rst);;

	if( m_pIndicator )
	{
		float range = abs(STREET_START + STREET_END);
		float progress = (rst.x-STREET_START)/range;
		m_pIndicator->setIndicatorMoveProgress(-progress);
	}
}

void ScMainStreet::menuBackCallback(CCObject* pSender)
{	
	SceneManager::getInst()->ChangeScene(SceneManager::kScene_Shop);	
}

 void ScMainStreet::update(ccTime dt)
 {	
	 if( fabs(m_fAccelPerTime) > 0.01f )
	 {
		CCNode* pNode = getChildByTag(kTag_ParralaxNode);
		CCPoint pt = pNode->getPosition();
		pt.x += m_fAccelPerTime*dt;
		
		if( pt.x < STREET_END )
		{
			m_fVelo = 0.f;
			m_fSpring = STREET_END;
			m_fAccelPerTime = 0.f;
		}
		if( pt.x > STREET_START)
		{
			m_fVelo = 0.f;
			m_fSpring = STREET_START;
			m_fAccelPerTime = 0.f;
		}

		m_fAccelPerTime *= 0.9f;//����
		pNode->setPosition(pt);
		if( fabs(m_fAccelPerTime) < 0.1f)
		{
			m_bAccelation = false;
		}

		pNode->setPosition(pt);
		
	 }
	 
	if( m_fSpring != 0.f )
	{
		const float k = 1;
		const float m = 10;
		const float d = 0.90f;
	
		CCNode* pNode = getChildByTag(kTag_ParralaxNode);
		CCPoint pt = pNode->getPosition();
		float dx = m_fSpring - pt.x;		
		float ax = k/m*dx;	
		
		m_fVelo += ax;		
		m_fVelo *= d;
		if( fabs(m_fVelo) < 0.01f)
		{
			m_fSpring = 0.f;
		}

		pt.x += m_fVelo;
		pNode->setPosition(pt);
	}

	m_fTick += dt;

	if( m_pIndicator && m_pIndicator->getIndicatorMoved())
	{
		float progress = m_pIndicator->getIndicatorMoveProgress();
		
		CCNode* pNode = getChildByTag(kTag_ParralaxNode);
		CCPoint pt = pNode->getPosition();
		float range = abs(STREET_START + STREET_END);
		float px = range * progress;
		pt.x = px + STREET_START;
		pNode->setPosition(pt);
	}
 }

 
void ScMainStreet::touchDelegateRetain()
{
	retain();
}

void ScMainStreet::touchDelegateRelease()
{
	release();
}