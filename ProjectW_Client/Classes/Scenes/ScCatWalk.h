#pragma once

#include "SceneBase.h"

class ScCatWalk : public SceneBase
{
public:
	ScCatWalk();
	virtual ~ScCatWalk();
		
	virtual bool init();

	virtual const char* getSceneName() { return "CatWalk"; }
	virtual void menuBackCallback(CCObject* pSender);
};

