#pragma once;

#include "SceneBase.h"

class SceneHome : public SceneBase
{
	const static unsigned int kTag_ParralaxNode = 33;	

public :
	
	SceneHome();
    virtual bool init(); 

    static cocos2d::CCScene* scene();
	LAYER_NODE_FUNC(SceneHome);

	virtual const char* getSceneName() { return "Home"; }

protected :

	virtual ~SceneHome();
	virtual void update(ccTime dt);


	float	m_fTick;
	bool	m_bAccelation;	
	float	m_fAccelPerTime;
	CCPoint	m_touchBeginPoint;
	float	m_touchBeginTime;
	
	CCNode*		m_pCharacterNode;
	
};