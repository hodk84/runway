#pragma once

#include "SceneBase.h"
class UISceneBuilding;
class ScCareerElevator : public SceneBase
{
	const static unsigned int kTag_ParralaxNode = 33;
	

public :
	ScCareerElevator();
	virtual ~ScCareerElevator();
    virtual bool init(); 

    static cocos2d::CCScene* scene();
	LAYER_NODE_FUNC(ScCareerElevator);

	virtual const char* getSceneName() { return "CareerElevator"; }

protected :

	virtual void update(ccTime dt);

	float	m_fTick;
	bool	m_bAccelation;	
	float	m_fAccelPerTime;
	CCPoint	m_touchBeginPoint;
	float	m_touchBeginTime;
	
	CCNode*		m_pCharacterNode;
	
};