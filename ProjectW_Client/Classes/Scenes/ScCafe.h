#pragma once

#include "SceneBase.h"
class UISceneBuilding;
class UISceneShowcase;
class ScCafe : public SceneBase
{		
	static const int ButtonID_All = 0;
	static const int ButtonID_Food = 1;
	static const int ButtonID_Drink = 2;
	static const int ButtonID_Bonus = 3;
	
public :	
    virtual bool init(); 
	
	ScCafe();
	virtual ~ScCafe();	

	virtual const char* getSceneName() { return "Cafe"; }
	
protected :
	virtual void messageBoxCallback(CCObject* pSender);	
	virtual void update(ccTime dt);

	void	menuBackCallback(CCObject* pSender);
	void	initButtons(CCLayer* pUILayer);
	void	itemSelectionCallback(CCObject* pSender);
	void	loadItems();
	void	loadItems(unsigned int merchandiseID, unsigned int type);

	float	m_fTick;
	bool	m_bAccelation;	
	float	m_fAccelPerTime;
	CCPoint	m_touchBeginPoint;
	float	m_touchBeginTime;
	
	CCNode*		m_pCharacterNode;
	CCMenu*		m_pCategory;
	UISceneShowcase* m_pShowcase;
};