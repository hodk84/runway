#pragma once

#include "SceneBase.h"

class UISceneLogin;
class UICtrlMessageBox;
class ScClub : public SceneBase
{
public:
	ScClub();
	virtual ~ScClub();
		
	virtual bool init();
	virtual void onEnter();


	virtual const char* getSceneName() { return "Club"; }	
	virtual void loginYesCallback(CCObject* pSender);
	virtual void loginNoCallback(CCObject* pSender);

	UISceneLogin*		m_pLoginUI;
	UICtrlMessageBox*	m_pMsgBox;
};

