#pragma once

#include "SceneBase.h"

class UISceneATM;
class ScATM : public SceneBase
{
		
	static const int ButtonID_All = 0;
	static const int ButtonID_Cash = 1;
	static const int ButtonID_Coin = 2;

public :	
    virtual bool init(); 
	
	ScATM();
	virtual ~ScATM();	

	virtual const char* getSceneName() { return "Shop"; }
	
protected :
	virtual void messageBoxCallback(CCObject* pSender);	
	virtual void update(ccTime dt);

	void	menuBackCallback(CCObject* pSender);
	void	initButtons(CCLayer* pUILayer);

	float	m_fTick;
	bool	m_bAccelation;	
	float	m_fAccelPerTime;
	CCPoint	m_touchBeginPoint;
	float	m_touchBeginTime;
	
	CCNode*		m_pCharacterNode;
	CCMenu*		m_pCategory;
	UISceneATM* m_pShowcase;
};