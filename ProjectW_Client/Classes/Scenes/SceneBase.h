#pragma once;

#include "SceneHeader.h"

using namespace cocos2d;

class SceneBase : public CCScene
{
public:
	static const int UILayerDepth = 50;
	static const int UILayerTag = 1111;

	SceneBase();
	virtual ~SceneBase(){}
	
	virtual bool init() { return true; }
	virtual const char* getSceneName() { return "Base"; }
	virtual void exit() {};
	virtual void messageBoxCallback(CCObject* pSender);
	virtual void onEnter() { CCScene::onEnter(); }

	CCNode*	getUILayer();

protected :
	virtual bool loadScene(const char* fileNameXml, CCNode* pParent);
	virtual bool parseSceneLayer(xmlNodePtr pNode, CCNode* parent, bool isParallaxNode = true);
	virtual bool parseSceneObject(xmlNodePtr pNode, CCNode* parent, const char* filePath);
	

};