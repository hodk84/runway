
#include "SceneHome.h"
#include "Managers/CharacterManager.h"
#include "HelloWorldScene.h"
#include "UI/UISceneHomeMenu.h"
#include "UI/UISceneTopMenu.h"

SceneHome::SceneHome()
	: m_bAccelation(false)
	, m_fAccelPerTime(0.f)
	, m_touchBeginTime(0.f)
	, m_fTick(0.f)	
	, m_pCharacterNode(NULL)
{
}
SceneHome::~SceneHome()
{

}

CCScene* SceneHome::scene()
{
    CCScene * scene = NULL;
    do 
    {
        scene = CCScene::node();
        CC_BREAK_IF(! scene);

        SceneHome *layer = SceneHome::node();
        CC_BREAK_IF(! layer);

        scene->addChild(layer);		

    } while (0);
	    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool SceneHome::init()
{	

    bool bRet = false;
    do 
    {	
		CCLayer* pUILayer = CCLayer::node();		
		this->addChild(pUILayer, SceneBase::UILayerDepth, SceneBase::UILayerTag);
	
		UISceneHomeMenu* pHomeMenu = new UISceneHomeMenu();
		pUILayer->setPosition(ccp(265, 0));
		pUILayer->addChild(pHomeMenu, 1);		
		
		CCLayer* pTopMenuLayer = CCLayer::node();		
		this->addChild(pTopMenuLayer, 99);
		UISceneTopMenu* pTopMenu = new UISceneTopMenu();
		pTopMenuLayer->addChild(pTopMenu, 2);
		pTopMenuLayer->setPosition(ccp(0, 0));
	
		CCSprite* pSprite = CCSprite::spriteWithFile("BG\\background.png");	
		pSprite->setPosition(ccp(pSprite->getContentSize().width/2.f, pSprite->getContentSize().height/2.f));
		addChild(pSprite, -1);
	
		WCharacter* pCharacter = CharacterManager::getInst()->getMyCharacter();
		if( pCharacter )
		{
			pCharacter->setParent(NULL);			
			pCharacter->setScale(1.f);
			pCharacter->setPosition(ccp(180.f, 330.f)); pCharacter->playAnimation(0);			
			pCharacter->setIsVisible(true);

			m_pCharacterNode = CCNode::node();
			m_pCharacterNode->addChild(pCharacter);			
			m_pCharacterNode->setPosition(ccp(50, 50));
			this->addChild(m_pCharacterNode, 1);
		}
	
		m_fTick = 0.f;
		this->schedule( schedule_selector(SceneHome::update) ); 

        bRet = true;
    } while (0);

    return bRet;
}

 void SceneHome::update(ccTime dt)
 {	
	 if( m_bAccelation )
	 {
		CCNode* pNode = getChildByTag(kTag_ParralaxNode);
		CCPoint pt = pNode->getPosition();
		pt.x += m_fAccelPerTime*dt;
		
		if( pt.x < -3000+800 )
			pt.x = -3000+800;
		if( pt.x > -10)
			pt.x = -10;

		m_fAccelPerTime *= 0.9f;//����
		pNode->setPosition(pt);
		if( fabs(m_fAccelPerTime) < 0.1f)
		{
			m_bAccelation = false;
		}

		/*const float k = 1;
		const float m = 10;
		const float d = 0.95f;

		float dx = touchPt.x - pt.x;		
		float ax = k/m*dx;	
		
		vx += ax;		
		vx *= d;		
		
		pt.x += vx;*/
		pNode->setPosition(pt);
		
	 }
	m_fTick += dt;	
 }