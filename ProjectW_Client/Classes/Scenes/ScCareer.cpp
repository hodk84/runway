
#include "ScCareer.h"
#include "Managers/CharacterManager.h"
#include "Managers/TableManager.h"
#include "HelloWorldScene.h"
#include "UI/UISceneHomeMenu.h"
#include "UI/UISceneTopMenu.h"
#include "UI/UISceneShowcase.h"
#include "UI/UICtrlWorkSlot.h"

ScCareer::ScCareer()
	: m_bAccelation(false)
	, m_fAccelPerTime(0.f)
	, m_touchBeginTime(0.f)
	, m_fTick(0.f)	
	, m_pCharacterNode(NULL)
{


}
ScCareer::~ScCareer()
{

}

// on "init" you need to initialize your instance
bool ScCareer::init()
{	

    bool bRet = false;
    do 
    {	
		CCLayer* pUILayer = CCLayer::node();		
		this->addChild(pUILayer, SceneBase::UILayerDepth, SceneBase::UILayerTag);
	
		UISceneTopMenu* pTopMenu = new UISceneTopMenu();
		pUILayer->addChild(pTopMenu, 5);

		{
			CCSprite* pSprite = CCSprite::spriteWithFile("bg\\ui_career_background.png");	
			pSprite->setPosition(ccp(pSprite->getContentSize().width/2.f, pSprite->getContentSize().height/2.f));
			addChild(pSprite, -1);
			
			int x = 608;
			int y = 120;
			int gap = 174;
			{
				UICtrlWorkSlot* slot = new UICtrlWorkSlot();
				slot->init();
				pSprite->addChild(slot);
				slot->setPosition(ccp(x, y));
				y += gap;

				JobData job;				
				if( TableManager::getInst()->getJob(100101, job) )				
				{
					slot->setWorkInfo(job);
				}	
			}
			{	
				UICtrlWorkSlot* slot = new UICtrlWorkSlot();				
				slot->init();				
				pSprite->addChild(slot);
				slot->setPosition(ccp(x, y));
				y += gap;
				JobData job;
				if( TableManager::getInst()->getJob(100102, job) )				
				{
					slot->setWorkInfo(job);
				}	
			}
			{
				UICtrlWorkSlot* slot = new UICtrlWorkSlot();				
				slot->init();				
				pSprite->addChild(slot);
				slot->setPosition(ccp(x, y));				
				JobData job;
				if( TableManager::getInst()->getJob(100103, job) )				
				{
					slot->setWorkInfo(job);
				}	
			}
		}

		WCharacter* pCharacter = CharacterManager::getInst()->getMyCharacter();
		if( pCharacter )
		{
			pCharacter->setParent(NULL);			
			pCharacter->setScale(1.f);
			pCharacter->setPosition(ccp(180.f, 330.f)); pCharacter->playAnimation(0);
			pCharacter->setIsVisible(true);
			pCharacter->playAnimation(0);

			m_pCharacterNode = CCNode::node();
			m_pCharacterNode->addChild(pCharacter);			
			this->addChild(m_pCharacterNode, 1);		
		}
		m_fTick = 0.f;
		this->schedule( schedule_selector(ScCareer::update) ); 

        bRet = true;
    } while (0);

    return bRet;
}

 void ScCareer::update(ccTime dt)
 {	
 }