#pragma once

#include "SceneBase.h"
class UISceneBuilding;
class ScShopElevator : public SceneBase
{

public :
	ScShopElevator();
	virtual ~ScShopElevator();
    virtual bool init(); 

	virtual const char* getSceneName() { return "ShopElevator"; }

protected :
	virtual void update(ccTime dt);

	float	m_fTick;
	bool	m_bAccelation;	
	float	m_fAccelPerTime;
	CCPoint	m_touchBeginPoint;
	float	m_touchBeginTime;
	
	CCNode*		m_pCharacterNode;
	
};