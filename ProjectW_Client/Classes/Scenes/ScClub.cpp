#include "ScClub.h"
#include "UI/UISceneTopMenu.h"
#include "UI/UISceneLogin.h"
#include "UI/UICtrlMessageBox.h"
#include "Managers/NetworkManager.h"


ScClub::ScClub()
{
}

ScClub::~ScClub()
{
}

bool ScClub::init()
{
	do
	{
		CCLayer* pUILayer = CCLayer::node();		
		this->addChild(pUILayer, SceneBase::UILayerDepth, SceneBase::UILayerTag);
	
		UISceneTopMenu* pTopMenu = new UISceneTopMenu();
		pUILayer->addChild(pTopMenu, 5);//탑메뉴

		CCSprite* pSprite = CCSprite::spriteWithFile("bg\\club_background_1.PNG");//임시배경
		pSprite->setPosition(ccp(pSprite->getContentSize().width/2.f, pSprite->getContentSize().height/2.f));
		addChild(pSprite);

		// Load Left Character
		WCharacter* pCharacter = CharacterManager::getInst()->getMyCharacter();
		if( pCharacter )
		{
			CCNode* leftNode = CCNode::node();
			leftNode->addChild( pCharacter );
			pCharacter->setScale(1.f);
			pCharacter->setPosition(ccp(280.f, 300.f));
			addChild(leftNode);			
		}


		//NetworkManager::getInst()->getClubUserInfo();
		/*WCharacter* otherCharacter = CharacterManager::getInst()->loadCharacterByXml("UserInfo.xml");
		if( pCharacter )
		{
			CCNode* rightNode = CCNode::node();
			rightNode->addChild( otherCharacter );
			rightNode->setScale(1.f);
			rightNode->setPosition(ccp(780.f, 300.f));
			addChild(rightNode);			
		}*/
		
		UISceneLogin* loginUI = new UISceneLogin();
		this->addChild(loginUI);
		loginUI->setIsVisible(false);


	} while(0);
		
	return true;
}

void ScClub::onEnter()
{
	m_pMsgBox = UICtrlMessageBox::CreateMessageBox();
	m_pMsgBox->initWithTarget(this, menu_selector(ScClub::loginYesCallback), menu_selector(ScClub::loginNoCallback));
	m_pMsgBox->setMessage("Hahahah don't" );
	addChild(m_pMsgBox);

	SceneBase::onEnter();
}


void ScClub::loginYesCallback(CCObject* pSender)
{
	UISceneLogin* loginUI = new UISceneLogin();
	this->addChild(loginUI);
	m_pMsgBox->setIsVisible(false);
}

void ScClub::loginNoCallback(CCObject* pSender)
{
	m_pMsgBox->setIsVisible(false);
}
