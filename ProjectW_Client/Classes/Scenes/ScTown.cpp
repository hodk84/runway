
#include "ScTown.h"
#include "Managers/CharacterManager.h"
#include "Managers/SceneManager.h"
#include "UI/UISceneHomeMenu.h"
#include "UI/UISceneTopMenu.h"
#include "UI/UISceneTownBuilding.h"

ScTown::ScTown()
	: m_bAccelation(false)
	, m_fAccelPerTime(0.f)
	, m_touchBeginTime(0.f)
	, m_fTick(0.f)	
	, m_pCharacterNode(NULL)
{
}
ScTown::~ScTown()
{

}

// on "init" you need to initialize your instance
bool ScTown::init()
{	

    bool bRet = false;
    do 
    {	
		CCLayer* pUILayer = CCLayer::node();		
		this->addChild(pUILayer, SceneBase::UILayerDepth, SceneBase::UILayerTag);
			
		CCLayer* pTopMenuLayer = CCLayer::node();		
		this->addChild(pTopMenuLayer, 99);
		UISceneTopMenu* pTopMenu = new UISceneTopMenu();
		pTopMenuLayer->addChild(pTopMenu, 2);
		pTopMenuLayer->setPosition(ccp(0, 0));
	
		CCSprite* pSprite = CCSprite::spriteWithFile("BG\\town.png");	
		pSprite->setPosition(ccp(pSprite->getContentSize().width/2.f, pSprite->getContentSize().height/2.f));
		addChild(pSprite, -1);

		m_pBuildingMenu = new UISceneTownBuilding();
		m_pBuildingMenu->setPosition(ccp(100, 100));
		pSprite->addChild(m_pBuildingMenu, 1);
	
		m_fTick = 0.f;
		this->schedule( schedule_selector(ScTown::update) ); 
		this->registerWithTouchDispatcher();
        bRet = true;
    } while (0);

    return bRet;
}

 void ScTown::update(ccTime dt)
 {	
	 if( m_bAccelation )
	 {
		CCNode* pNode = getChildByTag(kTag_ParralaxNode);
		CCPoint pt = pNode->getPosition();
		pt.x += m_fAccelPerTime*dt;
		
		if( pt.x < -3000+800 )
			pt.x = -3000+800;
		if( pt.x > -10)
			pt.x = -10;

		m_fAccelPerTime *= 0.9f;//����
		pNode->setPosition(pt);
		if( fabs(m_fAccelPerTime) < 0.1f)
		{
			m_bAccelation = false;
		}

		/*const float k = 1;
		const float m = 10;
		const float d = 0.95f;

		float dx = touchPt.x - pt.x;		
		float ax = k/m*dx;	
		
		vx += ax;		
		vx *= d;		
		
		pt.x += vx;*/
		pNode->setPosition(pt);
		
	 }
	m_fTick += dt;	
 }

 
void ScTown::registerWithTouchDispatcher()
{
    CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
}

bool ScTown::ccTouchBegan(CCTouch* touch, CCEvent* event)
{	
	m_fSpring = 0.f;
	m_touchBeginPoint = touch->locationInView(0);
	m_touchBeginTime = m_fTick;
	return true;
}

void ScTown::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	CCPoint ptEnd = touch->locationInView(0);	
	m_fAccelPerTime = (ptEnd.x - m_touchBeginPoint.x) / (m_fTick - m_touchBeginTime);
	if( m_fAccelPerTime )
	{
		m_pBuildingMenu->m_pMenu->ccTouchCancelled(touch, event);
	}
	else
	{
		m_pBuildingMenu->m_pMenu->ccTouchEnded(touch, event);
	}
}

void ScTown::ccTouchCancelled(CCTouch* touch, CCEvent* event)
{
}

void ScTown::ccTouchMoved(CCTouch* touch, CCEvent* event)
{
	
}

void ScTown::menuBackCallback(CCObject* pSender)
{	
	SceneManager::getInst()->ChangeScene(SceneManager::kScene_Shop);	
}

 
void ScTown::touchDelegateRetain()
{
	retain();
}

void ScTown::touchDelegateRelease()
{
	release();
}