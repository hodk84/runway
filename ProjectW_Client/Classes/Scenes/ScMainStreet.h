#pragma once;

#include "SceneBase.h"
class UISceneBuilding;
class WCharacter;
class UICtrlIndicator;
class ScMainStreet : public SceneBase, CCTouchDelegate
{
public :
	const static unsigned int kTag_ParralaxNode = 33;
	const static int	STREET_START = -10;
	const static int	STREET_END = -1840;
	const static int	MOVING_TOLENRANCE = 50;

public :
	ScMainStreet();
	virtual ~ScMainStreet();
    virtual bool init(); 

	virtual void menuBackCallback(CCObject* pSender);
	virtual const char* getSceneName() { return "MainStreet"; }

protected :
	
	
	void registerWithTouchDispatcher();

	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);

	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);

	virtual void ccTouchCancelled(CCTouch* touch, CCEvent* event);

	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);

	virtual void update(ccTime dt);

	virtual void touchDelegateRetain();
	virtual void touchDelegateRelease();
	

	float	m_fTick;
	bool	m_bAccelation;	
	float	m_fAccelPerTime;
	CCPoint	m_touchBeginPoint;
	float	m_touchBeginTime;
	CCParallaxNode* m_pParalNode;
	CCNode*	m_pCharacterNode;
	
	UICtrlIndicator* m_pIndicator;
	UISceneBuilding* m_pBuildingMenu;
	float	m_fSpring;
	float	m_fVelo;
};