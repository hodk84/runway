
#include "ScMainStreet2.h"
#include "Managers/CharacterManager.h"
#include "HelloWorldScene.h"
#include "UI/UISceneBuilding.h"
#include "UI/UISceneTopMenu.h"
#include "UI/UISceneShowcase.h"
#include "Managers/SceneManager.h"
#include "UI/UICtrlIndicator.h"

ScMainStreet2::ScMainStreet2()
	: m_bAccelation(false)
	, m_fAccelPerTime(0.f)
	, m_touchBeginTime(0.f)
	, m_fTick(0.f)	
	, m_pBuildingMenu(NULL)
	, m_pParalNode(NULL)
	, m_pCharacterNode(NULL)
	, m_fSpring(0.f)
{
	
}
ScMainStreet2::~ScMainStreet2()
{
	
}

// on "init" you need to initialize your instance
bool ScMainStreet2::init()
{	
    bool bRet = false;
    do 
    {	
		CCLayer* pUILayer = CCLayer::node();		
		this->addChild(pUILayer, SceneBase::UILayerDepth, SceneBase::UILayerTag);

		UISceneTopMenu* pTopMenu = new UISceneTopMenu();
		pUILayer->addChild(pTopMenu, 5);

		/*UICtrlIndicator* pIndicator = new UICtrlIndicator();
		m_pIndicator = pIndicator;
		m_pIndicator->setPosition(ccp(350, 100));
		pUILayer->addChild(pIndicator, 10);*/
		
		// create a void node, a parent node
		WCharacter* pCharacter = CharacterManager::getInst()->getMyCharacter();
		if( pCharacter )
		{
		 	pCharacter->setParent(NULL);
			pCharacter->setIsVisible(true);
			m_pCharacterNode = CCNode::node();
			m_pCharacterNode->addChild(pCharacter);			
			
			pCharacter->setScale(0.45f);
			pCharacter->setPosition(ccp(100.f, 200.f));
			pCharacter->playAnimation(0);
			addChild(m_pCharacterNode, 12);
		}
		
		m_fTick = 0.f;
		this->schedule( schedule_selector(ScMainStreet2::update) ); 
		this->registerWithTouchDispatcher();
        bRet = true;

		m_bg1 = CCSprite::spriteWithFile("bg\\runway_background.PNG");//임시배경
		m_bg1->setPosition(ccp(m_bg1->getContentSize().width/2.f, m_bg1->getContentSize().height/2.f));
		addChild(m_bg1);

		m_bg2 = CCSprite::spriteWithFile("bg\\club_background_1.PNG");//임시배경
		m_bg2->setPosition(ccp(m_bg2->getContentSize().width/2.f, m_bg2->getContentSize().height/2.f));
		addChild(m_bg2);


    } while (0);

    return bRet;
}


void ScMainStreet2::registerWithTouchDispatcher()
{
    CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
}

bool ScMainStreet2::ccTouchBegan(CCTouch* touch, CCEvent* event)
{	
	
	return true;
}

void ScMainStreet2::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	
}

void ScMainStreet2::ccTouchCancelled(CCTouch* touch, CCEvent* event)
{
}

void ScMainStreet2::ccTouchMoved(CCTouch* touch, CCEvent* event)
{	
}

void ScMainStreet2::menuBackCallback(CCObject* pSender)
{	
	SceneManager::getInst()->ChangeScene(SceneManager::kScene_Shop);	
}

 void ScMainStreet2::update(ccTime dt)
 {	
	if( m_pIndicator && m_pIndicator->getIndicatorMovingStop() )
	{
		float progress = m_pIndicator->getIndicatorMoveProgress();
		if( progress < -0.5f )
		{				
			m_bg1->runAction(CCFadeTo::actionWithDuration(1.f, 0));
			m_bg2->runAction(CCFadeTo::actionWithDuration(1.f, 255));
		}
		else
		{	
			m_bg1->runAction(CCFadeTo::actionWithDuration(1.f, 255));
			m_bg2->runAction(CCFadeTo::actionWithDuration(1.f, 0));
		}
		m_pIndicator->setIndicatorMovingStop(false);
	}
	SceneBase::update(dt);
 }

 
void ScMainStreet2::touchDelegateRetain()
{
	retain();
}

void ScMainStreet2::touchDelegateRelease()
{
	release();
}