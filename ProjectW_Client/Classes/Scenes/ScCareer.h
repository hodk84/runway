#pragma once

#include "SceneBase.h"
class UISceneBuilding;
class ScCareer : public SceneBase
{
public :	
	
	ScCareer();
	virtual ~ScCareer();
    virtual bool init(); 

    static cocos2d::CCScene* scene();
	LAYER_NODE_FUNC(ScCareer);

	virtual const char* getSceneName() { return "Career"; }

protected :

	
	virtual void update(ccTime dt);
	
	void registerWithTouchDispatcher();
	

	float	m_fTick;
	bool	m_bAccelation;	
	float	m_fAccelPerTime;
	CCPoint	m_touchBeginPoint;
	float	m_touchBeginTime;
	
	CCNode*		m_pCharacterNode;
	
};