#include "ScCatWalk.h"
#include "UI/UISceneTopMenu.h"

ScCatWalk::ScCatWalk()
{
}

ScCatWalk::~ScCatWalk()
{
}

bool ScCatWalk::init()
{
	do
	{
		CCLayer* pUILayer = CCLayer::node();		
		this->addChild(pUILayer, SceneBase::UILayerDepth, SceneBase::UILayerTag);
	
		UISceneTopMenu* pTopMenu = new UISceneTopMenu();
		pUILayer->addChild(pTopMenu, 5);//탑메뉴

		CCSprite* pSprite = CCSprite::spriteWithFile("bg\\runway_background.PNG");//임시배경
		pSprite->setPosition(ccp(pSprite->getContentSize().width/2.f, pSprite->getContentSize().height/2.f));
		addChild(pSprite);

		// Load Left Character
		WCharacter* pCharacter = CharacterManager::getInst()->getMyCharacter();
		if( pCharacter )
		{
			CCNode* leftNode = CCNode::node();
			leftNode->addChild( pCharacter );
			pCharacter->setScale(1.f);
			pCharacter->setPosition(ccp(280.f, 360.f));
			addChild(leftNode);			
		}
	} while(0);

	return true;
}

void ScCatWalk::menuBackCallback(CCObject* pSender)
{
}