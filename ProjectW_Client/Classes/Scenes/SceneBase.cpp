#include "SceneBase.h"
#include "UI/UICtrlMessageBox.h"

SceneBase::SceneBase()
{
	bool bRet = false;
 	do 
 	{
 		CCDirector * pDirector;
 		CC_BREAK_IF( ! (pDirector = CCDirector::sharedDirector()) );
 		this->setContentSize(pDirector->getWinSize()); 	
 		
 	} while (0); 
}

bool SceneBase::loadScene(const char* fileNameXml, CCNode* pParent)
{
	if( NULL == pParent ) return false;

	const char* filePath = CCFileUtils::fullPathFromRelativePath(fileNameXml);
	
	xmlDoc *doc = NULL;
	xmlNode *root_element = NULL;
	doc = xmlReadFile(filePath, NULL, 0);
	if( NULL == doc ) return false;

	root_element = xmlDocGetRootElement(doc);
	assert(root_element);	
	if( NULL == root_element) return false;

	xmlNode* pChild = root_element->children;
	while(pChild)//"Node" Element를 찾음.
	{
		//첫번째 Node가 Root가 되며 Root는 유일하다.
		if(0 == xmlStrcmp(pChild->name, (const xmlChar *)"layer"))
		{			
			parseSceneLayer(pChild, pParent); 
		}
		pChild = pChild->next;
	}

	xmlFreeDoc(doc);

	return true;
}

bool SceneBase::parseSceneLayer(xmlNodePtr pXmlNode, CCNode* parent, bool isParallaxNode)
{
	if(0 != xmlStrcmp(pXmlNode->name, (const xmlChar *)"layer"))
	return false;

	xmlChar *cName = xmlGetProp(pXmlNode, (const xmlChar *)"name");

	int depth = 0;
	int id = 0;
	float ratio = 1.f;
	bool anchorConvert = 0;

	xmlChar *cId = xmlGetProp(pXmlNode, (const xmlChar *)"id");
	if( cId)
	{ 
		id = atoi((const char*)cId);
		xmlFree(cId);
	}

	xmlChar *cDepth = xmlGetProp(pXmlNode, (const xmlChar *)"depth");
	if( cDepth)
	{ 
		depth = atoi((const char*)cDepth);
		xmlFree(cDepth);
	}
	
	xmlChar *cRatio = xmlGetProp(pXmlNode, (const xmlChar *)"ratio");
	if( cRatio )
	{
		ratio = atof((const char*)cRatio);
		xmlFree(cRatio);
	}
	
	CCNode* pNode = parent->getChildByTag(id);
	if( NULL == pNode )
	{
		pNode = CCNode::node();
	}

	xmlNode* pChild = pXmlNode->children;
	while(pChild)//"Node" Element를 찾음.
	{
		//첫번째 Node가 Root가 되며 Root는 유일하다.
		if(0 == xmlStrcmp(pChild->name, (const xmlChar *)"object"))
		{			
			parseSceneObject(pChild, pNode, (const char*)cName);
		}
		pChild = pChild->next;
	}
	xmlFree(cName);
	if( isParallaxNode )
	{
		CCParallaxNode* pParallaxNode = dynamic_cast<CCParallaxNode*>(parent);
		if( NULL == pParallaxNode )
		{
			assert(0);
			return false;
		}
		pParallaxNode->addChild(pNode, depth, ccp(ratio, ratio), CCPointZero);
	}
	else
	{
		parent->addChild(pNode, depth, id);
	}

	return true;
}

bool SceneBase::parseSceneObject(xmlNodePtr pNode, CCNode* parent, const char* filePath)
{
	if(0 != xmlStrcmp(pNode->name, (const xmlChar *)"object"))
	return false;

	
	xmlChar *cFileName = xmlGetProp(pNode, (const xmlChar *)"file");
	
	float posX = 0;
	float posY = 0;
	int tileCount = 1;
	float offsetX = 0;
	float offsetY = 0;

	float tileX = 0;
	float tileY = 0;
	
	xmlChar *cPosX = xmlGetProp(pNode, (const xmlChar *)"x");
	if( cPosX)
	{ 
		posX = atof((const char*)cPosX);
		xmlFree(cPosX);
	}
	
	xmlChar *cPosY = xmlGetProp(pNode, (const xmlChar *)"y");
	if( cPosY )
	{
		posY = atof((const char*)cPosY);
		xmlFree(cPosY);
	}

	
	xmlChar *cTileCount = xmlGetProp(pNode, (const xmlChar *)"tiling");
	if( cTileCount)
	{ 
		tileCount = atoi((const char*)cTileCount);
		xmlFree(cTileCount);
	}

	xmlChar *cOffsetX = xmlGetProp(pNode, (const xmlChar *)"offsetX");
	if( cOffsetX)
	{ 
		offsetX = atof((const char*)cOffsetX);
		xmlFree(cOffsetX);
	}
	
	xmlChar *cOffsetY = xmlGetProp(pNode, (const xmlChar *)"offsetY");
	if( cOffsetY )
	{
		offsetY = atof((const char*)cOffsetY);
		xmlFree(cOffsetY);
	}
	
	char fileDest[256];
	sprintf(fileDest, "%s\\%s",filePath, (const char*)cFileName);	
	for(int i = 0; i < tileCount; ++i, tileX+=offsetX, tileY+=offsetY)
	{
		CCSprite* pSprite = CCSprite::spriteWithFile(fileDest);		
		if( pSprite)
		{
			pSprite->setAnchorPoint(ccp(0.f, 0.f));
			pSprite->setPosition(ccp(posX + tileX, posY + tileY));
			parent->addChild(pSprite);			
		}
	}
	xmlFree(cFileName);
	return true;
}

void SceneBase::messageBoxCallback(CCObject* pSender)
{
	CCNode* pMB = dynamic_cast<CCNode*>(pSender);
	if( pMB )
	{
		this->removeChildByTag(UICtrlMessageBox::DEFAULT_TAG, true);
	}
}


CCNode*	SceneBase::getUILayer()
{
	return this->getChildByTag(SceneBase::UILayerTag);
}