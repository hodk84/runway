
#include "ScCafe.h"
#include "Managers/CharacterManager.h"
#include "Managers/TableManager.h"
#include "HelloWorldScene.h"
#include "UI/UISceneHomeMenu.h"
#include "UI/UISceneTopMenu.h"
#include "UI/UISceneShowcase.h"
#include "UI/UICtrlItemTag.h"


ScCafe::ScCafe()
	: m_bAccelation(false)
	, m_fAccelPerTime(0.f)
	, m_touchBeginTime(0.f)
	, m_fTick(0.f)	
	, m_pCharacterNode(NULL)
	, m_pCategory(NULL)
	, m_pShowcase(NULL)
{


}
ScCafe::~ScCafe()
{

}
void ScCafe::initButtons(CCLayer* pUILayer)
{
	if( NULL == pUILayer ) return;

	std::vector<CCMenuItemImage*> btnVec;
	int x = 0; 
	int y = 0;
	int add = 110;
	{
		CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
		"ui\\common_button_all.png","ui\\common_button_all_1.png",
		this, menu_selector(ScCafe::menuBackCallback));		
		
		pButton->setPosition(ccp(x, y));
		pButton->setTag(ButtonID_All);
		btnVec.push_back(pButton);		
	}
	x += add;
	{
		CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
		"ui\\common_button_food.png","ui\\common_button_food_1.png",
		this, menu_selector(ScCafe::menuBackCallback));
					
		pButton->setPosition(ccp(x, y));
		pButton->setTag(ButtonID_Food);
		btnVec.push_back(pButton);
	}
	x += add;
	{
		CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
		"ui\\common_button_drink.png","ui\\common_button_drink_1.png",
		this, menu_selector(ScCafe::menuBackCallback));
		
		pButton->setPosition(ccp(x, y));
		pButton->setTag(ButtonID_Drink);
		btnVec.push_back(pButton);
	}
	x += add;
	{
		CCMenuItemImage *pButton = CCMenuItemImage::itemFromNormalImage(
		"ui\\common_button_bonus.png","ui\\common_button_bonus_1.png",
		this, menu_selector(ScCafe::menuBackCallback));
		
		pButton->setPosition(ccp(x, y));
		pButton->setTag(ButtonID_Bonus);
		btnVec.push_back(pButton);
	}
	
	m_pCategory = CCMenu::menuWithItems(btnVec[0],btnVec[1], btnVec[2], btnVec[3], NULL);
	m_pCategory->setPosition(ccp(330, 525));
	pUILayer->addChild(m_pCategory, 5);
}

void ScCafe::menuBackCallback(CCObject* pSender)
{
	CCMenuItemImage* item = dynamic_cast<CCMenuItemImage*>(pSender);
	if( NULL == item )
		return;
	
	ccArray *arrayData = m_pCategory->getChildren()->data;
    for(int i=0 ; i < arrayData->num; i++ )
    {
		CCMenuItemImage* pNode = (CCMenuItemImage*) arrayData->arr[i];
		if( pNode )
		{
			pNode->unselected();
		}
	}
	item->selected();
	
	unsigned int type = item->getTag();
	loadItems(200, type);				
}

void ScCafe::loadItems(unsigned int merchandiseID, unsigned int type)
{
	m_pShowcase->resetShowcase();

	MerchandiseItemVec items;
	TableManager::getInst()->getMerchandiseSet(merchandiseID, type, items);	
	if( items.empty() ) return;
		
	MerchandiseItemVec::iterator itr = items.begin();	
	std::vector<CCMenuItem*> tagVec;
	for( itr; itr != items.end(); ++itr)
	{	
		UICtrlItemTag* pTag = UICtrlItemTag::itemFromNormalImage("UI\\shop_slot_01.png", "UI\\shop_slot_01_o.png",
			NULL, this, menu_selector(ScCafe::itemSelectionCallback));	
		if( NULL == pTag ) continue;
		pTag->setInfo((*itr));
		pTag->setTag((*itr)->merchandiseID);	
				
		m_pShowcase->addItemTag(pTag);
	}	
}

void ScCafe::itemSelectionCallback(CCObject* pSender)
{

}

bool ScCafe::init()
{	

    bool bRet = false;
    do 
    {	

		CCLayer* pUILayer = CCLayer::node();		
		this->addChild(pUILayer, SceneBase::UILayerDepth, SceneBase::UILayerTag);
		this->setAnchorPoint(ccp(0, 0));

		UISceneTopMenu* pTopMenu = new UISceneTopMenu();
		pUILayer->addChild(pTopMenu, 5);
		pUILayer->setAnchorPoint(ccp(0,0));

		initButtons(pUILayer);

		m_pShowcase = new UISceneShowcase();
		m_pShowcase->setPosition(ccp(280, 50));
		addChild(m_pShowcase, -1);
		m_pShowcase->init();

		{
			CCSprite* pSprite = CCSprite::spriteWithFile("bg\\bgofbg.png");	
			pSprite->setPosition(ccp(pSprite->getContentSize().width/2.f, pSprite->getContentSize().height/2.f));
			addChild(pSprite, -5);
		}
		{
			CCSprite* pSprite = CCSprite::spriteWithFile("bg\\ui_cafe_background.png");	
			pSprite->setAnchorPoint(ccp(0, 0));
			pSprite->setPosition(ccp(0, 0));
			addChild(pSprite, 2);
		}

		{
			CCSprite* pSprite = CCSprite::spriteWithFile("bg\\shop_background_hollow.png");	
			pSprite->setPosition(ccp(pSprite->getContentSize().width/2.f, pSprite->getContentSize().height/2.f));
			addChild(pSprite, 1);
		}
		/*{
			CCSprite* pSprite = CCSprite::spriteWithFile("bg\\shop_background_03.png");	
			pSprite->setPosition(ccp(pSprite->getContentSize().width/2.f+256.f, pSprite->getContentSize().height/2.f));
			addChild(pSprite, -1);
		}
		*/
	
		WCharacter* pCharacter = CharacterManager::getInst()->getMyCharacter();
		if( pCharacter )
		{
			pCharacter->setParent(NULL);			
			pCharacter->setScale(1.f);
			pCharacter->setPosition(ccp(180.f, 330.f)); pCharacter->playAnimation(0);			
			pCharacter->setIsVisible(true);

			m_pCharacterNode = CCNode::node();
			m_pCharacterNode->addChild(pCharacter);			
			this->addChild(m_pCharacterNode, 3);
		}
	
		m_fTick = 0.f;
		this->schedule( schedule_selector(ScCafe::update) ); 

        bRet = true;
    } while (0);

    return bRet;
}
void ScCafe::loadItems()
{

}


 void ScCafe::update(ccTime dt)
 {	
	 if( m_bAccelation )
	 {
		CCNode* pNode = getChildByTag(1);
		CCPoint pt = pNode->getPosition();
		pt.x += m_fAccelPerTime*dt;
		
		if( pt.x < -3000+800 )
			pt.x = -3000+800;
		if( pt.x > -10)
			pt.x = -10;

		m_fAccelPerTime *= 0.9f;//����
		pNode->setPosition(pt);
		if( fabs(m_fAccelPerTime) < 0.1f)
		{
			m_bAccelation = false;
		}

		/*const float k = 1;
		const float m = 10;
		const float d = 0.95f;

		float dx = touchPt.x - pt.x;		
		float ax = k/m*dx;	
		
		vx += ax;		
		vx *= d;		
		
		pt.x += vx;*/
		pNode->setPosition(pt);
		
	 }
	m_fTick += dt;	
 }

 void ScCafe::messageBoxCallback(CCObject* pSender)
 {
	 CCNode* pNode = dynamic_cast<CCNode*>(pSender);
	
	 this->removeChildByTag(1, true);
 }