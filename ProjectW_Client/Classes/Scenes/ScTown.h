#pragma once;

#include "SceneBase.h"

class UISceneTownBuilding;
class WCharacter;
class ScTown : public SceneBase, CCTouchDelegate
{
	const static unsigned int kTag_ParralaxNode = 33;
	

public :
	ScTown();
	virtual ~ScTown();
    virtual bool init(); 

	virtual void menuBackCallback(CCObject* pSender);
	virtual const char* getSceneName() { return "Town"; }

protected :
	
	
	void registerWithTouchDispatcher();

	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);

	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);

	virtual void ccTouchCancelled(CCTouch* touch, CCEvent* event);

	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);

	virtual void update(ccTime dt);

	virtual void touchDelegateRetain();
	virtual void touchDelegateRelease();
	

	float	m_fTick;
	bool	m_bAccelation;	
	float	m_fAccelPerTime;
	CCPoint	m_touchBeginPoint;
	float	m_touchBeginTime;
	CCParallaxNode* m_pParalNode;
	CCNode*	m_pCharacterNode;

	UISceneTownBuilding* m_pBuildingMenu;
	float	m_fSpring;
	float	m_fVelo;
};