
#include "ScCareerElevator.h"
#include "Managers/CharacterManager.h"
#include "HelloWorldScene.h"
#include "UI/UISceneHomeMenu.h"
#include "UI/UISceneTopMenu.h"
#include "UI/UISceneElevator.h"

ScCareerElevator::ScCareerElevator()
	: m_bAccelation(false)
	, m_fAccelPerTime(0.f)
	, m_touchBeginTime(0.f)
	, m_fTick(0.f)	
	, m_pCharacterNode(NULL)
{


}
ScCareerElevator::~ScCareerElevator()
{

}

// on "init" you need to initialize your instance
bool ScCareerElevator::init()
{	

    bool bRet = false;
    do 
    {	
		CCLayer* pUILayer = CCLayer::node();		
		this->addChild(pUILayer, SceneBase::UILayerDepth, SceneBase::UILayerTag);
	
		UISceneTopMenu* pTopMenu = new UISceneTopMenu();
		pUILayer->addChild(pTopMenu, 5);

		UISceneElevator* pElevator = new UISceneElevator();
		pElevator->setPosition(ccp(280, 50));
		pUILayer->addChild(pElevator, 1);
		pElevator->init();

		{
			CCSprite* pSprite = CCSprite::spriteWithFile("bg\\ui_career_background_1.png");	
			pSprite->setPosition(ccp(pSprite->getContentSize().width/2.f, pSprite->getContentSize().height/2.f));
			addChild(pSprite, -1);
		}

		/*{
			CCSprite* pSprite = CCSprite::spriteWithFile("bg\\shop_background_02.png");	
			pSprite->setPosition(ccp(pSprite->getContentSize().width/2.f, pSprite->getContentSize().height/2.f));
			addChild(pSprite, -1);
		}*/
		/*{
			CCSprite* pSprite = CCSprite::spriteWithFile("bg\\shop_background_03.png");	
			pSprite->setPosition(ccp(pSprite->getContentSize().width/2.f+256.f, pSprite->getContentSize().height/2.f));
			addChild(pSprite, -1);
		}
		*/
	
		WCharacter* pCharacter = CharacterManager::getInst()->getMyCharacter();
		if( pCharacter )
		{
			pCharacter->setParent(NULL);			
			pCharacter->setScale(1.f);
			pCharacter->setPosition(ccp(180.f, 330.f)); pCharacter->playAnimation(0);			
			pCharacter->setIsVisible(true);

			m_pCharacterNode = CCNode::node();
			m_pCharacterNode->addChild(pCharacter);			
			this->addChild(m_pCharacterNode, 1);
		}
	
		m_fTick = 0.f;
		this->schedule( schedule_selector(ScCareerElevator::update) ); 

        bRet = true;
    } while (0);

    return bRet;
}


 void ScCareerElevator::update(ccTime dt)
 {	
	
 }