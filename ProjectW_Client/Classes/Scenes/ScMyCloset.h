#pragma once

#include "SceneBase.h"

class UISceneShowcase;
class ScMyCloset : public SceneBase
{
		
	static const int ButtonID_All = 0;
	static const int ButtonID_Dress = 1;
	static const int ButtonID_Top = 2;
	static const int ButtonID_Bottom = 3;
	static const int ButtonID_Shoes = 4;
	static const int ButtonID_Accessory = 5;	

public :	
    virtual bool init(); 
	
	ScMyCloset();
	virtual ~ScMyCloset();	

	virtual const char* getSceneName() { return "Closet"; }
	
protected :
	virtual void messageBoxCallback(CCObject* pSender);	
	virtual void update(ccTime dt);

	void	menuBackCallback(CCObject* pSender);
	void	itemSelectionCallback(CCObject* pSender);

	void	initButtons(CCLayer* pUILayer);
	void	loadItems();
	void	loadItems(unsigned int type);

	float	m_fTick;
	bool	m_bAccelation;	
	float	m_fAccelPerTime;
	CCPoint	m_touchBeginPoint;
	float	m_touchBeginTime;
	
	CCNode*				m_pCharacterNode;
	CCMenu*				m_pCategory;
	UISceneShowcase*	m_pShowcase;
};