#pragma once;

#include "BasicHeader.h"

class WSprite;
struct CosmeticData
{
	unsigned int idx;
	unsigned int id;

	unsigned int partId;
	unsigned int resourceId;
	unsigned int expression;
};
typedef std::vector<CosmeticData> CosmeticDataVec;

class Cosmetic : public CCNode
{
public :
	enum
	{
		Expression_Default = 0,
		Expression_A = 1,
		Expression_B = 2,
		Expression_Count,
	};

	Cosmetic();
	virtual ~Cosmetic();
	
	static Cosmetic* cosmetic();

	void	setCosmeticData(const CosmeticDataVec& vec);
	UInt	PartId;

	static bool	checkExpression(unsigned int expressionIdx);
	void		setExpression(unsigned int expressionIdx);
	virtual void draw();
	
protected :
	CCMutableArray<WSprite*>	m_expressions;
	WSprite*					m_selectedExpression;	
};
