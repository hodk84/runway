#pragma once;

#include <Cocos2d.h>

struct MerchandiseItem
{
	unsigned int tableType;

	unsigned int merchandiseID;
	unsigned int itemID; //Clothes ID
	//unsigned int thumbnailID; //Resource ID -> clothes�� �ű�.
	unsigned int category;//Clothes Type
	
	unsigned int priceType;
	unsigned int price;

};
typedef map<unsigned int, MerchandiseItem*> MerchandiseItemMap;
typedef vector<MerchandiseItem*> MerchandiseItemVec;

struct MerchandiseList
{
	enum TableType
	{
		Type_Clothes = 1,
		Type_Money = 2,
		Type_Cosmetic = 3,
	};


	unsigned int id;
	string		 name;
	unsigned int season;
	unsigned int type;

	
	MerchandiseItem* getItem(unsigned int itemID)
	{
		MerchandiseItemMap::iterator itrFound = items.find(itemID);
		if( itrFound != items.end() )
		{
			return (*itrFound).second;
		}
		return NULL;
	}

	void getItemsByCategory(unsigned int category, vector<MerchandiseItem*>& retItems)
	{
		MerchandiseItemMap::iterator it = items.begin();
		for( it; it != items.end(); ++it)
		{
			MerchandiseItem* pItem = it->second;
			if( NULL == pItem ) continue;

			if( 0 == category|| category == pItem->category )
			{
				retItems.push_back(pItem);
			}
		}
	}

	MerchandiseItemMap items;
};
typedef map<unsigned int, MerchandiseList*> MerchandiseMap;