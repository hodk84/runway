#pragma once

#include "Cocos2d.h"
#include "Animator.h"
#include "WSprite.h"
#include "DrawDebug.h"
#include "WBone.h"

class WSkeleton : public CCNode
{
public : 
	enum
	{
		Node_Root = 0,
		Node_Hip = 1,
		Node_Chest = 2,
		Node_Chest_back = 3,
		Node_Neck = 4,
		Node_Neck_front= 5,

		Node_Face = 6,
		Node_Eye = 7,
		Node_Mouth = 8,		
		Node_Hair_back = 9,
		Node_Hair_front = 10,
		
		Node_r_UpperArm = 11,
		Node_r_LowerArm = 12,		
		Node_r_Hand = 13,
		
		Node_l_UpperArm = 14,
		Node_l_LowerArm = 15,
		Node_l_Hand = 16,
		
		Node_r_UpperLeg = 17,
		Node_r_LowerLeg = 18,
		Node_r_Foot = 19,

		Node_l_UpperLeg = 20,
		Node_l_LowerLeg = 21,
		Node_l_Foot = 22,

		Node_Count,
	};

	WSkeleton();
	virtual ~WSkeleton();
	
	virtual void visit();
	virtual bool LoadCharacterSkeleton(const char* fileName);
	virtual CCNode* ProcessNode(xmlNodePtr pNode, int &nDepth);

	CCNode*	GetChildNode(string nodeName);
	void DoZSort();

	WBone* getPartFromPartID(unsigned partID);

	bool attachNodeBone(unsigned int partID, CCNode* pNode, int depth = 1);

private :
	WBoneNameMap	m_childNameMap;
	WBoneVec		m_childBoneVec;
	std::vector<CCNode*> m_ZNodes;
};