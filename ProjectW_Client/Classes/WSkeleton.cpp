
#include "WSkeleton.h"

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>
#include <libxml/xmlreader.h>
#include <algorithm>
#include "effects/CCGrid.h"


WSkeleton::WSkeleton()
{
	m_childBoneVec.resize(Node_Count, NULL);
}

WSkeleton::~WSkeleton()
{

}

void WSkeleton::visit()
{
	CCNode::visit();
	//if (!m_bIsVisible)
	//{
	//	return;
	//}
	//glPushMatrix();

 //	if (m_pGrid && m_pGrid->isActive())
 //	{
 //		//m_pGrid->beforeDraw();
 //		this->transformAncestors();
 //	}

	//this->transform();

 //   CCNode* pNode = NULL;
 //	// self draw
	//this->draw();
	//std::vector<CCNode*>::iterator itrZ = m_ZNodes.begin();
	//for( ; itrZ!=m_ZNodes.end(); ++itrZ)
	//{
	//	pNode = (*itrZ);
	//	pNode->visit();
	//	//if( ! pNode->getIsVisible() )
	//	//	continue;
	//	//glPushMatrix();
	//	//CCGridBase* pGrid = pNode->getGrid();		
 //	//	if (pGrid && pGrid->isActive())
 //	//	{
 //	//		//m_pGrid->beforeDraw();
 //	//		pNode->transformAncestors();
 //	//	}
	//	//pNode->transform();
	//	//pNode->draw();
	//	//if (m_pGrid && m_pGrid->isActive())
 //	//	{
 //	//		//m_pGrid->afterDraw(this);
	//	//}
	//	//glPopMatrix();

	//}	

 //	if (m_pGrid && m_pGrid->isActive())
 //	{
 //		//m_pGrid->afterDraw(this);
	//}
 //
	//glPopMatrix();
}

bool WSkeleton::LoadCharacterSkeleton(const char* fileName)
{
	m_childNameMap.clear();

	const char* filePath = CCFileUtils::fullPathFromRelativePath(fileName);
	
	xmlDoc *doc = NULL;
	xmlNode *root_element = NULL;
	doc = xmlReadFile(filePath, NULL, 0);	
	root_element = xmlDocGetRootElement(doc);
	assert(root_element);	
	xmlNode* pChild = root_element->children;
	while(pChild)//"Node" Element를 찾음.
	{
		//첫번째 Node가 Root가 되며 Root는 유일하다.
		if(0 == xmlStrcmp(pChild->name, (const xmlChar *)"Node"))
		{
			int depth = 0;
			CCNode* pSkeletonRoot = ProcessNode(pChild, depth);	
			addChild(pSkeletonRoot);			
		}
		pChild = pChild->next;
	}

	xmlFreeDoc(doc);

	
	xmlCleanupParser();		
	
	DoZSort();
	
	return true;
}

CCNode* WSkeleton::ProcessNode(xmlNodePtr pNode, int &nDepth)
{
	if(0 != xmlStrcmp(pNode->name, (const xmlChar *)"Node"))
		return NULL;

	xmlChar *cName = xmlGetProp(pNode, (const xmlChar *)"name");
	xmlChar *cIDX = xmlGetProp(pNode, (const xmlChar *)"idx");
	unsigned int idx = 0;
	if( cIDX )
	{
		idx = atoi((const char*)cIDX);
		xmlFree(cIDX);
	}
	
	float posX = 0;
	float posY = 0;
	float anchorX = 0;
	float anchorY = 0;
	bool anchorConvert = 0;

	xmlChar *cPosX = xmlGetProp(pNode, (const xmlChar *)"posX");
	if( cPosX) posX = atof((const char*)cPosX);
	
	xmlChar *cPosY = xmlGetProp(pNode, (const xmlChar *)"posY");
	if( cPosY )	posY = atof((const char*)cPosY);
	
	xmlChar *cAnchorX = xmlGetProp(pNode, (const xmlChar *)"anchorX");
	if( cAnchorX )	anchorX = atof((const char*)cAnchorX);

	xmlChar *cAnchorY = xmlGetProp(pNode, (const xmlChar *)"anchorY");
	if(cAnchorY) anchorY = atof((const char*)cAnchorY);

	xmlChar *cAnchorConv = xmlGetProp(pNode, (const xmlChar *)"anchorConvert");
	if( cAnchorConv )	anchorConvert = atoi((const char*)cAnchorConv);

	xmlChar *cDepth = xmlGetProp(pNode, (const xmlChar *)"depth");
	if( cDepth ) nDepth = atoi((const char*)cDepth);
	
	xmlChar *cFileName = xmlGetProp(pNode, (const xmlChar *)"file");
	//Modified!
	
	float width = 0.f;
	float height = 0.f;

	xmlChar *cWidth = xmlGetProp(pNode, (const xmlChar *)"width");
	if( cWidth)
	{
		width = atof((const char*)cWidth);
		xmlFree(cWidth);
	}
	
	xmlChar *cHeight = xmlGetProp(pNode, (const xmlChar *)"height");
	if( cHeight )
	{	
		height = atof((const char*)cHeight);
		xmlFree(cHeight);
	}

	//WSprite* pSprite = WSprite::CreateNodeWSprite((const char*)cFileName, posX, posY, anchorX, anchorY, anchorConvert, nDepth);	
	WBone* pBone = WBone::CreateJoint(width, height, anchorX, anchorY, anchorConvert, nDepth, true, (const char*)cFileName);

	pBone->m_id = idx;
	string szNodeName((char*)cName);

	xmlFree(cName);
	xmlFree(cPosX);
	xmlFree(cPosY);
	xmlFree(cAnchorX);
	xmlFree(cAnchorY);
	xmlFree(cDepth);
	xmlFree(cFileName);
	
	if( NULL == pBone ) return NULL;
	pBone->setPosition(CCPoint(posX, posY));
	pBone->m_origPos = CCPoint(posX, posY);	

	m_childBoneVec[idx] = pBone;
	m_childNameMap.insert(WBoneNameMap::value_type(szNodeName, pBone));	
	xmlNode* child = pNode->children;
	while(child)
	{
		int childDepth = 0;
		CCNode* pChildSprite = ProcessNode(child, childDepth);//pSprite);	
		if( pChildSprite )
		{
			pBone->addChild(pChildSprite);
			pBone->reorderChild(pChildSprite, childDepth);		 
		}
		child = child->next;
	}
	m_ZNodes.push_back((CCNode*)pBone);
	return pBone;
}

CCNode* WSkeleton::GetChildNode(string nodeName)
{
	WBoneNameMap::iterator itrChild = m_childNameMap.find(nodeName);
	if( itrChild != m_childNameMap.end() )
	{
		return (CCNode*)(*itrChild).second;
	}
	else
		return NULL;
}

void WSkeleton::DoZSort()
{
	struct ZFunctor
	{
		bool operator()(CCNode* pNode1, CCNode* pNode2) const
		{
			return (pNode1->getZOrder() > pNode2->getZOrder());
		}
	}fn;

	std::sort(m_ZNodes.begin(), m_ZNodes.end(), fn);	
}

WBone* WSkeleton::getPartFromPartID(unsigned partID)
{
	unsigned idx = partID;

	if( idx < 0 || m_childBoneVec.size() < idx ) return NULL;

	return m_childBoneVec[idx];
}

bool WSkeleton::attachNodeBone(unsigned int partID, CCNode* pNode, int depth)
{
	WBone* pPart = getPartFromPartID(partID);
	if( NULL == pPart ) return false;

	pPart->addChild(pNode, depth);

	return true;
}