#pragma once

#include "Cocos2d.h"
#include "Animator.h"

using namespace cocos2d;

class WSprite : public CCSprite
{
public :
	WSprite()
	{
		m_origPos = CCPoint(0.f, 0.f);
		m_origScale = CCPoint(1.f, 1.f);
		m_origRot = 0.f;
	}
	virtual ~WSprite();

	static WSprite* WSpriteWithFile(const char *pszFileName);	
	static WSprite* CreateNodeWSprite(const char* fileName, float px, float py, float ax, float ay, bool anchorConvert, int depth);

	virtual void SetRotate(const KeyAnimation* anim, float duration);	
	virtual void SetMove(const KeyAnimation* anim, float duration);

	CCPoint m_origPos;
	float	m_origRot;
	CCPoint m_origScale;	
};

typedef std::vector<WSprite*> WSpriteVec;
typedef std::map<string, WSprite*> WSpriteNameMap;