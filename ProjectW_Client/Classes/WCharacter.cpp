
#include "WCharacter.h"

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>
#include <libxml/xmlreader.h>

#include "Managers\TableManager.h"
#include "WSprite.h"

WCharacter::WCharacter()
: WSkeleton()
, m_characterId(0)
, m_nameTag(NULL) 
{
	m_wearedClothes.resize(WClothes::Cloth_Count, 0);
}

WCharacter::~WCharacter()
{

}

bool WCharacter::playAnimation(int animationID)
{
	if( animationID >= 0 && animationID < m_animations.size() )
	{
		WAnimation* pAnimSet = m_animations[animationID];
		pAnimSet->PlayAnimation();		
		return true;
	}
	return false;
}

void WCharacter::addAnimationSet(WAnimation* pAnimation)
{
	if( pAnimation )
	{
		m_animations.push_back(pAnimation);
	}
}
void WCharacter::startKeyAnimation(std::string str, float rotate)
{
	WBone* pNode = (WBone*)GetChildNode(str);

	if( pNode )
	{
		char frontNodeName [128];
		char backNodeName [128];
		
		sprintf(frontNodeName, "%s_f", str.c_str());
		sprintf(backNodeName, "%s_b", str.c_str());
		
		std::string strFront;
		std::string strBack;

		strFront.assign(frontNodeName);
		strBack.assign(backNodeName);

		WBone* pNodeFront = (WBone*)GetChildNode(strFront);
		WBone* pNodeBack = (WBone*)GetChildNode(strBack);		
				
		pNode->setRotation(rotate);
			
		if(pNodeFront)
		{
			pNodeFront->setRotation(rotate);
		}
		if(pNodeBack)
		{
			pNodeBack->setRotation(rotate);
		}	
	}
	
}
void WCharacter::startKeyAnimation(const KeyAnimation* pAnim, float duration)
{
	if( pAnim == NULL ) return;	

	WBone* pNode = (WBone*)GetChildNode(pAnim->_nodeName);

	if( pNode )
	{
		if( 0 == strcmp(pAnim->_nodeName.c_str(), "Neck") )
		{
			//return;
			int a;
			a= 1;
		}

		else if( 0 == strcmp(pAnim->_nodeName.c_str(), "Chest") )
		{
			//return;
			int a;
			a= 1;
		}
		char frontNodeName [128];
		char backNodeName [128];
		
		sprintf(frontNodeName, "%s_f", pAnim->_nodeName.c_str());
		sprintf(backNodeName, "%s_b", pAnim->_nodeName.c_str());
		
		std::string strFront;
		std::string strBack;

		strFront.assign(frontNodeName);
		strBack.assign(backNodeName);

		WBone* pNodeFront = (WBone*)GetChildNode(strFront);
		WBone* pNodeBack = (WBone*)GetChildNode(strBack);
		
		pNode->SetRotate(pAnim, duration);
		pNode->SetMove(pAnim, duration);
			
		if( pNodeFront )
		{
			pNodeFront->SetMove(pAnim, duration);
			pNodeFront->SetRotate(pAnim, duration);
		}
		if( pNodeBack )
		{
			pNodeBack->SetMove(pAnim, duration);
			pNodeBack->SetRotate(pAnim, duration);
		}			
	}		
}


void WCharacter::setClothesApply(unsigned int clothID, bool isMine)
{
	WClothes* pCloth = TableManager::getInst()->getClothes(clothID);
	if( NULL == pCloth ) return;
	unsigned int clothType = pCloth->m_type -1;
	if( m_wearedClothes.size() <= clothType )
		m_wearedClothes.resize(clothType + 1);
	
	//옷을 입힘.
	if( pCloth )
	{
		this->setClothes(pCloth);
	}
	// 내 옷일 경우 입고 착용인벤토리에도 기록
	if( isMine && m_wearedClothes[clothType] != clothID )
	{
		m_wearedClothes[clothType] = clothID;	
	}
}
void WCharacter::setClothes(unsigned int clothId)
{
	WClothes* pCloth = TableManager::getInst()->getClothes(clothId);
	if( NULL == pCloth ) return;

	this->setClothes(pCloth);
}

void WCharacter::setClothes(WClothes* pCloth)
{
	if( NULL == pCloth ) return;

	WClothes::ClothParts::iterator itrParts = pCloth->m_parts.begin();
	for(;itrParts != pCloth->m_parts.end(); ++itrParts)
	{
		WClothes::Part* part = &(*itrParts);
		if( part )
		{
			WBone* pPartBone = this->getPartFromPartID(part->partID);
			if( pPartBone )
			{
				char fileDest[256];
				TableManager::getInst()->getFilePathByResourceId(part->resourceID, fileDest);				
				
				pPartBone->removeChildByTag(pCloth->m_type, true);
				
				WSprite* pSprite = WSprite::CreateNodeWSprite(fileDest, 0.f, 0.f, 0.f, 0.f, false, part->depth);				
				pSprite->setTag(pCloth->m_type);				
				pPartBone->addChild(pSprite, part->depth);
			}
		}
	}
}
void WCharacter::setSkin(CharacterSkin* skin)
{
	m_pCharacterSkin = skin;
	reloadSkin();
}

void WCharacter::reloadSkin()
{
	if( NULL == m_pCharacterSkin ) return ;
	for( int i = 0; i < m_pCharacterSkin->skinParts.size(); ++i)
	{		
		ImageData* pData = TableManager::getInst()->getImageDataByResourceId(m_pCharacterSkin->skinParts[i]);
		if( pData )
		{
			string path = pData->getFullPathAsStr();
			WSprite* pSkin = WSprite::CreateNodeWSprite(path.c_str(), 0, 0, 0, 0, false, 0);
			if( pSkin )
			{
				attachNodeBone(i, pSkin, 0);
			}
		}
	}
}

void WCharacter::applyClothes()
{
	vector<unsigned int>::iterator itr = m_wearedClothes.begin();
	for( itr; itr != m_wearedClothes.end(); ++itr)
	{
		WClothes* pCloth = TableManager::getInst()->getClothes((*itr));
		if( pCloth )
		{
			this->setClothes(pCloth);
		}
	}
}

bool WCharacter::loadDefaultCharacterData()
{	
	LoadCharacterSkeleton("Skeleton_A.xml");
	
	WAnimation* pAnimation = new WAnimation(this);	
	pAnimation->LoadAnimation("Animations.xml");

	this->addAnimationSet(pAnimation);	
	
	//from Saved Data
	CharacterSkin* pSkin = new CharacterSkin("DefaultSkin.xml");
	this->setSkin(pSkin);	

	//from Definition data	
	m_wearedClothes.push_back(12001);
	m_wearedClothes.push_back(13001);
	m_wearedClothes.push_back(14001);
	m_wearedClothes.push_back(15001);
	
	m_closet.addClothes(2, 12003, 1);
	m_closet.addClothes(2, 12002, 1);
	m_closet.addClothes(3, 13002, 1);
	m_closet.addClothes(4, 14002, 1);
	m_closet.addClothes(5, 15002, 1);

	m_wearedCosmetic.push_back(300001);
	m_wearedCosmetic.push_back(310001);

	applyClothes();
	applyCosmetic();
	
	
	CCLabelTTF* pLabel = CCLabelTTF::labelWithString("", "Thonburi", 32);
	pLabel->setPosition(ccp(0.f, 220.f));
	addChild(pLabel, 99);
	m_nameTag = pLabel;
	
	return true;
}


bool WCharacter::hasClothes(unsigned int clothesID)
{	
	if( NULL != m_closet.getClothes(clothesID) )
		return true;

	return false;
}

void WCharacter::applyCosmetic()
{
	vector<unsigned int>::iterator itr = m_wearedCosmetic.begin();
	for(itr; itr != m_wearedCosmetic.end(); ++itr)
	{
		setCosmetic((*itr));
	}
}

void WCharacter::setCosmetic(unsigned int cosmeticId)
{
	CosmeticDataVec vec;
	TableManager::getInst()->getCosmetic(cosmeticId, vec);
	if( vec.empty() ) return;

	WBone* pPartBone = this->getPartFromPartID(vec[0].partId);
	if( pPartBone )
	{	
		Cosmetic* pCosmetic = Cosmetic::cosmetic();
		if( pCosmetic )
		{				
			pCosmetic->setCosmeticData(vec);
			pCosmetic->setTag(CosmeticTagOffset+vec[0].partId);
			pPartBone->removeChildByTag(CosmeticTagOffset+vec[0].partId, true);
			pPartBone->addChild(pCosmetic);
		}
	}
}

void WCharacter::setExpression(unsigned int cosmeticPart, unsigned int expressionIdx)
{
	WBone* pPartBone = this->getPartFromPartID(cosmeticPart);
	if( pPartBone )
	{
		CCNode* pNode = pPartBone->getChildByTag(CosmeticTagOffset+cosmeticPart);
		if( pNode )
		{
			Cosmetic* pCosmetic = dynamic_cast<Cosmetic*>(pNode);
			if( pCosmetic)
			{
				pCosmetic->setExpression(expressionIdx);
			}
		}
	}
}

void WCharacter::updateCharacterStatus()
{
	//캐릭터 데이터 업데이트 프롬 디비 혹은 로컬 데이터
	CharacterStatus data;
	data.cash = 100;
	data.gameMoney = 1000;
	data.energy = 5;
	data.clothCount = m_closet.getClothesCount();
	
	this->setCharacterStatus(data);
}

int WCharacter::getCharacterDayPoint()
{
	int ret = 0;
	vector<unsigned int>::iterator itr = m_wearedClothes.begin();
	for(itr; itr != m_wearedClothes.end(); ++itr)
	{
		WClothes* cloth = TableManager::getInst()->getClothes((*itr));
		if( NULL == cloth ) continue;
		for(int i = 0; i < cloth->m_options.size(); ++i)
		{
			if( cloth->m_options[i].type == 1 )
			{
				ret += cloth->m_options[i].value;
			}
		}
	}
	return ret;
}

int WCharacter::getCharacterNightPoint()
{
	int ret = 0;
	vector<unsigned int>::iterator itr = m_wearedClothes.begin();
	for(itr; itr != m_wearedClothes.end(); ++itr)
	{
		WClothes* cloth = TableManager::getInst()->getClothes((*itr));
		if( NULL == cloth ) continue;
		for(int i = 0; i < cloth->m_options.size(); ++i)
		{
			if( cloth->m_options[i].type == 2 )
			{
				ret += cloth->m_options[i].value;
			}
		}
	}
	return ret;
}