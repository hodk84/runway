#pragma once;

#include <cocos2d.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>


string GetPropertyString(xmlNodePtr node, const char* prop);

int GetPropertyInt(xmlNodePtr node, const char* prop);

unsigned int GetPropertyUInt(xmlNodePtr node, const char* prop);

float GetPropertyFloat(xmlNodePtr node, const char* prop);

