#include "CurlTest.h"
#include "stdio.h"
#include "stdlib.h"
#include "curl/curl.h"
#include "CCCommon.h"


size_t recieveData(void *ptr, size_t size, size_t nmemb, void *out)
{
	int copy_size = 0;
	int actual_size = size*nmemb;

	MEMORY_STRUCT_S *pst_mem = (MEMORY_STRUCT_S*)out;

	if( actual_size + pst_mem->done_size > pst_mem->max_size)
	{
		copy_size = pst_mem->max_size - pst_mem->done_size;
	}
	else
	{
		copy_size = actual_size;
	}

	memcpy(pst_mem->ptr + pst_mem->done_size, ptr, copy_size);
	pst_mem->done_size += copy_size;

	return actual_size;
}

CurlTest::CurlTest()
{
	CCSize s = CCDirector::sharedDirector()->getWinSize();
	CCLabelTTF* label = CCLabelTTF::labelWithString("Curl Test", "Arial", 28);
	addChild(label, 0);
	label->setPosition( ccp(s.width/2, s.height-50) );

    setIsTouchEnabled(true);

    // create a label to display the tip string
    m_pLabel = CCLabelTTF::labelWithString("Touch the screen to connect", "Arial", 22);
    m_pLabel->setPosition(ccp(s.width / 2, s.height / 2));
	addChild(m_pLabel, 0);
    
    m_pLabel->retain();
}


// the test code is
// http://curl.haxx.se/mail/lib-2009-12/0071.html
void CurlTest::ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent)
{
	CURL *curl;
	CURLcode res;
	
	char buffer[40960];


	MEMORY_STRUCT_S st_body;
	st_body.done_size = 0;
	st_body.max_size = 40960;
	st_body.ptr = buffer;


	//curl_global_init(CURL_GLOBAL_ALL);
	curl = curl_easy_init();
	if (curl) 
	{
		//context객체 설정. CURL_OPT_URL = 목표Url
		curl_easy_setopt(curl, CURLOPT_URL, "http://google.com/");
		curl_easy_setopt(curl, CURLOPT_HEADER, 0);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, recieveData);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &st_body);
		//curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1);

		//이하 헤더는 표준에러, 바디는 표준출력으로 가져오도록 설정
		//cur_easy_setopt(curl, CURLOPT_WRITEHEADER, stderr);
		//cur_easy_setopt(curl, CURLOPT_WRITEDATA, stdout);

		//실제 페이지를 긁어옴.
		res = curl_easy_perform(curl);
				
		//연결 성공
		/*if (res == CURLE_OK)
		{
			long statLong;
			char* statString = 0;
			double statDouble;

			if( CURLE_OK == curl_easy_getinfo(curl, CURLINFO_HTTP_CODE, &statLong) )
			{
				CCLog("Log: %d", statLong);
			}
			if( CURLE_OK == curl_easy_getinfo(curl, CURLINFO_CONTENT_TYPE, &statString) )
			{
				CCLog("Log: %s", statString);
			}
			if( CURLE_OK == curl_easy_getinfo(curl, CURLINFO_SIZE_DOWNLOAD, &statDouble) )
			{
				CCLog("Log: %d", statDouble);
			}
			if( CURLE_OK == curl_easy_getinfo(curl, CURLINFO_SPEED_DOWNLOAD, &statDouble) )
			{
				CCLog("Log: %d", statDouble);
			}
			
			if( CURLE_OK == curl_easy_setopt(curl, CURLOPT_WRITEDATA, &st_body))
			{
				CCLog("Log: %d", statDouble);
			}
			if( CURLE_OK == curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, recieveData) )
			{
				CCLog("Log: %d", statDouble);
			}

		}
		else
		{
			sprintf(buffer,"code: %i",res);
			m_pLabel->setString(buffer);
		}*/
	} 
	else 
	{
		m_pLabel->setString("no curl");
	} 
	/* always cleanup */
	curl_easy_cleanup(curl);
	//curl_global_cleanup();
}

CurlTest::~CurlTest()
{
	m_pLabel->release();
}

void CurlTestScene::runThisTest()
{
    CCLayer* pLayer = new CurlTest();
    addChild(pLayer);

    CCDirector::sharedDirector()->replaceScene(this);
    pLayer->release();
}
